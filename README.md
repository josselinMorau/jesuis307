# JeSuis106_v2

Repo officiel du botTILLA.

## Installation et test en local

- Clonez le repo sur votre PC : ```git clone https://gitlab.atilla.org/moraujosse/atilla-discord-bot.git``` (HTTPS) ou ```git clone git@gitlab.atilla.org:moraujosse/atilla-discord-bot.git``` (SSH)
- Assurez vous d'avoir [Node.js d'installé](https://nodejs.org/en/download/current/) (v16 ou plus)
    - Pour vérifier votre version de Node : ```node -v```
- Installez une base de donnée SQL localement sur votre PC (de préférence MySQL)
- À la racine du repo, créez un fichier `.env` qui contiendra les variables d'environnement dont le bot à besoin. Référez vous à [la section sur les variables d'environnement](#variables-denvironnement) pour plus d'informations.
    - Ajoutez la ligne ```ATILLA_BOT_TOKEN=le_token_du_bot_testé``` (Demandez au devs le token du bot de test)
    - Ajoutez la ligne ```ATILLA_BOT_DATABASE_URI=uri_de_votre_bdd```. Si vous utilisez MySQL par exemple, ce sera : ```ATILLA_BOT_DATABASE_URI=mysql://username:password@localhost:3306/le_nom_de_ma_bdd```
    - Ajoutez la ligne ```ATILLA_BOT_TESTING=TRUE``` pour vous assurez d'être en mode de test.
- Assurez vous que tous les node modules soient installés : ```npm install```
- Lancez le bot : ```npm start```

## Documentation

### Manuel

La documentation des classes et des différents concepts à connaitre pour programmer sur le bot se trouve [ici](<./documentation/Manual.md>).

### Coding Guidelines

La documentation sur les coding guidelines se trouve [ici](<./documentation/Coding Guidelines.md>).

## Variables d'environnement

Les variables d'environnement sont utilisées pour passer des options ou des credentials au bot.
La liste des variables d'environnement peut être trouvée ici : 

<table>
    <thead>
        <tr>
            <th>Nom</th>
            <th>Requis ?</th>
            <th>Valeur par défaut</th>
            <th>Description</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td>ATILLA_BOT_TOKEN</td>
            <td>✔️</td>
            <td>Aucune</td>
            <td>Le token Discord du bot.</td>
        </tr>
        <tr>
            <td>ATILLA_BOT_DATABASE_URI</td>
            <td>✔️</td>
            <td>Aucune</td>
            <td>L'uri utilisée pour se connecter à la bdd.</td>
        </tr>
        <tr>
            <td>ATILLA_BOT_TESTING</td>
            <td>❌</td>
            <td>FALSE</td>
            <td>Un booléen pour déterminer si le bot est en mode de test ou non.</td>
        </tr>
        <tr>
            <td>ATILLA_BOT_LOGGER</td>
            <td>❌</td>
            <td>Console</td>
            <td>Le nom du type de logger à utiliser pendant la durée de vie de l'application.</td>
        </tr>
        <tr>
            <td>ATILLA_BOT_DISPLAY_COMMAND_TABLE</td>
            <td>❌</td>
            <td>FALSE</td>
            <td>Si à 'TRUE', affiche la table des commandes dans les logs au démarrage de l'application.</td>
        </tr>
        <tr>
            <td>ATILLA_BOT_DEVELOPER_IDS</td>
            <td>❌</td>
            <td>Aucune</td>
            <td>Une liste d'ids Discord de développeurs. La liste est séparée par des virgules. Cette liste permet d'outrepasser les checks de permission des commandes en mode de test.</td>
        </tr>
        <tr>
            <td>ATILLA_BOT_RESTART_TIME</td>
            <td>❌</td>
            <td>10000</td>
            <td>La durée (en ms) de redémarrage du bot. Le bot redémarre uniquement via une commande manuelle.</td>
        </tr>
    </tbody>
</table>

## Auteurs/Contributeurs

- CORBEAU Maxime
- MORAU Josselin
- MOUSSET Martin
- MALZAC Guillaume
- HEURION Thomas
- CESSON Maxime

## Documentations utiles

- [Javascript](https://developer.mozilla.org/en-US/docs/Web/JavaScript)
- [Node.js](https://nodejs.org/en/docs/)
- [Discord.js](https://discord.js.org/#/docs/main/stable/general/welcome)
- [Sequelize](https://sequelize.org/master/manual/getting-started.html)
