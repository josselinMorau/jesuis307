const { Client, GuildMember, Interaction, Message, Guild, ThreadChannel } = require("discord.js");
const { EventEmitter } = require("events");
const { options } = require("../options.js");
const { logger } = require("./logging/logging.js");
const { getClassMethodNames } = require("./utils/helper.js");
const { BaseService } = require("./BaseService");
const { CommandService } = require("./commands/CommandService.js");
const { ReactionService } = require("./reaction/ReactionService.js");
const { ChannelListener } = require("./utils/channels/ChannelListener.js");
const { SettingService, SETTING_NAMES } = require("./utils/guildSettings/SettingsService.js");
const { WidgetListener } = require("./utils/widgets/WidgetListener.js");
const { TaskExecutor } = require("./commands/TaskExecutor.js");
const { RoutineService } = require("./routine/RoutineService.js");
const { NewcomerHandler } = require("./misc/NewcomerHandler.js");
const { RoleService } = require('./utils/roles/RoleService.js');
const { ChannelService } = require("./utils/channels/ChannelService.js");
const { DataModel } = require("./database/DataModel.js");
const { JailService } = require("./misc/JailService.js");
const { BirthdayService } = require("./misc/BirthdayService.js");
const { PollService } = require("./misc/PollService.js");
const { MusicService } = require("./misc/MusicService.js");
const { ChannelLogListener } = require("./misc/ChannelLogListener.js");
const { CacheService } = require("./misc/CacheService.js");

/**
 * @type {(typeof BaseService)[]} 
 * List of all service types. The order in the array defines the order in which each service will be initialized.
 */
const SERVICES = [
    CommandService,
    TaskExecutor,
    ReactionService,
    SettingService,
    RoutineService,
    RoleService,
    ChannelService,
    ChannelListener,
    ChannelLogListener,
    WidgetListener,
    NewcomerHandler,
    JailService,
    BirthdayService,
    PollService,
    MusicService,
    CacheService
];

/**
 * @class A wrapper object for the Discord's client.
 */
class ClientContext
{
    static __idCount = 0;

    /**
     * The Discord's client.
     * @param {Client} client 
     */
    constructor(client) {
        /**
         * The Discord's client.
         */
        this.client = client;
        /**
         * The unique id for this context.
         */
        this.id = ClientContext.__idCount++;
        /**
         * List of all the services bound to the context.
         */
        this.services = SERVICES.map(Service => new Service(this));
        // Setup Discord's callbacks.
        // Callbacks are deduced from the method name.
        const methodNames = getClassMethodNames(ClientContext);
        for (let methodName of methodNames) {
            if (!methodName.startsWith('on') || methodName.startsWith('once') || methodName.length <= 2)
                continue;
            const method = this[methodName];
            if (!method)
                continue;
            const eventName = methodName.charAt(2).toLowerCase() + methodName.slice(3);
            this.client.on(eventName, method.bind(this)); // Has to call bind to make sure 'this' keep its reference.
        }
        /**
         * Emitter for terminated event.
         */
        this.__emitter = new EventEmitter();
    }

    /**
     * Terminate all services bound to this context and destroy the client.
     */
    async terminate(shouldRestart = false) {
        // Unregister events bound to the context.
        DataModel.clearAllModelEvents(this);
        // Terminate all services.
        for (let service of this.services) {
            await service.terminate();
        }
        this.client.destroy();
        this.__emitter.emit('terminated', this, shouldRestart);
    }

    /**
     * Add a listener called once the client has been destroyed and the context terminated.
     * @param {(ctx : ClientContext, shouldRestart : boolean) => void} listener 
     */
    onceTerminated(listener) {
        this.__emitter.once('terminated', listener);
    }

    /**
     * Remove the given listenr to the 'terminated' event.
     * @param {(ctx : ClientContext, shouldRestart : boolean) => void} listener  
     */
    offTerminated(listener) {
        this.__emitter.off('terminated', listener);
    }

    /**
     * Returns the associated service of the given type.
     * @template {BaseService} T
     * @param {new T} ServiceType The type of the service to fetch.
     * @returns {T} The resulting service.
     */
    getService(ServiceType) {
        const index = SERVICES.findIndex(type => type === ServiceType || type.name === ServiceType.name);
        return this.services[index];
    }

    /**
     * Callback for the 'ready' event.
     */
    async onReady() {
        for (let service of this.services) {
            await service.init();
        }
        
        if (options.displayCommandTable) {
            const commandService = this.getService(CommandService);
            commandService.logCommandList();
        }
        logger.log(`Logged in as ${this.client.user.tag}!`);
        if (options.test)
            logger.log("TESTING MODE IS ENABLED.");
    }

    /**
     * Callback for the 'error' event.
     * @param {Error} error 
     */
    onError(error) {
        logger.error(error);
    }

    /**
     * Callback for the 'guildMemberAdd' event.
     * @param {GuildMember} member 
     */
    async onGuildMemberAdd(member) {
        if (member.user.bot)
            return;

        const settingService = this.getService(SettingService);
        const welcomeNewcomer = await settingService.getSetting(member.guild.id, SETTING_NAMES.WELCOME_ENABLED)
            .catch(_err => {
                logger.error(_err);
                return false;
            });
    
        if (welcomeNewcomer) {
            const newcomerHandler = this.getService(NewcomerHandler);
            newcomerHandler.handleNewcomer(member)
                .catch(_err => { logger.error(_err); });
        }
    }

    /**
     * Callback for the 'guildCreate' event.
     * @param {Guild} guild 
     */
    async onGuildCreate(guild) {
        for (let service of this.services) {
            await service.initNewGuild(guild);
        }
    }

    /**
     * Callback for the 'threadCreate' event.
     * @param {ThreadChannel} threadChannel 
     */
    onThreadCreate(threadChannel) {
        threadChannel.join() // Automatically joins thread channels.
            .catch(err => logger.error(err));
    }

    /**
     * Callback for the 'interactionCreate' event.
     * @param {Interaction} interaction 
     */
    onInteractionCreate(interaction) {
        // Ignore interactions generated by other applications.
        if (interaction.applicationId !== this.client.application.id) {
            return;
        }

        const widgetListener = this.getService(WidgetListener);
        if (interaction.isButton()) {
            widgetListener.invokeButtonCallbacks(interaction);
            return;
        }

        if (interaction.isSelectMenu()) {
            widgetListener.invokeSelectMenuCallbacks(interaction);
            return;
        }
    }

    /**
     * Callback for the 'messageCreate' event.
     * @param {Message} msg
     */
    async onMessageCreate(msg) {
        if (msg.author.bot || !msg.guild)
            return;
        
        const settingService = this.getService(SettingService);
        const prefix = await settingService.getPrefix(msg.guildId);
        const commandService = this.getService(CommandService);
        const isCmd = await commandService.tryExecuteCommand(prefix, msg).catch(_err => logger.error(_err));
        if (!isCmd) {
            const reactionService = this.getService(ReactionService);
            await reactionService.react(msg);

            const channelListener = this.getService(ChannelListener);
            channelListener.invokeSingleChannelCallbacks(msg.channelId, msg);
            channelListener.invokeMultipleChannelCallbacks(msg.guildId, msg);

            // A way to remind the prefix.
            if (msg.mentions.members && msg.mentions.members.has(msg.guild.me.id) && msg.content.toLowerCase().includes("aled")) {
                msg.channel.send(`Utilise \`${prefix}help\` pour avoir la liste des commandes`);
            }
        }
    }

}

module.exports = {
    ClientContext
}