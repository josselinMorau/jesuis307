const { scheduleJob } = require('node-schedule');
const { Job, RecurrenceRule } = require('node-schedule');
const { ClientContext } = require('../ClientContext');

/**
 * @typedef {import('./RoutineService').ScheduleRule} ScheduleRule
 */

/**
 * Code of the initial schedule.
 * @param {ClientContext} ctx The application's client context.
 * @param {string} guildId The id of the guild.
 * @param {ScheduleRule} rule The rule for the schedule.
 * @returns {Job} The Job created by the schedule.
 */
function schedule(ctx, guildId, rule) {
    // Code goes there.
}

module.exports = {
    name: "example",
    enabledByDefault: true,
    defaultRule: '0 16 * * 1', // Every monday at 16:00.
    schedule: schedule
}