const fs = require('fs');
const { Job, scheduleJob, RecurrenceRule } = require("node-schedule");
const { RoutineInfo } = require('../database/models/RoutineInfo.js');
const { logger } = require('../logging/logging.js');
const { cleanObject } = require('../utils/helper.js');
const { BaseService } = require('../BaseService.js');
const { Guild } = require('discord.js');

/**
 * @typedef {string} CronString A string with the cron format to schedule events. See https://fr.wikipedia.org/wiki/Cron#Syntaxe_de_la_table.
 */

/**
 * @typedef {object} RecurrenceSpecDateRange
 * @property {Date | string | number | undefined} start
 * @property {Date | string | number | undefined} end
 * @property {CronString} rule
 * @property {string | undefined} tz
 */

/**
 * @typedef {CronString | number | RecurrenceRule | RecurrenceSpecDateRange | Date} ScheduleRule
 */

/**
 * @typedef {import('../ClientContext').ClientContext} ClientContext
 */

/**
 * @typedef {object} Routine The model for the object to export in a routine script.
 * @property {string} name The name of the routine.
 * @property {boolean} builtin Whether the routine object is a built-in. Automatically set.
 * @property {boolean} enabledByDefault (optional) Whether the routine should be enabled by default.
 * @property {CronString} defaultRule The default schedule rule for the routine.
 * @property {(ctx : ClientContext, guildId : string, rule : ScheduleRule) => Job} schedule The function to initialize the schedule of the routine.
 */

/**
 * Returns whether the given object respects the Routine type.
 * @param {object} obj The object to test.
 * @returns {boolean}
 */
function isRoutine(obj) {
    return typeof obj === 'object' &&
        typeof obj.name === 'string' &&
        typeof obj.defaultRule === 'string' &&
        typeof obj.schedule === 'function';
}

/**
 * The action performed when a dummy routine occurs.
 * @param {ClientContext} ctx The application's client.
 * @param {string} guildId The id of the routine guild.
 * @param {string} name The name of the routine.
 */
async function dummyRoutineAction(ctx, guildId, name) {
    const routineInfo = await RoutineInfo.findByPk(RoutineInfo.createUniqueId(guildId, name));
    const guild = await ctx.client.guilds.fetch(guildId);    
    const channel = await guild.channels.fetch(routineInfo.messageChannelId);
    channel.send(routineInfo.message);
}

/**
 * The schedule function for dummy routines.
 * @param {ClientContext} ctx The application's client.
 * @param {string} guildId The id of the routine guild.
 * @param {string} name The name of the routine.
 * @param {CronString} rule The scheduling rule.
 * @returns {Job} The resulting job.
 */
function scheduleDummyRoutine(ctx, guildId, name, rule) {
    return scheduleJob(rule, (_date) => {
        dummyRoutineAction(ctx, guildId, name)
            .catch(_err => logger.error(_err));
    });
}

/**
 * Generate a schedule function for a dummy routine.
 * @param {RoutineInfo} routineInfo The RoutineInfo of the dummy routine.
 * @returns {(ctx : ClientContext, rule : CronString) => Job}
 */
function createScheduleFunction(routineInfo) {
    return (ctx, guildId, rule) => { 
        return scheduleDummyRoutine(ctx, guildId, routineInfo.name, rule); 
    };
}

/**
 * Creates and returns a dummy `Routine` object based on a `RoutineInfo`.
 * @param {RoutineInfo} routineInfo 
 * @returns {Routine}
 */
function createDummyRoutine(routineInfo) {
    return {
        builtin: false,
        defaultRule: routineInfo.rule,
        enabledByDefault: routineInfo.enabled,
        name: routineInfo.name,
        schedule: createScheduleFunction(routineInfo)
    };
}

/**
 * @enum {string} Enum of all the possibles error codes when manipulating routines.
 */
const ROUTINE_ERRORS = {
    NO_ERROR: "NO_ERROR",
    // Generic errors
    WRONG_PARAMETERS: "WRONG_PARAMETERS",
    MISSING_PARAMETERS: "MISSING_PARAMETERS",
    NOT_FOUND: "NOT_FOUND",
    // Creation errors
    ALREADY_EXISTING_ROUTINE_NAME: "ALREADY_EXISTING_ROUTINE_NAME",
    CREATION_FAILED: "CREATION_FAILED",
    // Update errors
    UPDATE_FAILED: "UPDATE_FAILED",
    // Deletion errors
    CANT_DELETE_BUILTIN_ROUTINE: "CANT_DELETE_BUILTIN_ROUTINE",
    DELETE_FAILED: "DELETE_FAILED"
};

/**
 * @class Class to handle Routine objects. Routines are scripts to create periodic events.
 */
class RoutineService extends BaseService {

    constructor(ctx) {
        super(ctx);

        /**
         * @type {Map<string,Routine>} A map containing the built-in routine objects. Built-in routines are common for every guild.
         * Key is the routine name.
         */
        this.__routines = new Map();
        /**
         * @type {Map<string, Routine>} A map containing the custom routine objects. Key is the RoutineInfo's unique id.
         */
        this.__customRoutines = new Map();
        /**
         * @type {Map<string, Job>} A map containing all the current running jobs. Jobs are specific to each guild.
         * This means that a same routine can be scheduled for one guild and not for another.
         * Key is the unique id of a RoutineInfo.
         */
        this.__jobs = new Map();
        this.__builtinImported = false;
        
        RoutineInfo.on(this.clientContext, "afterCreate", instance => { this.__onRoutineInfoCreate(instance); });
        RoutineInfo.on(this.clientContext, "afterUpdate", instance => { this.__onRoutineInfoUpdate(instance); });
        RoutineInfo.on(this.clientContext, "afterDestroy", instance => { this.__onRoutineInfoDestroy(instance); });
    }

    /**
     * Callback for the `afterCreate` event of `RoutineInfo`.
     * @param {RoutineInfo} routineInfo The created instance.
     */
    __onRoutineInfoCreate(routineInfo) {
        if (this.__builtinImported) // Prevent wrong initialization of built-in routines.
            this.__initRoutine(routineInfo);
    }

    /**
     * Callback for the `afterUpdate` event of `RoutineInfo`.
     * @param {RoutineInfo} routineInfo The updated instance.
     */
    __onRoutineInfoUpdate(routineInfo) {
        // (Re)start the schedule of the routine if necessary.
        if (routineInfo.enabled && (routineInfo.changed("enabled") || routineInfo.changed("rule"))) {
            this.__startRoutineSchedule(routineInfo);
        }
        // Cancel the schedule if the routine has been disabled.
        else if (!routineInfo.enabled && routineInfo.changed("enabled")) {
            this.__cancelRoutineSchedule(routineInfo);
        }
    }

    /**
     * Callback for the `afterDestroy` event of `RoutineInfo`.
     * @param {RoutineInfo} routineInfo The destroyed instance.
     */
    __onRoutineInfoDestroy(routineInfo) {
        this.__cancelRoutineSchedule(routineInfo);
        // Delete from the routine map.
        if (routineInfo.builtin)
            this.__routines.delete(routineInfo.name);
        else
            this.__customRoutines.delete(routineInfo.uniqueId);
    }

    /**
     * Initialize a routine and starts its schedule if it's enabled.
     * @param {RoutineInfo} routineInfo The RoutineInfo of the routine to initialize.
     */
    __initRoutine(routineInfo) {
        // Add routine to the map if not present.
        if (!routineInfo.builtin && !this.__customRoutines.has(routineInfo.uniqueId)) {
            this.__customRoutines.set(routineInfo.uniqueId, createDummyRoutine(routineInfo));
        }

        // Skip start of schedule if disabled or if the schedule has already been started.
        if (!routineInfo.enabled || this.__jobs.has(routineInfo.uniqueId))
            return;
        
        this.__startRoutineSchedule(routineInfo);
    }

    /**
     * Starts the schedule of a routine.
     * @param {RoutineInfo} routineInfo 
     */
    __startRoutineSchedule(routineInfo) {
        // Make sure to cancel the current schedule if it exists.
        this.__cancelRoutineSchedule(routineInfo);

        const routine = this.getRoutine(routineInfo);
        if (!routine)
            return;
        const rule = { rule: routineInfo.rule, tz: "Europe/Paris" };
        const job = routine.schedule(this.clientContext, routineInfo.guildId, rule);
        this.__jobs.set(routineInfo.uniqueId, job);
    }

    /**
     * Cancel the schedule of a routine.
     * @param {RoutineInfo} routineInfo 
     */
    __cancelRoutineSchedule(routineInfo) {
        const job = this.__jobs.get(routineInfo.uniqueId);
        if (!job)
            return;
        job.cancel();
        this.__jobs.delete(routineInfo.uniqueId);
    }

    /**
     * Get the corresponding routine object.
     * @param {RoutineInfo} routineInfo 
     * @returns {Routine}
     */
    getRoutine(routineInfo) {
        return routineInfo.builtin ? 
            this.__routines.get(routineInfo.name) : 
            this.__customRoutines.get(routineInfo.uniqueId);
    }

    /**
     * @inheritdoc
     */
    async init() {
        // Import all base routines.
        /**
         * @type {Routine[]}
         */
        const routines = fs.readdirSync('./src/routine/routines')
            .filter(file => file.endsWith('.js'))
            .map(file => require(`./routines/${file}`))
            .filter(routine => isRoutine(routine));
        
        // Add base routines in the database if not already there.
        for (let guild of this.clientContext.client.guilds.cache.values()) {
            let guildId = guild.id;
            for (let routine of routines) {
                let uniqueId = RoutineInfo.createUniqueId(guildId, routine.name);
                await RoutineInfo.findOrCreate({
                    where: { uniqueId: uniqueId },
                    defaults: { 
                        uniqueId: uniqueId,
                        name: routine.name,
                        guildId: guildId,
                        enabled: !!routine.enabledByDefault,
                        rule: routine.defaultRule,
                        builtin: true
                    }
                });
            }
        }

        routines.forEach(routine => {
            routine.builtin = true; // Tells whether the Routine object is a built-in one.
            this.__routines.set(routine.name, routine);
        });

        this.__builtinImported = true;

        const routineInfos = await RoutineInfo.findAll();

        routineInfos.forEach(routineInfo => {
            // If the RoutineInfo corresponds to a deleted built-in routine. Removes it.
            if (routineInfo.builtin && !this.isBuiltinRoutine(routineInfo.name)) {
                routineInfo.destroy().catch(_err => logger.error(_err));
                return;
            }
            this.__initRoutine(routineInfo);
        });
    }

    /**
     * @inheritdoc
     * @param {Guild} guild 
     */
    async initNewGuild(guild) {
        // Fetch all built-in routines.
        const guildId = guild.id;
        const routineInfos = [];
        for (let routine of this.__routines.values()) {
            let uniqueId = RoutineInfo.createUniqueId(guildId, routine.name);
            let [info] = await RoutineInfo.findOrCreate({ 
                where: { uniqueId: uniqueId },
                defaults: { 
                    uniqueId: uniqueId,
                    name: routine.name,
                    guildId: guildId,
                    enabled: !!routine.enabledByDefault,
                    rule: routine.defaultRule,
                    builtin: true
                }
            });
            routineInfos.push(info);
        }

        // Initialize them.
        routineInfos.forEach(info => {
            this.__initRoutine(info);
        });
    }

    /**
     * @inheritdoc
     */
    async terminate() {
        // Cancel all pending jobs.
        this.__jobs.forEach(job => {
            job.cancel();
        });
        this.__jobs.clear();
    }

    /**
     * Returns whether the given routine is a built-in one.
     * @param {string} name The name of the routine to check.
     * @returns {boolean}
     */
    isBuiltinRoutine(name) {
        const routine = this.__routines.get(name);
        return !!routine && routine.builtin;
    }

    /**
     * Returns `true` if the given string is a correct Cron.
     * @param {string} rule The string to test.
     * @returns {boolean}
     */
    isCorrectRule(rule) {
        if (typeof rule !== "string")
            return false;
        
        // Best way to check is to create a dummy job.
        const job = scheduleJob(rule, () => {});
        if (!job)
            return false;
        job.cancel();
        return true;
    }

    /**
     * Get the next date of invocation for the routine with the given name.
     * @param {string} guildId The id of the guild.
     * @param {string} name The name of the routine.
     * @returns {Date?} Returns a Date object of the next invocation or `null` if the routine is disabled or non-existent.
     */
    getNextInvocationDate(guildId, name) {
        const job = this.__jobs.get(RoutineInfo.createUniqueId(guildId, name));
        if (!job)
            return null;
        return job.nextInvocation();
    }

    /**
     * Returns all RoutineInfos from the given guild.
     * @param {string} guildId The id of the guild.
     * @param {Promise<RoutineInfo[]>}
     */
    async getAllRoutineInfos(guildId) {
        return await RoutineInfo.findAll({ where: { guildId: guildId } });
    }

    /**
     * Returns the RoutineInfo with the given name.
     * @param {string} guildId The id of the guild.
     * @param {string} name The name of the routine.
     * @returns {Promise<RoutineInfo?>}
     */
    async getOneRoutineInfo(guildId, name) {
        return await RoutineInfo.findByPk(RoutineInfo.createUniqueId(guildId, name));
    }

    /**
     * Create a custom routine for the given guild.
     * @param {string} guildId The id of the guild.
     * @param {string} name The name of the new routine.
     * @param {object} createOptions Parameters to create the routine.
     * @param {CronString} createOptions.rule The cron rule of occurence of the routine.
     * @param {string} createOptions.message The message to post when the routine event occurs.
     * @param {string} createOptions.messageChannelId The id of the channel to write in when the routine event occurs.
     * @param {boolean} createOptions.enabled (optional) Whether the routine is enabled. Default is `false`.
     * @returns {Promise<[boolean, ROUTINE_ERRORS]>} Returns a [success, errorMessage] pair.
     */
    async createCustomRoutine(guildId, name, createOptions) {
        if (!createOptions || !createOptions.rule || !createOptions.message || !createOptions.messageChannelId) {
            return [false, ROUTINE_ERRORS.MISSING_PARAMETERS];
        }

        if (!this.isCorrectRule(createOptions.rule)) {
            return [false, ROUTINE_ERRORS.WRONG_PARAMETERS];
        }

        const uniqueId = RoutineInfo.createUniqueId(guildId, name);

        const options = {};
        Object.assign(options, createOptions);
        options.uniqueId = uniqueId;
        options.guildId = guildId;
        options.name = name;
        options.enabled = !!createOptions.enabled;
        options.builtin = false;

        var isError = false;

        const [_info, created] = await RoutineInfo.findOrCreate({ where: { uniqueId: uniqueId }, defaults: options })
            .catch(_err => { 
                logger.error(_err);
                isError = true;
                return [null, false]; 
            });
        
        if (isError)
            return [false, ROUTINE_ERRORS.CREATION_FAILED];
        
        return [created, created ? ROUTINE_ERRORS.NO_ERROR : ROUTINE_ERRORS.ALREADY_EXISTING_ROUTINE_NAME];
    }

    /**
     * Edit an existing RoutineInfo (built-in or custom) for the given guild.
     * @param {string} guildId The id of the guild.
     * @param {string} name The name of the routine to edit.
     * @param {object} editOptions Parameters to edit the routine.
     * @param {CronString} editOptions.rule (optional) The cron rule of occurence of the routine.
     * @param {string} editOptions.message (optional) The message to post when the routine event occurs.
     * @param {string} editOptions.messageChannelId (optional) The id of the channel to write in when the routine event occurs.
     * @param {boolean} editOptions.enabled (optional) Whether the routine is enabled. Default is `false`.
     * @returns {Promise<[boolean, ROUTINE_ERRORS]>} Returns a [success, errorMessage] pair.
     */
    async editRoutine(guildId, name, editOptions) {
        if (!editOptions) {
            return [false, ROUTINE_ERRORS.MISSING_PARAMETERS];
        }

        // If it's a built-in routine, remove useless options.
        if (this.isBuiltinRoutine(name)) {
            delete editOptions.message;
            delete editOptions.messageChannelId;
        }

        if (!editOptions.rule && !editOptions.message && !editOptions.messageChannelId && 
            (editOptions.enabled === undefined || editOptions.enabled === null)) {
            return [false, ROUTINE_ERRORS.MISSING_PARAMETERS];
        }

        if (editOptions.rule && !this.isCorrectRule(editOptions.rule)) {
            return [false, ROUTINE_ERRORS.WRONG_PARAMETERS];
        }

        const uniqueId = RoutineInfo.createUniqueId(guildId, name);
        cleanObject(editOptions);
        const fields = Object.keys(editOptions);
        var isError = false;

        const [count] = await RoutineInfo.update(editOptions, { where: { uniqueId: uniqueId }, fields: fields, individualHooks: true })
            .catch(_err => {
                logger.error(_err);
                isError = true;
                return [0];
            });
        
        if (isError)
            return [false, ROUTINE_ERRORS.UPDATE_FAILED];
        
        const success = count > 0;
        return [success, success ? ROUTINE_ERRORS.NO_ERROR : ROUTINE_ERRORS.NOT_FOUND];
    }

    /**
     * Delete the routine with the given name.
     * @param {string} guildId The id of the guild.
     * @param {string} name The name of the routine to delete.
     * @returns {Promise<[boolean, ROUTINE_ERRORS]>} Returns a [success, errorMessage] pair.
     */
    async removeCustomRoutine(guildId, name) {
        if (this.isBuiltinRoutine(name)) {
            return [false, ROUTINE_ERRORS.CANT_DELETE_BUILTIN_ROUTINE];
        }

        var isError = false;

        const count = await RoutineInfo.destroy({ where: { uniqueId: RoutineInfo.createUniqueId(guildId, name) }, individualHooks: true })
            .catch(_err => {
                logger.error(_err);
                isError = true;
                return 0;
            });
        
        if (isError)
            return [false, ROUTINE_ERRORS.DELETE_FAILED];
        
        const success = count > 0;
        return [success, success ? ROUTINE_ERRORS.NO_ERROR : ROUTINE_ERRORS.NOT_FOUND];
    }

}

module.exports = {
    RoutineService,
    ROUTINE_ERRORS
}