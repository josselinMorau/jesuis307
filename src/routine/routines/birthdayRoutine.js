const { Job, scheduleJob } = require('node-schedule');
const { ClientContext } = require('../../ClientContext');
const { logger } = require('../../logging/logging');
const { BirthdayService } = require('../../misc/BirthdayService');
const { ChannelService, CHANNEL_CATEGORIES } = require('../../utils/channels/ChannelService');

const DEFAULT_BIRTHDAY_MESSAGE = "Bon anniversaire %n !";

/**
 * @typedef {import('../RoutineService').ScheduleRule} ScheduleRule
 */

/**
 * Callback of this routine.
 * @param {ClientContext} ctx 
 * @param {string} guildId 
 */
async function checkBirthday(ctx, guildId) {
    const guild = await ctx.client.guilds.fetch(guildId);
    const channelService = ctx.getService(ChannelService);
    const channel = await channelService.getChannelWithCategory(guild, CHANNEL_CATEGORIES.BIRTHDAY);
    if (!channel)
        return;
    
    const birthdayService = ctx.getService(BirthdayService);
    const birthdays = await birthdayService.findAllBirthdaysWithDate(guildId, new Date()); // Find all birthday matching today's date.

    for (let birthday of birthdays) {
        let member = await guild.members.fetch(birthday.userId).catch(err => logger.error(err));
        if (!member) {
            birthday.destroy().catch(() => {}); // If the member doesn't exist anymore we can destroy the corresponding model.
            continue;
        }

        const birthdayMsg = await birthdayService.findOneRandomBirthdayMessage(guildId).catch(() => {});
        const content = (birthdayMsg ? birthdayMsg.content : DEFAULT_BIRTHDAY_MESSAGE)
            .replace("%n", member.toString());
        channel.send(content);
    }
}

/**
 * Code of the initial schedule.
 * @param {ClientContext} ctx The application's client context.
 * @param {string} guildId The id of the guild.
 * @param {ScheduleRule} rule The rule for the schedule.
 * @returns {Job} The Job created by the schedule.
 */
function schedule(ctx, guildId, rule) {
    const job = scheduleJob(rule, () => {
        checkBirthday(ctx, guildId).catch(_err => logger.error(_err));
    });
    return job;
}

module.exports = {
    name: "birthday",
    enabledByDefault: true,
    defaultRule: '0 7 * * *', // Every day at 7:00.
    schedule: schedule
}