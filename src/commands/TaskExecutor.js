const { TextChannel } = require('discord.js');
const { BaseService } = require('../BaseService.js');
const { logger } = require('../logging/logging.js');
const { SettingService, SETTING_NAMES } = require('../utils/guildSettings/SettingsService.js');

/**
 * @enum {string} Enumeration of all the kind of tasks that can be executed by the TaskExecutor. 
 */
const TASK_TYPES = {
    MANAGE_MESSAGES: "MANAGE_MESSAGES", // For message copy, deletion, etc...
    MANAGE_ROLES: "MANAGE_ROLES" // For role upgrade etc...
}

/**
 * @typedef {object} Task An object representing the status of a task executed by the TaskExecutor.
 * @property {Promise} promise The promise associated to the task.
 * @property {Date} startTime A date of the start of the execution of the task.
 * @property {Date?} endTime A date of the end of the execution of the task. Undefined or null if the task is not finished yet.
 * @property {boolean} finished Whether the task is finished.
 * @property {'pending' | 'success' | 'fail'} status The status of the task.
 */

/**
 * Class used to manage the execution of complex tasks.
 */
class TaskExecutor extends BaseService
{
    /**
     * 
     * @param {*} ctx 
     */
    constructor(ctx) {
        super(ctx);

        /**
         * @type {Map<string, Map<TASK_TYPES, Task>>}
         */
        this.__taskMap = new Map();
    }

    /**
     * Inner method to fetch the map of tasks for a given guild.
     * @param {string} guildId 
     * @returns {Map<TASK_TYPES, Task>}
     */
    __getGuildTaskMap(guildId) {
        // Lazy instanciation.
        var map = this.__taskMap.get(guildId);
        if (!map) {
            map = new Map();
            this.__taskMap.set(guildId, map);  
        }
        return map;
    }
    
    /**
     * Inner implementation of `tryExecuteTask`.
     * @param {string} guildId The id of the guild to execute the task in.
     * @param {TASK_TYPES} taskType The type of task to execute.
     * @param {number} cooldown The cooldown for the task in ms.
     * @param {(...params) => Promise<any>} taskFunc The task to execute. It must be asynchronous.
     * @param  {...any} params The parameters to pass to the task.
     * @returns {Promise<[boolean, Task]>} A [boolean, Task] pair. The first value indicate if the given task has started execution.
     * The second corresponds to the currently running Task corresponding to the given values.
     */
    async __tryExecuteTask(guildId, taskType, cooldown, taskFunc, ...params) {
        const map = this.__getGuildTaskMap(guildId);
        const currentTask = map.get(taskType);
        const now = new Date();
        if (currentTask && !!currentTask.promise && (!currentTask.finished || now - currentTask.endTime < cooldown)) {
            return [false, currentTask];
        }

        /**
         * @type {Task}
         */
        const task = {
            startTime: now,
            endTime: null,
            finished: false,
            status: 'pending',
            promise: null
        };

        map.set(taskType, task);
        task.promise = taskFunc(...params);
        task.promise.then(() => {
            task.status = 'success';
        })
        .catch(_err => {
            logger.error(_err);
            task.status = 'fail';
        })
        .finally(() => {
            task.finished = true;
            task.endTime = new Date();
        });
        return [true, task];
    }

    /**
     * Same as `tryExecuteTask` but with an automatically added default callback to the `then` method. 
     * @param {TextChannel} channel The channel to send messages to. The guild id is also parsed from this.
     * @param {TASK_TYPES} taskType The type of task to execute.
     * @param {(...params) => Promise<any>} taskFunc The task to execute. It must be asynchronous.
     * @param  {...any} params The parameters to pass to the task.
     */
    async tryExecuteTaskWithDefaultCallback(channel, taskType, taskFunc, ...params) {
        const settingService = this.clientContext.getService(SettingService);
        const cooldown = 1000 * await settingService.getSetting(channel.guildId, SETTING_NAMES.TASK_COOLDOWN);
        await this.__tryExecuteTask(channel.guildId, taskType, cooldown, taskFunc, ...params)
            .then(this.createDefaultCallback(channel, cooldown));
    }

    /**
     * Try to execute the given task. If a task of the same type is already running, the task is not executed.
     * @param {string} guildId The id of the guild to execute the task in.
     * @param {TASK_TYPES} taskType The type of task to execute.
     * @param {(...params) => Promise<any>} taskFunc The task to execute. It must be asynchronous.
     * @param  {...any} params The parameters to pass to the task.
     * @returns {Promise<[boolean, Task]>} A [boolean, Task] pair. The first value indicate if the given task has started execution.
     * The second corresponds to the currently running Task corresponding to the given values.
     */
    async tryExecuteTask(guildId, taskType, taskFunc, ...params) {
        const settingService = this.clientContext.getService(SettingService);
        const cooldown = 1000 * await settingService.getSetting(guildId, SETTING_NAMES.TASK_COOLDOWN);
        return await this.__tryExecuteTask(guildId, taskType, cooldown, taskFunc, ...params);
    }

    /**
     * Creates the default `then` callback to `tryExecuteTask`.
     * @param {TextChannel} channel The channel to send messages to.
     * @param {number} cooldown The cooldown of the task in ms.
     */
    createDefaultCallback(channel, cooldown) {
        return ([executing, task]) => {
            if (!executing) {
                if (task && task.finished) {
                    const elapsedTime = new Date() - task.endTime;
                    const seconds = Math.max(0, Math.floor((cooldown - elapsedTime) / 1000));
                    channel.send(`Une opération similaire vient de se terminer. Tu pourras essayer cette commande dans ${seconds} seconde(s).`);
                }
                else
                    channel.send("Une opération similaire en cours. Réessaye plus tard.");
            }
        };
    }
}

module.exports = {
    TaskExecutor,
    TASK_TYPES
};