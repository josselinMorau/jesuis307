const { Message, Permissions, TextChannel, ThreadChannel, MessageEmbed } = require('discord.js');
const { Aki } = require('aki-api');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { waitForUserAnswer, CONSTANTS } = require('../../utils/helper.js');
const { CacheService } = require('../../misc/CacheService.js');

const TIMEOUT = 5 * 60_000; // 5 min
const CACHE_KEY = "AKI_GAMES";

/**
 * @class Class to handle a game of Akinator.
 */
class AkiGame {
    /**
     * 
     * @param {ClientContext} ctx The `ClientContext` in which the game was started.
     * @param {Message} msg The message of the command that started the game.
     */
    constructor(ctx, msg) {
        this.gameId = msg.id;
        this.player = msg.member;
        this.channel = msg.channel;
        this.shouldStop = false;
        this.shouldTerminate = false;
        this.ctx = ctx;
        /**
         * Callback registered to `ClientContext.onceTerminated`.
         */
        this.terminatedCallback = () => {
            this.shouldTerminate = true;
        }

        // Make the game stop if the client is killed.
        this.ctx.onceTerminated(this.terminatedCallback);
    }

    /**
     * Called once the game is over.
     */
    terminate() {
        if (this.ctx)
            this.ctx.offTerminated(this.terminatedCallback);
    }

    /**
     * Handles the game loop.
     */
    async gameLoop() {
        const akiGames = this.ctx.getService(CacheService).get(this.channel.guildId, CACHE_KEY);
        akiGames.set(this.channel.id, this);
        const aki = new Aki({ region: 'fr', childMode: true });
        var isWin = false;
        var questionCount = 0;
        await aki.start();
        while (!this.shouldStop) {
            const choices = ["Oui", "Non", "Ne sais pas", "Probablement", "Probablement pas"];
            if (aki.currentStep > 0) // Can't go back if it's the first question.
                choices.push({ label: "Retour", style: "DANGER" });
            choices.push({ label: "Stop", style: "DANGER" });
            
            const [answer, confirmMsg] = await waitForUserAnswer(this.player, this.channel, TIMEOUT, { content: `**${aki.question}**` }, choices);
            if (!answer || answer === "Stop" || this.shouldTerminate) {
                this.shouldStop = true; // TIMEOUT or STOP : quit the loop
                continue;
            }

            if (answer === "Retour") {
                await aki.back();
                --questionCount;
                continue;
            }

            ++questionCount;

            // Change color of the user's answer.
            const components = confirmMsg.components;
            components.forEach(row => {
                row.components.forEach(comp => {
                    if (comp.type === "BUTTON" && comp.label === answer)
                        comp.setStyle("SUCCESS");
                    comp.setDisabled(true);
                });
            });
            await confirmMsg.edit({ components: components });

            await aki.step(choices.indexOf(answer));
            // Arbitrary check. Either the level of confidence is enough or the game runs for too long...
            if (aki.progress >= 85 || aki.currentStep >= 78) {
                isWin = true;
                this.shouldStop = true;
            }
        }

        if (this.shouldTerminate)
            return;

        if (isWin) {
            await aki.win();
            var lose = true;
            const guesses = aki.answers;
            for (let guess of guesses) {
                // Process guesses.
                const embed = new MessageEmbed()
                    .setTitle(`Tu pensais à : ${guess.name} ?`)
                    .setDescription(guess.description || '\u200b') // \u200b is empty character to prevent empty string errors
                    .setImage(guess.absolute_picture_path)
                    .setColor(CONSTANTS.COLOR_INFO);
                const [answer] = await waitForUserAnswer(this.player, this.channel, TIMEOUT, { embeds: [embed] }, ["Oui", "Non"]);
                if (!answer || this.shouldTerminate) {
                    lose = false;
                    this.channel.send("La partie a été annulée.");
                    break;
                }

                if (answer === "Oui") {
                    lose = false;
                    this.channel.send(`J'ai gagné en ${questionCount} question${questionCount > 0 ? 's' : ''} ! :partying_face:`);
                    break;
                }
            }

            if (lose)
                this.channel.send("Tu m'as posé une colle...");
        }
        else {
            this.channel.send("La partie a été annulée.");
        }
        
        const currentGame = akiGames.get(this.channel.id);
        // Check if we didn't insert another game with this key.
        if (currentGame && currentGame.gameId === this.gameId) {
            akiGames.delete(this.channel.id);
        }
        this.terminate();
    }
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const cacheService = ctx.getService(CacheService);
    var akiGames = cacheService.get(msg.guildId, CACHE_KEY);
    if (!akiGames) {
        akiGames = new Map();
        cacheService.set(msg.guildId, CACHE_KEY, akiGames);
    }
    if (akiGames.has(msg.channelId)) {
        msg.channel.send("Une partie est encore en cours dans ce channel.");
        return;
    }

    const newGame = new AkiGame(ctx, msg);
    newGame.gameLoop().catch(err => {
        logger.error(err);

        const currentGame = akiGames.get(newGame.channel.id);
        if (currentGame && currentGame.gameId === newGame.gameId)
            akiGames.delete(newGame.channel.id);
    });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "akinator", // The command's name.
    aliases: ["aki"], // A list of aliases for the command.
    usage: "akinator", // The usage of the command.
    description: "Démarre une partie d'Akinator dans le channel en cours.", // The command's description.
    examples: [], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
