const { Message, Permissions, MessageEmbed } = require('discord.js');
const { default: axios } = require('axios');
const { JSDOM } = require('jsdom');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { CONSTANTS, unaccent, randomElement } = require('../../utils/helper.js');
const { CacheService } = require('../../misc/CacheService.js');

const TIMEOUT = 5 * 60_000; // 5 min
const CACHE_KEY = "MOTUS_GAMES";
const DEFAULT_COUNT = 6;
const DEFAULT_TRIES = 6;
const POSSIBLE_LETTERS = "abcdefghijlmnoprstuv";

function letterToEmoji(letter) {
    return `:regional_indicator_${letter}:`
}

// /**
//  * Fetch a list of words
//  * @param {number} count The number of letters of the words.
//  * @param {string} firstLetter The first letter of the words.
//  * @returns {Promise<string[]>}
//  */
// async function fetchWordList(count, firstLetter) {
//     const resp = await axios.get(`https://www.liste-de-mots.com/mots-nombre-lettre/${count}/${firstLetter}/`)
//         .catch(err => logger.error(err));
//     if (!resp)
//         return [];
//     const doc = new JSDOM(resp.data).window.document;
//     const elem = [...doc.querySelectorAll('p').values()].find(elem => elem.className === "liste-mots");
//     if (!elem || elem.textContent.includes("Aucun mot ne correspond à votre recherche. Désolé."))
//         return [];
//     return elem.textContent.split(',').map(word => unaccent(word).trim().toLowerCase());
// }

/**
 * Fetch a list of words
 * @param {number} count The number of letters of the words.
 * @param {string} firstLetter The first letter of the words.
 * @returns {Promise<string[]>}
 */
async function fetchWordList(count, firstLetter) {
    const fetchFunc = async (page = 0) => {
        const pageStr = page > 1 ? `page${page}` : "";
        const resp = await axios.get(`https://www.listesdemots.net/d/${firstLetter}/1/mots${count}lettresdebutant${firstLetter}${pageStr}.htm`).catch(err => logger.error(err));
        if (!resp)
            return [];
        const doc = new JSDOM(resp.data).window.document;
        const span = [...doc.querySelectorAll('span').values()].find(elem => elem.className === "mot");
        return [[...span.childNodes.values()]
            .filter(node => node.textContent.trim().toLowerCase() !== firstLetter)
            .map(node => firstLetter + unaccent(node.textContent.trim()).toLowerCase()), doc];
    };
    const [words, doc] = await fetchFunc();
    const pageParagraph = [...doc.querySelectorAll('p').values()]
        .find(elem => {
            const content = elem.textContent.toLowerCase();
            return content.includes("pages") && content.includes(":");
    });
    if (pageParagraph) {
        const maxPage = parseInt(pageParagraph.textContent.split(/\s/).filter(str => !!str).at(-1));
        if (maxPage && maxPage > 1) {
            for (let i=2; i<=maxPage; ++i) {
                const [words2] = await fetchFunc(i);
                words.push(...words2);
            }
        }
    }
    return words;
}

class MotusGame
{
    /**
     * 
     * @param {ClientContext} ctx The `ClientContext` in which the game was started.
     * @param {Message} msg The message of the command that started the game.
     */
    constructor(ctx, msg)
    {
        this.gameId = msg.id;
        this.ctx = ctx;
        this.channel = msg.channel;
        this.player = msg.member;
        this.shouldStop = false;
        this.shouldTerminate = false;
        this.word = "";
        /**
         * @type {string[]}
         */
        this.guesses = [];
        /**
         * @type {Message}
         */
        this.lastDisplayMsg = null;
        this.lastContent = "";
        this.validLetters = [];
        this.maxTries = 0;
        this.tries = 0;

        /**
         * Callback registered to `ClientContext.onceTerminated`.
         */
        this.terminatedCallback = () => {
            this.shouldTerminate = true;
        }

        // Make the game stop if the client is killed.
        this.ctx.onceTerminated(this.terminatedCallback);
    }

    terminate() {
        this.ctx.offTerminated(this.terminatedCallback);

        const motusGames = this.ctx.getService(CacheService).get(this.channel.guildId, CACHE_KEY);
        const currentGame = motusGames.get(this.channel.id);
        // Check if we didn't insert another game with this key.
        if (currentGame && currentGame.gameId === this.gameId) {
            motusGames.delete(this.channel.id);
        }
    }

    async displayStatus(finished = false) {
        if (this.lastDisplayMsg) {
            this.lastDisplayMsg.delete().catch(_err => {});
        }

        const lost = this.tries > this.maxTries;
        
        const embed = new MessageEmbed()
            .setColor(CONSTANTS.COLOR_INFO)
            .setTitle(`Motus ${lost ? "💀" : this.tries}/${this.maxTries}`)
            .setDescription(`${this.lastContent || `\u200b`}`); // prevent empty string errors
        if (!finished) {
            const validLettersDisplay = this.validLetters
                .map(letter => letter === " " ? "⬛" : letterToEmoji(letter))
                .join("\u200b"); // Have to insert an empty character between or else flag emojis will be displayed instead
            embed.addField("Lettres bien placées:", validLettersDisplay);
        }
        else if (lost) {
            embed.addField("La bonne réponse était:", Array.from(this.word).map(char => letterToEmoji(char)).join("\u200b"));
        }
        this.lastDisplayMsg = await this.channel.send({ embeds: [embed] });
    }

    /**
     * Handles the game loop.
     * @param {object} options
     * @param {number} options.count The number of letters for the word to guess.
     * @param {string} options.firstLetter The first letter for the word to guess.
     * @param {number} options.tries The number of tries to guess the word.
     */
    async gameLoop(options) {
        const motusGames = this.ctx.getService(CacheService).get(this.channel.guildId, CACHE_KEY);
        motusGames.set(this.channel.id, this);

        const wordList = await fetchWordList(options.count, options.firstLetter);
        if (wordList.length === 0) {
            this.channel.send("Aucun mot ne correspond aux critères donnés.");
            this.terminate();
            return;
        }
        
        this.maxTries = options.tries;
        this.word = randomElement(wordList);
        this.validLetters = Array(this.word.length).fill(" ");
        this.validLetters[0] = this.word.at(0);
        const filterMsg = (msg) => msg.author.id === this.player.id;
        while (!this.shouldStop && !this.shouldTerminate) {
            await this.displayStatus();
            ++this.tries;
            // Fetch answer.
            var answer = null;
            do {
                const fetched = await this.channel.awaitMessages({ filter: filterMsg, max: 1, time: TIMEOUT, errors: ["time"] })
                    .catch(_reason => {});
                if (!fetched || this.shouldTerminate) {
                    this.shouldTerminate = true;
                    this.channel.send("La partie a été annulée pour trop longue inactivité.");
                    break;
                }
                const content = unaccent(fetched.first().content.trim().split(/\s/).filter(str => !!str).at(0)).toLowerCase();
                if (content.length !== this.word.length) {
                    this.channel.send(`Le mot doit faire ${this.word.length} lettres.`);
                }
                else if (!content.startsWith(this.word.at(0))) {
                    this.channel.send(`Le mot doit commencer par la lettre ${this.word.at(0)}.`);
                }
                else if (!wordList.includes(content)) {
                    this.channel.send("Le mot donné n'est pas dans mon dictionnaire.");
                }
                else {
                    answer = content;
                }
            } while (!answer);

            if (!answer)
                break;

            // Check the answer.
            const STATE = { NO_MATCH: 0, MISPLACED: 1, MATCH: 2 };
            const guess = Array(this.word.length).fill(STATE.NO_MATCH);
            const letterMap = {};
            for (let i=0; i<this.word.length; ++i) {
                letterMap[this.word.at(i)] = (letterMap[this.word.at(i)] || 0) + 1;
            }

            for (let i=0; i<this.word.length; ++i) {
                const ref = this.word.at(i);
                const letter = answer.at(i);

                if (letter === ref) {
                    this.validLetters[i] = ref;
                    guess[i] = STATE.MATCH;
                    letterMap[ref] -= 1;
                }
                else if (letterMap[letter]) {
                    let indexOf = -1;
                    let isMisplaced = false;
                    do {
                        indexOf = this.word.indexOf(letter, indexOf+1);
                        if (indexOf !== -1 && this.word.at(indexOf) !== answer.at(indexOf)) {
                            isMisplaced = true;
                            letterMap[letter] -= 1;
                        }
                    } while (indexOf !== -1 && !isMisplaced);

                    if (isMisplaced) {
                        guess[i] = STATE.MISPLACED;
                    }
                }
            }

            const STATE_EMOJI_MAP = { [STATE.NO_MATCH]: "🟦", [STATE.MISPLACED]: "🟠", [STATE.MATCH]: "🟥"  }

            this.lastContent += Array.from(answer).map(char => letterToEmoji(char)).join("\u200b") + "\n"; // Have to insert an empty character between or else flag emojis will be displayed instead
            this.lastContent += guess.map(x => STATE_EMOJI_MAP[x]).join("") + "\n";
            
            if (guess.every(x => x === STATE.MATCH)) {
                this.shouldStop = true;
                this.channel.send("Bravo. :partying_face:");
                break;
            }
            
            if (this.tries >= this.maxTries) {
                this.tries = this.maxTries + 1; // To indicate that the player lost.
                this.shouldStop = true;
                break;
            }
        }

        if (!this.shouldTerminate)
            await this.displayStatus(true);

        this.terminate();
    }
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const cacheService = ctx.getService(CacheService);
    var motusGames = cacheService.get(msg.guildId, CACHE_KEY);
    if (!motusGames) {
        motusGames = new Map();
        cacheService.set(msg.guildId, CACHE_KEY, motusGames);
    }
    if (motusGames.has(msg.channelId)) {
        msg.channel.send("Une partie est encore en cours dans ce channel.");
        return;
    }

    const count = kwargs.count ? parseInt(kwargs.count) : DEFAULT_COUNT;
    if (!isFinite(count) || !count || count < 3) {
        msg.channel.send("Le nombre de lettres est invalide.");
        return;
    }

    const firstLetter = kwargs.letter && kwargs.letter.length ? kwargs.letter.at(0).at(0).toLowerCase() : randomElement(POSSIBLE_LETTERS);
    const charCode = (firstLetter.charCodeAt(0) - "a".charCodeAt(0));
    if (charCode < 0 || charCode > 25) {
        msg.channel.send("La lettre n'est pas valide.");
        return;
    }

    const tries = kwargs.tries ? parseInt(kwargs.tries) : DEFAULT_TRIES;
    if (!isFinite(tries) || !tries || tries <= 0) {
        msg.channel.send("Le nombre d'essais est invalide.");
        return;
    }

    const newGame = new MotusGame(ctx, msg);
    newGame.gameLoop({ count: count, firstLetter: firstLetter, tries: tries }).catch(err => {
        logger.error(err);
        
        newGame.terminate();
        msg.channel.send("Une erreur est survenue. La partie est annulée.");
    });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "motus", // The command's name.
    aliases: ["sutom", "wordle"], // A list of aliases for the command.
    usage: "motus [--count <count>] [--letter <firstLetter>] [--tries <tries>]", // The usage of the command.
    description: "Démarre une partie de motus dans le channel en cours.", // The command's description.
    examples: [
        {
            usage: "motus",
            description: "Démarre une partie de motus avec les options par défaut."
        },
        {
            usage: "motus --count 8",
            description: "Démarre une partie de motus avec un mot de 8 lettres. Si non précisé le mot fait 6 lettres."
        },
        {
            usage: "motus --letter f",
            description: "Démarre une partie de motus avec un mot commençant par f. Si non précisé la lettre initiale est tirée au hasard."
        },
        {
            usage: "motus --tries 7",
            description: "Démarre une partie de motus avec 7 essais. Si non précisé il y a 6 essais."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
