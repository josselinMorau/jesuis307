const { Message, Permissions, TextChannel } = require('discord.js');
const { JSDOM } = require('jsdom');
const { imageHash } = require('image-hash');
const { default: axios } = require('axios');

const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { randomElement, randomRangeInt } = require('../../utils/helper.js');

const SUPPORTED_EXTENSIONS = ['png', 'jpg', 'jpeg', 'webp'];

/**
 * @typedef {object} SearchResult
 * @property {string} link
 * @property {string} preview
 */

/**
 * 
 * @param {string} html 
 * @returns {SearchResult[]}
 */
function getResults(html) {
    const doc = new JSDOM(html).window.document;
    return [...doc.querySelectorAll('a').values()].map(aEl => {
        const img = aEl.querySelector('img');
        if (!img || !img.src || !img.src.startsWith('http'))
            return null;
        const res = /\/url\?q=(.*)/gm.exec(aEl.href);
        if (!res || !res[1])
            return null;
        try {
            const uri = decodeURIComponent(res[1]);
            const ind = uri.indexOf('&');
            const href = ind !== -1 ? uri.slice(0, ind) : uri;
            return {
                link: decodeURIComponent(href),
                preview: img.src
            }
        }
        catch (_err) {
            return null;
        }
    })
    .filter(elem => !!elem);
}

/**
 * 
 * @param {string} hash1 
 * @param {string} hash2 
 */
 function hammingDistanceRatio(hash1, hash2) {
    var dist = 0;
    for (let i=0; i<hash1.length; ++i) {
        if (hash1[i] !== hash2[i])
            ++dist;
    }
    return dist / hash1.length;
}

/**
 * 
 * @param {object} input
 * @param {string?} input.ext
 * @param {string?} input.name
 * @param {Buffer} input.data 
 * @param {number} bits 
 */
async function hash(input, bits) {
    const [error, data] = await new Promise(resolve => imageHash(input, bits, true, (err,data) => { resolve([err,data]); }));
    if (error)
        throw error;
    return data;
}

/**
 * 
 * @param {string} url 
 */
function filenameFromUrl(url) {
    return new URL(url).pathname.split('/').at(-1);
}

/**
 * 
 * @param {string} url 
 * @param {string} imageUrl 
 * @property {number} imageRatio
 * @returns {Promise<string>}
 */
async function findClosestImage(url, imageUrl) {
    // Constants.
    const bits = 16;
    const threshold = 0.35;

    // First compute original image hash
    const ogBuf = await axios.get(imageUrl, { responseType: 'arraybuffer' }).then(resp => Buffer.from(resp.data, 'binary'));
    const ogHash = await hash({ data: ogBuf, ext: 'image/jpeg', name: "input.jpg" /* Workaround to avoid annoying logs.*/ }, bits);

    const resp = await axios.get(url);
    const doc = new JSDOM(resp.data, {  }).window.document;
    const imgElements = doc.querySelectorAll('img');
    // iterate over all img nodes.
    for (let imgEl of imgElements.values()) {
        if (!imgEl.src)
            continue;
        
        // Create absolute path if necessary.
        let src = imgEl.src.startsWith('http') ? imgEl.src : 
            new URL(imgEl.src, url).toString();
        let filename = filenameFromUrl(src);
        
        // Ignore unsupported file extensions.
        if (!filename || SUPPORTED_EXTENSIONS.findIndex(ext => filename.endsWith(ext)) === -1)
            continue;
        
        let buf = await axios.get(src, { responseType: 'arraybuffer' })
            .then(resp => Buffer.from(resp.data, 'binary'))
            .catch(() => {});
        
        if (!buf)
            continue;
        
        let imgHash = await hash({ data: buf, name: filename }, bits).catch(() => {});
        if (!imgHash)
            continue;
        
        // If the distance between the two hashes is acceptable, returns the corresponding link.
        if (hammingDistanceRatio(ogHash, imgHash) <= threshold) {
            return src;
        }
    }

    return null;
}

/**
 * Search
 * @param {TextChannel} channel 
 * @param {string} query 
 * @param {boolean} big
 */
async function searchImg(channel, query, big) {
    const page = randomRangeInt(0,2);
    const resp = await axios.get(`https://www.google.com/search`, {
        params: {
            tbs: `imgo:${page}`, // Search page
            tbm: "isch", // Search images
            hl: "fr", // Language is french
            safe: "active", // To enable SFW mode
            q: query
        }
    });

    const results = getResults(resp.data);
    if (!results || results.length === 0) {
        channel.send("¯\\_(ツ)_/¯");
        return;
    }

    const searchResult = randomElement(results);
    // Look for the original image only if the --big option was passed.
    const img = big ? ((await findClosestImage(searchResult.link, searchResult.preview).catch(() => {})) || searchResult.preview) : 
        searchResult.preview;
    channel.send(img);
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const query = args.map(arg => encodeURIComponent(arg)).join('+');
    if (!query) {
        return msg.channel.send("Tu n'as pas précisé quoi chercher.");
    }
    const big = !!kwargs.big;
    searchImg(msg.channel, query, big)
        .catch(_err => logger.error(_err));
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "img", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "img <query...> [--big]", // The usage of the command.
    description: "Cherche et affiche une image au hasard.", // The command's description.
    examples: [
        {
            usage: "img chien mignon",
            description: "Fais la recherche 'chien mignon' et affiche une image au hasard parmi les résultats."
        },
        {
            usage: "img chien mignon --big",
            description: "Fais la recherche 'chien mignon' et affiche une image au hasard parmi les résultats.\n" + 
                "Utiliser --big est à la fois moins sûr et moins rapide mais permet d'afficher l'image dans sa taille originale."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
