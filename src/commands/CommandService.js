const fs = require('fs');
const { PermissionFlags, Message, GuildMember, Permissions, Channel, Guild } = require('discord.js');
const { TERMINAL_COLORS, getFormatStringArgCount, format } = require('../utils/helper.js');
const { CommandPermission } = require('../database/models/CommandPermission.js');
const { CustomAliasInfo } = require('../database/models/CustomAliasInfo.js');
const { CustomCommandInfo } = require('../database/models/CustomCommandInfo.js');
const { logger } = require('../logging/logging.js');
const { options } = require('../../options.js');
const { MemberInfo } = require('../database/models/MemberInfo.js');
const { RoleInfo } = require('../database/models/RoleInfo.js');
const { BaseService } = require('../BaseService.js');
const { JailService } = require('../misc/JailService.js');
const { ChannelService, CHANNEL_CATEGORIES } = require('../utils/channels/ChannelService.js');
const { SettingService, SETTING_NAMES, SPAM_MODE_VALUES } = require('../utils/guildSettings/SettingsService.js');

/**
 * @typedef {object} UsageExample An object containing infos on a specific example of a command's usage.
 * @property {string} usage The usage of the command for the current example.
 * @property {string} description The description of the specific usage.
 * @property {boolean} noPrefix (optional) If true the usage won't be displayed with the prefix.
 */

/**
 * @typedef {"any" | "all" | undefined} FlagOp
 */

/**
 * @typedef {"builtin" | "alias" | "custom"} CommandType
 */

/**
 * @typedef {object} CommandHelp An object containing infos on the command.
 * @property {string} name The command's name.
 * @property {string} category The name of the command's category. This field is deduced from the command folder name.
 * @property {string[]} aliases A list of alias names for the command.
 * @property {string} usage The usage of the command.
 * @property {string} description The command's description.
 * @property {UsageExample[]} examples A list of specific examples of usage of the command.
 * @property {object} permissions An object to encapsulate all the necessary permissions to run the command.
 * @property {PermissionFlags} permissions.flag The Discord's flag that allow an user to run the command. The flag is a bitfield of values in `Permission.FLAGS`. 
 * @property {FlagOp} permissions.op The type of operation when checking the permissions. If undefined no check is done.
 * @property {boolean} delete Whether the message of the command should be deleted.
 * @property {boolean} onTest Whether the command should only be available in testing mode.
 */

/**
 * @typedef {import('../ClientContext').ClientContext} ClientContext
 */


/**
 * @typedef {object} Command The template type for a built-in Command object.
 * @property {(ctx : ClientContext, msg : Message, args : string[], kwargs : Object<string, string[]>) => void} run The code of the command. 
 * @property {CommandHelp} help An object containing infos on the command.
 * @property {CommandType} type The type of command. Always equals to "builtin".
 */

/**
 * @typedef {object} CommandArgs
 * @property {string[]} args The list of arguments.
 * @property {Object<string,string[]>} kwargs The set of keyword arguments.
 */


/**
 * @typedef {object} CustomAlias The template type for a CommandAlias object.
 * @property {CommandHelp} help Copy of the original command's help.
 * @property {string} guildId The id of the guild the alias is defined in.
 * @property {string} aliasName The name of the alias.
 * @property {string} aliasArgs The raw string to format to get the default arguments of the custom alias.
 * @property {number} aliasArgCount The number of expected replacement arguments for an alias.
 * @property {Command} originalCmd The original command the alias points to.
 * @property {CommandType} type The type of command. Always equals to "alias".
 */

/**
 * @typedef {object} CustomCommand
 * @property {CommandHelp} help An auto generated help.
 *  * @property {string} guildId The id of the guild the command is defined in.
 * @property {CommandType} type The type of command. Always equals to "custom".
 * @property {string} content The content that should be posted for the command.
 */

/**
 * Creates and returns a custom alias object.
 * @param {Command} originalCmd The original command the alias points to.
 * @param {string} guildId The id of the guild the alias is defined in.
 * @param {string} aliasName The name of the alias.
 * @param {string} aliasArgs The raw string to format to get the default arguments of the custom alias.
 * @returns {CustomAlias}
 */
function createCustomAliasObject(guildId, originalCmd, aliasName, aliasArgs) {
    return {
        help: originalCmd.help,
        guildId: guildId,
        aliasName: aliasName,
        aliasArgs: aliasArgs,
        aliasArgCount: getFormatStringArgCount(aliasArgs),
        originalCmd: originalCmd,
        type: "alias"
    };
}

/**
 * Creates a custom command object.
 * @param {CustomCommandInfo} customCommand The corresponding data model.
 * @returns {CustomCommand}
 */
function createCustomCommandObject(customCommand) {
    return {
        type: "custom",
        guildId: customCommand.guildId,
        content: customCommand.content,
        help: {
            category: "custom_command",
            delete: customCommand.deleteMsg,
            description: "Une commande custom définie sur ce serveur.",
            usage: customCommand.name,
            name: customCommand.name,
            permissions: {
                flag: Permissions.DEFAULT,
                op: null
            }
        }
    };
}

/**
 * @enum {string} Enum of all the possibles error codes when manipulating commands.
 */
const COMMAND_ERRORS = {
    NO_ERROR: "NO_ERROR",
    NO_SUCH_COMMAND: "NO_SUCH_COMMAND",
    NO_SUCH_ALIAS: "NO_SUCH_ALIAS",
    NO_SUCH_CUSTOM_COMMAND: "NO_SUCH_CUSTOM_COMMAND",
    INVALID_PARAMS: "INVALID_PARAMS",
    ALREADY_EXISTING_BUILTIN_COMMAND: "ALREADY_EXISTING_BUILTIN_COMMAND",
    ALREADY_EXISTING_CUSTOM_COMMAND: "ALREADY_EXISTING_CUSTOM_COMMAND",
    ALREADY_EXISTING_ALIAS_COMMAND: "ALREADY_EXISTING_ALIAS_COMMAND",
    INVALID_OPERATION_ON_BUILTIN_COMMAND: "INVALID_OPERATION_ON_BUILTIN_COMMAND",
    INVALID_OPERATION_ON_CUSTOM_COMMAND: "INVALID_OPERATION_ON_CUSTOM_COMMAND",
    INVALID_OPERATION_ON_ALIAS_COMMAND: "INVALID_OPERATION_ON_ALIAS_COMMAND",
    CREATE_FAILED: "CREATE_FAILED",
    ADD_FAILED: "ADD_FAILED",
    DELETE_FAILED: "DELETE_FAILED"
};

class CommandService extends BaseService
{
    constructor(ctx) {
        super(ctx);

        /**
         * @type {Set<string>} A set of the existing category names.
         */
        this.__categories = new Set();
        /**
         * @type {Map<string, Command>} The map for the base commands.
         */
        this.__commands = new Map();
        /**
         * @type {Map<string, CustomCommand>}
         */
        this.__customCommands = new Map();
        /**
         * @type {Map<string, CustomAlias>} The map for the custom aliases.
         */
        this.__customAliases = new Map();
    }

    /**
     * Returns an array of all the command categories.
     * @returns {string[]}
     */
    getCategories() {
        return [...this.__categories.values()];
    }

    /**
     * Returns whether the given category name exists.
     * @param {string} category 
     */
    isCategory(category) {
        return this.__categories.has(category);
    }

    /**
     * Returns whether the given command name exists.
     * @param {string} guildId
     * @param {string} command 
     */
    isCommand(guildId, command) {
        return !!this.getCommand(guildId, command);
    }

    /**
     * @inheritdoc
     */
    async init() {        
        const dirsNames = fs.readdirSync("./src/commands", { withFileTypes: true })
            .filter(dirent => dirent.isDirectory())
            .map(dirent => dirent.name);
        
        for (let dir of dirsNames) {
            const commandsFiles = fs.readdirSync(`./src/commands/${dir}`)
                .filter(file => file.endsWith(".js"));
            for (let file of commandsFiles) {
                const command = require(`./${dir}/${file}`);
                if (!command || !command.help || !command.run)
                    continue;
                command.help.category = dir;
                command.type = "builtin";
                this.__categories.add(dir);
                this.__commands.set(command.help.name, command);

                // Import each alias as well.
                if (command.help.aliases)
                    command.help.aliases.forEach(aliasName => { this.__commands.set(aliasName, command); });
            }
        }

        // Import custom commands.
        const customCommands = await CustomCommandInfo.findAll();
        for (let customCmd of customCommands) {
            let customObj = createCustomCommandObject(customCmd);
            this.__customCommands.set(customCmd.uniqueId, customObj);
        }

        // Import custom aliases.
        const aliases = await CustomAliasInfo.findAll();
        for (let alias of aliases) {
            let originalCmd = this.__commands.get(alias.commandName);
            if (!originalCmd) {
                // Get rid of expired aliases.
                alias.destroy().catch(_err => logger.error(_err));
                continue;
            }
            let cmdAlias = createCustomAliasObject(alias.guildId, originalCmd, alias.aliasName, alias.aliasArgs);
            this.__customAliases.set(alias.uniqueId, cmdAlias);
        }
    }

    /**
     * Try to execute a command.
     * @param {string} prefix The command prefix.
     * @param {Message} msg The message to parse.
     * @returns {Promise<boolean>} Returns `true` if the message was recognized as a command.
     */
    async tryExecuteCommand(prefix, msg) {
        
        if (!msg.content.startsWith(prefix))
            return false;
        
        const { commandName, rawArgs } = this.parseCommandNameAndRawArgs(prefix, msg);

        const command = this.getCommand(msg.guildId, commandName);

        if (!command)
            return false;

        if (command.help.onTest && !options.test) {
            msg.reply("Cette commande est encore en développement.");
            return true;
        }

        const allow = await this.hasPermission(msg.member, msg.channel, command);
        if (!allow) {
            msg.reply("Tu n'as pas les permissions nécessaires pour utiliser cette commande.");
            return;
        }

        if (command.help.delete)
            await msg.delete().catch(_err => logger.error(_err));
            
        const settingService = this.clientContext.getService(SettingService);
        const spamMode = await settingService.getSetting(msg.guildId, SETTING_NAMES.SPAM_MODE);

        // If the command is from the spam category, check if we are in a spam channel.
        if (command.type !== "custom" && command.help.category.toUpperCase() === "SPAM" && spamMode !== SPAM_MODE_VALUES.FREE) {
            const channelService = this.clientContext.getService(ChannelService);
            const isSpamChannel = await channelService.hasCategory(msg.channel, CHANNEL_CATEGORIES.SPAM);

            if (!isSpamChannel) {

                if (spamMode === SPAM_MODE_VALUES.RESTRICTED) {
                    const spamChannels = (await channelService.getAllChannelsWithCategory(msg.guild, CHANNEL_CATEGORIES.SPAM))
                        .filter(channel => msg.member.permissionsIn(channel).has(Permissions.FLAGS.SEND_MESSAGES));
                    if (spamChannels.length === 0)
                        msg.member.send("Désolé. Actuellement tu n'es pas autorisé d'utiliser des commandes de spam dans ce channel.");
                    else
                        msg.member.send(`Désolé. Si tu veux utiliser une commande de spam fais le dans un de ces channels : ${spamChannels.join(', ')}`);
                }
                else if (spamMode === SPAM_MODE_VALUES.BAN) {
                    const jailService = this.clientContext.getService(JailService);
                    jailService.jail(msg.member, { channel: msg.channel })
                        .then(() => { msg.member.send("PAS DE SPAM ICI ! Je t'ai mis en prison temporairement pour la peine. :upside_down:"); })
                        .catch(() => { msg.member.send("PAS DE SPAM ICI ! J'aurais dû t'envoyer en prison normalement mais j'y suis pas arrivé. :upside_down:"); });
                }
                
                return;
            }
        }
        
        // /!\ Not catching an error in a promise is not supported anymore in Node.js /!\
        try {
            this.runCommand(command, msg, rawArgs);
        }
        catch (err) {
            logger.error(err);
        }
    }

    /**
     * Returns the Command object corresponding to the given command name.
     * @param {string} guildId The id of the guild the original message comes from.
     * @param {string} cmdName The name of the command.
     * @returns {Command | CustomCommand | CustomAlias | undefined} Returns the corresponding Command object or `undefined` if no command matches.
     */
    getCommand(guildId, cmdName) {
        var cmd = this.__commands.get(cmdName);
        if (!cmd)
            cmd = this.__customCommands.get(CustomCommandInfo.createUniqueId(guildId, cmdName));
        if (!cmd) {
            cmd = this.__customAliases.get(CustomAliasInfo.createUniqueId(guildId, cmdName));
        }
        return cmd;
    }

    /**
     * Checks whether the given user has the permission to run a command.
     * @param {GuildMember} member The member that tries to run the command.
     * @param {Channel} channel The channel in which the user tries to run the command.
     * @param {Command} command The command to execute.
     * @returns {Promise<boolean>} Returns `true` if the user can run the command.
     */
    async hasPermission(member, channel, command) {
        // If it's a test command don't show it.
        if (command.help.onTest && !options.test)
            return false;
        // If the user is in jail, he has no access to the commands.
        const jailService = this.clientContext.getService(JailService);
        const inJail = await jailService.isInJail(member);
        if (inJail)
            return false;

        // Permission override of developers. Enabled only in test mode.
        if (options.test && options.developerIds.includes(member.id))
            return true;

        // If no operator is defined, skip the permissions check.
        if (!command.help.permissions || !command.help.permissions.op)
            return true;
        
        var allow = false;
        const permInChannel = member.permissionsIn(channel);
        if (command.help.permissions.op === "all")
            allow = permInChannel.has(command.help.permissions.flag);
        else if (command.help.permissions.op === "any")
            allow = permInChannel.any(command.help.permissions.flag);
        
        if (!allow) {
            // Check if the user has a custom permission through his role or his own id.
            let guildId = member.guild.id;
            let memberId = member.id;
            let permission = await this.getPermissionWithCommandName(guildId, command.help.name);
            if (permission) {
                allow = permission.MemberInfos && (permission.MemberInfos.findIndex(mInfo => mInfo.userId === memberId) !== -1);
                if (!allow) {
                    allow = permission.RoleInfos && (permission.RoleInfos.findIndex(rInfo => member.roles.cache.has(rInfo.roleId)) !== -1);
                }
            }
        }

        return allow;
    }

    /**
     * Checks whether the given user has the permission to run any command of the given category.
     * @param {GuildMember} member The member that tries to run the command.
     * @param {Channel} channel The channel in which the user tries to run the command.
     * @param {string} category The category of command the user wants access to.
     * @returns {Promise<boolean>}
     */
    async hasPermissionForCategory(member, channel, category) {
        const commands = this.getUniqueCommandList(category);
        for (let command of commands) {
            if (await this.hasPermission(member, channel, command)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Parse the command name and the raw arguments from a message.
     * @param {string} prefix The command prefix.
     * @param {Message} msg The message that initiated the command.
     */
    parseCommandNameAndRawArgs(prefix, msg) {
        let parameterList = msg.content.slice(prefix.length).split(/\s/).filter(str => !!str); // Separator : any whitespace
        let commandName = parameterList.shift();
        let rawArgs = parameterList.join(' ');
        return {
            commandName: commandName,
            rawArgs: rawArgs
        };
    }

    /**
     * Parse the arguments and kwargs from a raw text.
     * @param {string} rawArgs The raw text of the arguments.
     * @returns {CommandArgs}
     */
    parseCommandArgsAndKwargs(rawArgs) {
        let parameterList = rawArgs.split(/\s/).filter(elem => !!elem); // Separator : any whitespace
        let args = [];
        let kwargs = {};
        let index = 0;
        while (index < parameterList.length) {
            if (parameterList[index].startsWith("--")) { // kwarg
                let kwargName = parameterList[index].slice(2);
                let kwargValues = [];
                index += 1;
                while (index < parameterList.length && !parameterList[index].startsWith("--")) {
                    kwargValues.push(parameterList[index]);
                    index += 1;
                }
                kwargs[kwargName] = kwargValues;
            } else {
                args.push(parameterList[index]);
                index += 1;
            }
        }

        return {
            args: args,
            kwargs: kwargs
        }
    }

    /**
     * Run the code of the given command.
     * @param {Command | CustomAlias | CustomCommand} cmd The command to run.
     * @param {Message} msg The message that initiated the command.
     * @param {string} rawArgs The raw text of the arguments.
     */
    runCommand(cmd, msg, rawArgs) {
        if (cmd.type === "custom") { // Just post the custom command's content.
            msg.channel.send(cmd.content).catch(_err => logger.error(_err));
            return;
        }

        var cmdToRun = cmd;
        if (cmd.type === "alias") {
            cmdToRun = cmd.originalCmd;
            if (cmd.aliasArgs) {
                let splitArgs = rawArgs.split(/\s/).filter(str => !!str);
                if (splitArgs.length < cmd.aliasArgCount)
                    return msg.reply(`L'alias ${cmd.aliasName} attend ${cmd.aliasArgCount} argument(s). ${splitArgs.length} ont été donnés.`);
                
                rawArgs = format(cmd.aliasArgs, ...splitArgs) + " " + splitArgs.slice(cmd.aliasArgCount).join(" ");
            }
        }
        const { args, kwargs } = this.parseCommandArgsAndKwargs(rawArgs);
        cmdToRun.run(this.clientContext, msg, args, kwargs);
    }
    
    /**
     * Returns a list composed only of distincts commands. (No alias).
     * @param {string?} category (optional) Filters command by category. By default accept all commands.
     * @returns {Command[]}
     */
    getUniqueCommandList(category = undefined) {
        const list = [];
        this.__commands.forEach((cmd, key) => {
            if (key === cmd.help.name && (!category || cmd.help.category === category)) // If there's no match that means it's an alias.
                list.push(cmd);
        });
        return list;
    }

    /**
     * Returns a list of all the custom commands defined on the given guild.
     * @param {string} guildId The id of the guild. 
     * @returns {CustomCommand[]}
     */
    getCustomCommandList(guildId) {
        return [...this.__customCommands.values()].filter(cmd => cmd.guildId === guildId);
    }
    
    /**
     * Display table with commands in the logs.
     */
    logCommandList() {
        let usagelen = 45;
        let aliaslen = 30;
        /******************************************************************************/
        logger.log(`+-${"-".repeat(usagelen)}---${"-".repeat(aliaslen)}-+`);
        let title = "COMMANDES";
        let space = " ".repeat((usagelen + aliaslen - title.length) / 2);
        logger.log("| " + space + `  ${TERMINAL_COLORS.Bright}` + title + `${TERMINAL_COLORS.Reset} ` + space + " |");
        /******************************************************************************/
        let usage = "Usage".substr(0, usagelen);
        let aliases = "Alias".substr(0, aliaslen);
        let spaceUsage = " ".repeat((usagelen - usage.length) / 2);
        let spaceAliases = " ".repeat((aliaslen - aliases.length) / 2);
        logger.log(`+-${"-".repeat(usagelen)}-+-${"-".repeat(aliaslen)}-+`);
        logger.log("| " + spaceUsage + `${TERMINAL_COLORS.Bright}` + usage + `${TERMINAL_COLORS.Reset}` + spaceUsage + " | " + spaceAliases + `${TERMINAL_COLORS.Bright}` + aliases + `${TERMINAL_COLORS.Reset}` + spaceAliases + "  |");
        /******************************************************************************/
        this.getUniqueCommandList().forEach(c => {
            let usage = c.help.usage.substr(0, usagelen);
            usage += " ".repeat(usagelen - usage.length);
            let aliases = c.help.aliases.join(', ').substr(0, aliaslen);
            aliases += " ".repeat(aliaslen - aliases.length);
            let color = "";
            if (c.help.onTest) {
                color = TERMINAL_COLORS.FgRed;
            } else {
                color = TERMINAL_COLORS.FgGreen;
            }
            logger.log(`+-${"-".repeat(usagelen)}-+-${"-".repeat(aliaslen)}-+`);
            logger.log(`| ${usage} | ${aliases} |` + color + ` ⬤${TERMINAL_COLORS.Reset}`);
        });

        logger.log(`+-${"-".repeat(usagelen)}-+-${"-".repeat(aliaslen)}-+`);
    }

    /**
     * Creates a custom command.
     * @param {string} guildId The id of the guild where to add the new command.
     * @param {string} customCommandName The name of the new command.
     * @param {string} content The content of the message posted when the command is invoked.
     * @param {boolean} deleteMsg Whether to delete the message that invokes the command.
     * @returns {Promise<[boolean, COMMAND_ERRORS]>} A [success, errorMessage] pair.
     */
    async addCustomCommand(guildId, customCommandName, content, deleteMsg = false) {
        if (this.__commands.has(customCommandName))
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_BUILTIN_COMMAND];
        if (this.__customAliases.has(CustomAliasInfo.createUniqueId(guildId, customCommandName)))
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND];
        
        const uniqueId = CustomCommandInfo.createUniqueId(guildId, customCommandName);
        if (this.__customCommands.has(uniqueId))
            return [false, COMMAND_ERRORS.ALREADY_EXISTING_CUSTOM_COMMAND];
        if (!content)
            return [false, COMMAND_ERRORS.INVALID_PARAMS];
        
        var isError = false;
        const customCommand = await CustomCommandInfo.create({
            uniqueId: uniqueId,
            guildId: guildId,
            name: customCommandName,
            content: content,
            deleteMsg: !!deleteMsg
        }).catch(_err => {
            logger.error(_err);
            isError = true;
        });

        if (isError || !customCommand)
            return [false, COMMAND_ERRORS.CREATE_FAILED];
        
        // Register the new custom command to the current map.
        const customObj = createCustomCommandObject(customCommand);
        this.__customCommands.set(uniqueId, customObj);
        return [true, COMMAND_ERRORS.NO_ERROR];
    }

    /**
     * Removes the given custom command.
     * @param {string} guildId The id of the guild.
     * @param {string} customCommandName The name of the custom command to remove.
     * @returns {Promise<[boolean, COMMAND_ERRORS]>}
     */
    async removeCustomCommand(guildId, customCommandName) {
        if (this.__commands.has(customCommandName))
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_BUILTIN_COMMAND];
        if (this.__customAliases.has(CustomAliasInfo.createUniqueId(guildId, customCommandName)))
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND];
        
        const uniqueId = CustomCommandInfo.createUniqueId(guildId, customCommandName);
        if (!this.__customCommands.has(uniqueId))
            return [false, COMMAND_ERRORS.NO_SUCH_CUSTOM_COMMAND];
        
        var isError = false;
        await CustomCommandInfo.destroy({ where: { uniqueId: uniqueId }})
            .catch(_err => {
                logger.error(_err);
                isError = true;
            });
        
        if (isError)
            return [false, COMMAND_ERRORS.DELETE_FAILED];
        
        this.__customCommands.delete(uniqueId);
        return [true, COMMAND_ERRORS.NO_ERROR];
    }

    /**
     * Add a custom alias to the inner map and the database.
     * @param {string} guildId The if of the guild to define the alias in.
     * @param {string} commandName The name of the command to create an alias for.
     * @param {string} aliasName The name of the alias.
     * @param {string} aliasArgs (optional) The alias arguments.
     * @returns {Promise<[boolean, COMMAND_ERRORS]>} A [success, errorMessage] pair.
     */
    async addCustomAlias(guildId, commandName, aliasName, aliasArgs = undefined) {
        const command = this.getCommand(guildId, commandName);
        if (!command)
            return [false, COMMAND_ERRORS.NO_SUCH_COMMAND];
        if (command.type === "alias") // We don't create alias of alias.
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND];
        if (command.type === "custom")
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_CUSTOM_COMMAND];
        
        const existingCmd = this.getCommand(guildId, aliasName);
        if (existingCmd) {
            if (existingCmd.type === "alias")
                return [false, COMMAND_ERRORS.ALREADY_EXISTING_ALIAS_COMMAND];
            if (existingCmd.type === "custom")
                return [false, COMMAND_ERRORS.ALREADY_EXISTING_CUSTOM_COMMAND];
            return [false, COMMAND_ERRORS.ALREADY_EXISTING_BUILTIN_COMMAND];
        }

        aliasArgs = aliasArgs || "";
        var isError = false;

        const alias = await CustomAliasInfo.create({ 
            uniqueId: CustomAliasInfo.createUniqueId(guildId, aliasName),
            guildId: guildId,
            aliasName: aliasName,
            commandName: command.help.name,
            aliasArgs: aliasArgs  
        })
        .catch(_err => {
            logger.error(_err);
            isError = true;
        });

        if (isError || !alias)
            return [false, COMMAND_ERRORS.CREATE_FAILED];
        
        // Add the alias to the current map
        const aliasObj = createCustomAliasObject(alias.guildId, command, alias.aliasName, alias.aliasArgs);
        this.__customAliases.set(alias.uniqueId, aliasObj);

        return [true, COMMAND_ERRORS.NO_ERROR];
    }

    /**
     * Returns all custom aliases defined in the given guild.
     * @param {string} guildId The id of the guild.
     * @returns {CustomAlias[]}
     */
    getAllCustomAliases(guildId) {
        return [...this.__customAliases.values()]
            .filter(alias => alias.guildId === guildId);
    }

    /**
     * Returns all custom aliases of a given command.
     * @param {string} guildId The id of the guild.
     * @param {string} commandName The name of the command to search aliases for.
     * @returns {CustomAlias[]}
     */
    getAllCustomAliasesOfCommand(guildId, commandName) {
        return [...this.__customAliases.values()]
            .filter(alias => alias.guildId === guildId && 
                (alias.help.name === commandName || alias.help.aliases.includes(commandName)));
    }

    /**
     * Delete a custom alias from the inner map and the database.
     * @param {string} guildId The id of the guild the alias is defined in.
     * @param {string} aliasName The name of the alias.
     * @returns {Promise<[boolean, COMMAND_ERRORS]>} A [success,errorMessage] pair.
     */
    async removeOneCustomAlias(guildId, aliasName) {
        // Reject if the name is of one built-in command.
        if (this.__commands.has(aliasName)) {
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_BUILTIN_COMMAND];
        }
        if (this.__customCommands.has(CustomCommandInfo.createUniqueId(guildId, aliasName)))
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_CUSTOM_COMMAND]

        const uniqueId = CustomAliasInfo.createUniqueId(guildId, aliasName);
        if (!this.__customAliases.has(uniqueId)) {
            return [false, COMMAND_ERRORS.NO_SUCH_ALIAS];
        }

        var isError = false;
        await CustomAliasInfo.destroy({ where: { uniqueId: uniqueId } })
            .catch(_err => {
                isError = true;
                logger.error(_err);
            });
        
        if (isError) {
            return [false, COMMAND_ERRORS.DELETE_FAILED];
        }
        this.__customAliases.delete(uniqueId);
        return [true,COMMAND_ERRORS.NO_ERROR];
    }

    /**
     * @typedef {object} IdInfo Object prototype
     * @property {string} id The id to grant the permission.
     * @property {"user" | "role"} type Indicate if the id is from a specific user or a role.
     */

    /**
     * Inner method to add or remove command permission.
     * @param {string} guildId The id of the guild.
     * @param {string} commandName The name of the command to add/remove a permission to.
     * @param {IdInfo[]} idInfos The ids to grant/remove the permission.
     * @param {boolean} add Whether the method should add or remove.
     * @returns {Promise<[boolean, COMMAND_ERRORS]>} A [success, errorMessage] pair.
     */
    async __addOrRemoveCommandPermission(guildId, commandName, idInfos, add) {
        if (this.__customAliases.has(CustomAliasInfo.createUniqueId(guildId, commandName))) {
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND];
        }
        if (this.__customCommands.has(CustomCommandInfo.createUniqueId(guildId, commandName))) {
            return [false, COMMAND_ERRORS.INVALID_OPERATION_ON_CUSTOM_COMMAND];
        }

        const command = this.__commands.get(commandName);
        if (!command) {
            return [false, COMMAND_ERRORS.NO_SUCH_COMMAND];
        }

        var isError = false;
        const uniqueId = CommandPermission.createUniqueId(guildId, command.help.name);
        const [commandPerm, _created] = await CommandPermission.findOrCreate({ 
            where: { uniqueId: uniqueId },
            defaults: { uniqueId: uniqueId, guildId: guildId, commandName: command.help.name },
            include: [
                { model: MemberInfo },
                { model: RoleInfo }
            ]
        })
        .catch(_err => {
            logger.error(_err);
            isError = true;
            return [null, false];
        });

        if (isError || !commandPerm)
            return [false, add ? COMMAND_ERRORS.ADD_FAILED : COMMAND_ERRORS.DELETE_FAILED];
        
        for (let idInfo of idInfos) {
            if (idInfo.type === "user") {
                let memberInfoId = MemberInfo.constructUniqueId(guildId, idInfo.id);
                let [memberInfo] = await MemberInfo.findOrCreate({ 
                    where: { uniqueId: memberInfoId },
                    defaults: { uniqueId: memberInfoId }
                }).catch(_err => {
                    logger.error(_err);
                    isError = true;
                    return [null, false];
                });

                if (!isError) {
                    let promise = add ? commandPerm.addMemberInfo(memberInfo) : commandPerm.removeMemberInfo(memberInfo);
                    await promise.catch(_err => {
                        logger.error(_err);
                        isError = true;
                    })
                }
            }
            else if (idInfo.type === "role") {
                let [roleInfo] = await RoleInfo.findOrCreate({
                    where: { roleId: idInfo.id },
                    defaults: { roleId: idInfo.id, guildId: guildId }
                }).catch(_err => {
                    logger.error(_err);
                    isError = true;
                    return [null, false];
                });

                if (!isError) {
                    let promise = add ? commandPerm.addRoleInfo(roleInfo) : commandPerm.removeRoleInfo(roleInfo);
                    await promise.catch(_err => {
                        logger.error(_err);
                        isError = true;
                    })
                }
            }

            if (isError)
                break;
        }

        if (isError)
            return [false, add ? COMMAND_ERRORS.ADD_FAILED : COMMAND_ERRORS.DELETE_FAILED];
        
        return [true, COMMAND_ERRORS.NO_ERROR];
    }

    /**
     * Add a custom permission for a command.
     * @param {string} guildId The id of the guild.
     * @param {string} commandName The name of the command to add a permission to.
     * @param {IdInfo[]} idInfos The ids to grant the permission.
     * @returns {Promise<[boolean, COMMAND_ERRORS]>} A [success, errorMessage] pair.
     */
    async addCommandPermission(guildId, commandName, ...idInfos) {
        return await this.__addOrRemoveCommandPermission(guildId, commandName, idInfos, true);
    }

    /**
     * Remove a custom permission for a command.
     * @param {string} guildId The id of the guild.
     * @param {string} commandName The name of the command to remove a permission from.
     * @param {IdInfo[]} idInfos The ids to remove the permission from.
     * @returns {Promise<[boolean, COMMAND_ERRORS]>} A [success, errorMessage] pair.
     */
    async removeCommandPermission(guildId, commandName, ...idInfos) {
        return await this.__addOrRemoveCommandPermission(guildId, commandName, idInfos, false);
    }

    /**
     * Returns all permissions defined in a guild.
     * @param {string} guildId The id of the guild. 
     */
    async getAllPermissions(guildId) {
        return await CommandPermission.findAll({
            where: { guildId: guildId },
            include: [
                { model: MemberInfo },
                { model: RoleInfo }
            ]
        });
    }

    /**
     * Returns the permission for the given command name.
     * @param {string} guildId The id of the guild.
     * @param {string} cmdName The name of the command the permissions give access to.
     * @returns {Promise<CommandPermission>}
     */
    async getPermissionWithCommandName(guildId, cmdName) {
        const cmd = this.getCommand(guildId, cmdName);
        if (!cmd)
            return [];
        
        return await CommandPermission.findOne({
            where: { guildId: guildId, commandName: cmd.help.name },
            include: [
                { model: MemberInfo },
                { model: RoleInfo }
            ]
        });
    }
}

module.exports = {
    CommandService,
    COMMAND_ERRORS
}