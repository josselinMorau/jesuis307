const { Message, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { unaccent, parseMessageIdFromLink } = require('../../utils/helper.js');

const alphabet = {
    "a": ['🇦', '🅰️'],
    "b": ['🇧', '🅱️'],
    "c": ['🇨'],
    "d": ['🇩'],
    "e": ['🇪'],
    "f": ['🇫'],
    "g": ['🇬'],
    "h": ['🇭'],
    "i": ['🇮','ℹ️'],
    "j": ['🇯'],
    "k": ['🇰'],
    "l": ['🇱'],
    "m": ['🇲','Ⓜ️'],
    "n": ['🇳'],
    "o": ['🇴','🅾️','⭕'],
    "p": ['🇵','🅿️'],
    "q": ['🇶'],
    "r": ['🇷','®️'],
    "s": ['🇸','💲','🐍'],
    "t": ['🇹'],
    "u": ['🇺'],
    "v": ['🇻'],
    "w": ['🇼'],
    "x": ['🇽','❎','❌','✖'],
    "y": ['🇾'],
    "z": ['🇿'],
    "0": ['0️⃣'],
    "1": ['1️⃣'],
    "2": ['2️⃣'],
    "3": ['3️⃣'],
    "4": ['4️⃣'],
    "5": ['5️⃣'],
    "6": ['6️⃣'],
    "7": ['7️⃣'],
    "8": ['8️⃣'],
    "9": ['9️⃣'],
    "#": ['#️⃣'],
    "*": ['*️⃣'],
    "!": ['❗', '❕'],
    "?": ['❓', '❔'],
};

/**
 * 
 * @param {string} word 
 * @param {Message} msg 
 */
async function react(word, msg) {
    const emojis = [];
    const count = {};

    for (let char of word) {
        let index = count[char] || 0;
        let emoji = alphabet[char][index];
        if (emoji) {
            emojis.push(emoji);
            count[char] = index + 1;
        }
    }

    for (let emoji of emojis) {
        await msg.react(emoji)
            .catch(() => {});
    }
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
async function runCmd(ctx, msg, args, kwargs) {
    if (args.length === 0)
        return;
    
    const word = unaccent(args[0]);
    var target = null;
    
    if (args.length >= 2) {
        const id = parseMessageIdFromLink(args[1]);
        target = await msg.channel.messages.fetch(id).catch(() => null);
    }
    else if (msg.type === "REPLY") {
        target = await msg.channel.messages.fetch(msg.reference.messageId).catch(() => null);
    }
    else {
        target = (await msg.channel.messages.fetch({ limit: 1 })).last();
    }

    if (!target)
        return;
    
    react(word, target);
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    runCmd(ctx, msg, args, kwargs);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "react", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "react <reaction> [<msgLink>]", // The usage of the command.
    description: "Créer une réaction avec des émojis.", // The command's description.
    examples: [
        {
            usage: "react omg",
            description: "Met `OMG` en réaction du dernier message posté sur le channel." +
                "Si la commande react est en réponse à un message, la réaction est ajoutée sur ce dernier."
        },
        {
            usage: "react omg https://discord.com/channels/123456789/123456789/123456789",
            description: "Met `OMG` en réaction au message correspondant au lien donné."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
