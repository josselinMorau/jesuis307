const { Message, Permissions, MessageEmbed } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { PollService, POLL_ERRORS } = require('../../misc/PollService.js');
const { parseMessageIdFromLink, createEmbedsFromData, CONSTANTS, createEmbedsFromDataAsync, findMessageInGuild } = require('../../utils/helper.js');

const DEFAULT_CHOICE_EMOJIS = ['🇦','🇧','🇨','🇩','🇪','🇫','🇬','🇭','🇮','🇯','🇰','🇱','🇲','🇳','🇴','🇵','🇶','🇷','🇸','🇹','🇺','🇻','🇼','🇽','🇾','🇿'];

/**
 * @param {PollService} service
 * @param {Message} msg 
 */
async function displayPollList(service, msg) {
    const polls = await service.findAllPolls(msg.guildId);
    if (polls.length === 0) {
        msg.channel.send("Il n'y a aucun sondage en cours sur le serveur.");
        return;
    }

    const embeds = await createEmbedsFromDataAsync({ title: "Sondages", color: CONSTANTS.COLOR_INFO }, polls, async (embed, poll) => {
        const pollMsg = await findMessageInGuild(msg.guild, poll.msgId);
        embed.addField(poll.question, `**[Lien](${pollMsg.url})**, **Auteur:** <@${poll.author}>, **Anonyme:** ${poll.anonymous}, **Choix multiple:** ${poll.allowMultiple}`);
    });

    msg.channel.send({ embeds: embeds });
}

/**
 * 
 * @param {PollService} service 
 * @param {Message} msg 
 * @param {string} pollId
 */
async function displayPollInfo(service, msg, pollId) {
    const poll = await service.fetchPoll(pollId);
    if (!poll) {
        msg.channel.send("Aucun sondage ne correspond à l'id donné.");
        return;
    }

    const pollMsg = await findMessageInGuild(msg.guild, poll.msgId);
    const results = await service.fetchResults(msg.guild, poll);

    const embed = new MessageEmbed()
        .setTitle(poll.question)
        .addField("Auteur", `<@${poll.author}>`)
        .addField("Anonyme", `${poll.anonymous ? 'Oui' : 'Non'}`)
        .addField("Choix multiple", `${poll.allowMultiple ? 'Oui' : 'Non'}`)
        .addField("Nombre de votants", `${results.userCount}`)
        .setURL(pollMsg.url);

    msg.channel.send({embeds: [embed]});
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const pollService = ctx.getService(PollService);

    const info = kwargs.info;
    if (!!info) {
        if (info.length === 0) {
            msg.channel.send("Tu n'as pas précisé le lien du sondage.");
        }
        else {
            const id = parseMessageIdFromLink(info[0]);
            displayPollInfo(pollService, msg, id).catch(err => {
                logger.error(err);
                msg.channel.send("Une erreur est survenue.");
            });
        }
        return;
    }

    const close = kwargs.close;
    if (!!close) {
        if (close.length === 0) {
            msg.channel.send("Tu n'as pas précisé le lien du sondage à fermer.");
        }
        else {
            const id = parseMessageIdFromLink(close[0]);
            pollService.closePoll(msg.channel, id).then(() => {
                msg.channel.send("Le sondage a bien été clos.");
            })
            .catch(err => {
                switch (err) {
                    case POLL_ERRORS.NO_SUCH_POLL: {
                        msg.channel.send("Aucun sondage ne correspond à l'id donné.");
                        break;
                    }
                    default: {
                        logger.error(err);
                        msg.channel.send("Une erreur est survenue.");
                        break;
                    }
                }
            });
        }
        return;
    }

    if (!!kwargs.list) {
        displayPollList(pollService, msg).catch(err => {
            logger.error(err);
            msg.channel.send("Une erreur est survenue.");
        });
        return;
    }

    if (args.length === 0) {
        msg.channel.send("Tu n'as précisé aucune question.");
        return;
    }

    const question = args.join(' ');
    const choices = [];

    const a = 'a'.charCodeAt(0);
    const z = 'z'.charCodeAt(0);
    for (let char = a; char <= z; ++char) {
        const field = String.fromCharCode(char);
        const choice = kwargs[field];
        if (!choice)
            continue;
        
        const emoji = DEFAULT_CHOICE_EMOJIS[char - a];
        if (choice.length === 0) {
            msg.channel.send(`La réponse ${emoji} est vide.`);
            return;
        }

        const content = choice.join(' ');
        choices.push({ content: content, emoji: emoji });
    }

    if (choices.length === 0) {
        // Create default choices.
        choices.push({ emoji: '👍', content: "Oui" });
        choices.push({ emoji: '👎', content: "Non" });
        choices.push({ emoji: '🤷', content: "Peut-être" });
    }
    else if (choices.length === 1) {
        msg.channel.send("À quoi sert un sondage avec un seul choix de réponse ?");
        return;
    }

    const anonymous = !!kwargs.anonymous;
    const multiple = !!kwargs.multiple;

    pollService.createPoll(msg.channel, { 
        question: question, anonymous: anonymous,
        allowMultiple: multiple, choices: choices, author: msg.member })
        .catch(err => {
            logger.error(err);
            msg.channel.send("Une erreur est survenue.");
        });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "poll", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "poll [ [<question...>] [ [--a] [<choice1...>] [--b] [<choice2...>] ... ] [--anonymous] [--multiple] ] [ [--close] [--info] [<pollLink>] ] [--list]", // The usage of the command.
    description: "Créer un sondage.", // The command's description.
    examples: [
        {
            usage: "poll Ma question ?",
            description: "Créer un sondage avec comme question \"Ma question ?\" et comme réponses possibles \"Oui\", \"Non\" et \"Peut-être\"."
        },
        {
            usage: "poll Ma question ? --a Réponse 1 --b Réponse 2 --c Réponse 3",
            description: "Créer un sondage avec comme question \"Ma question ?\" et comme réponses possibles \"Réponse 1\", \"Réponse 2\" et \"Réponse 3\".\n" + 
                "Vous pouvez écrire des réponses de a à z avec le kwarg correspondant."
        },
        {
            usage: "poll Ma question ? --a Réponse 1 --b Réponse 2 --anonymous",
            description: "Créer un sondage anonyme. Les sondages anonymes utilisent un menu au lieu des emojis pour récupérer les votes."
        },
        {
            usage: "poll Ma question ? --a Réponse 1 --b Réponse 2 --multiple",
            description: "Créer un sondage qui autorise plusieurs réponses par utilisateur."
        },
        {
            usage: "poll --close https://discord.com/channels/123456789/123456789/123456789",
            description: "Ferme le sondage avec le lien de message donné et affiche ses résultats dans le channel."
        },
        {
            usage: "poll --info https://discord.com/channels/123456789/123456789/123456789",
            description: "Affiche les infos du sondage avec le lien de message donné."
        },
        {
            usage: "poll --list",
            description: "Affiche la liste de tous les sondages en cours sur le serveur."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.MANAGE_MESSAGES, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "all" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
