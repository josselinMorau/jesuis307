const { Message, Permissions, TextChannel } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { BirthdayService } = require('../../misc/BirthdayService.js');
const { createEmbedsFromData, waitForUserAnswer, CONSTANTS } = require('../../utils/helper.js');

/**
 * 
 * @param {BirthdayService} service 
 * @param {TextChannel} channel 
 */
async function displayAllBirthdayMessages(service, channel) {
    const birthdayMessages = await service.findAllBirthdayMessages(channel.guildId);
    if (birthdayMessages.length === 0) {
        channel.send("Il n'y a aucun message d'anniversaire enregistré sur ce serveur.");
        return;
    }

    const embeds = createEmbedsFromData({ title: "Messages enregistrés", color: CONSTANTS.COLOR_INFO }, birthdayMessages, (embed, birthdayMsg) => {
        embed.addField(birthdayMsg.uniqueId, `"${birthdayMsg.content}"\nCréé par <@${birthdayMsg.authorId}>.`);
    });

    channel.send({ embeds: embeds });
}

/**
 * 
 * @param {BirthdayService} service 
 * @param {Message} msg 
 * @param {string} id 
 */
async function deleteBirthdayMessages(service, msg, id) {
    const { channel } = msg;
    const birthdayMessages = await service.findAllBirthdayMessagesWithId(channel.guildId, id);
    if (birthdayMessages.length === 0) {
        channel.send(`Aucun message d'anniversaire ne commence par ${id}.`);
        return;
    }
    
    const embeds = createEmbedsFromData({ title: "Messages à détruire", color: CONSTANTS.COLOR_DESTROY }, birthdayMessages, (embed, birthdayMsg) => {
        embed.addField(birthdayMsg.uniqueId, `"${birthdayMsg.content}"\nCréé par <@${birthdayMsg.authorId}>.`)
    });

    const plural = birthdayMessages.length > 1;
    const letterS = plural ? 's' : '';
    const [answer, confirMsg] = await waitForUserAnswer(msg.author, msg.channel, 120_000,
        { content: `Es-tu sûr(e) de vouloir supprimer le${letterS} message${letterS} suivant${letterS} ?`, embeds: embeds },
        [{ style: "PRIMARY", label: "Oui" }, { style: "DANGER", label: "Non" }]);
    
    confirMsg.delete().catch(() => {});
    
    if (answer === "Oui") {
        const promises = [];
        for (let birthday of birthdayMessages) {
            promises.push(
                birthday.destroy().catch(err => logger.error(err))
            );
        }
        await Promise.all(promises);
        channel.send(`Le${letterS} message${letterS} d'anniversaire${letterS} ${plural ? 'ont' : 'a'} bien été supprimé${letterS}.`);
    }
    else {
        channel.send("L'opération a été annulée.");
    }
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const add = kwargs.add;
    const remove = kwargs.remove || kwargs.rm;
    const option = add || remove;

    const birthdayService = ctx.getService(BirthdayService);

    if (!option) {
        displayAllBirthdayMessages(birthdayService, msg.channel)
            .catch(err => {
                logger.error(err);
                msg.channel.send("Une erreur est survenue.");
            });
        return;
    }

    if (option === add) {
        const content = option.join(" ");
        if (option.length === 0 || !content) {
            msg.channel.send("Tu n'as pas passé de message en argument.");
            return;
        }

        birthdayService.addBirthdayMessage(msg.member, content).then(() => {
            msg.channel.send("Ton message a été ajouté avec succès.");
        })
        .catch(err => {
            logger.error(err);
            msg.channel.send("Une erreur est survenue.");
        });
        return;
    }

    if (option === remove) {
        
        if (option.length === 0) {
            msg.channel.send("Tu n'as passé aucun id en argument.");
            return;
        }

        const id = option[0];
        deleteBirthdayMessages(birthdayService, msg, id).catch(err => {
            logger.error(err);
            msg.channel.send("Une erreur est survenue.");
        });
        return;
    }
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "birthday-message", // The command's name.
    aliases: ["birthdayMessage", "birthdayMsg", "bdayMsg"], // A list of aliases for the command.
    usage: "birthday-message [--add <message...>] [--remove | --rm <id>]", // The usage of the command.
    description: "Gère les messages d'anniversaires.", // The command's description.
    examples: [
        {
            usage: "birthday-message",
            description: "Affiche la liste des messages enregistrés sur le serveur."
        },
        {
            usage: "birthday-message --add Joyeux anniversaire %n !",
            description: "Ajoute 'Joyeux anniversaire %n !' comme message d'anniversaire.\n" + 
                "Les symboles '%n' dans le message seront remplacés par le pseudo de la personne dont ça sera l'anniversaire."
        },
        {
            usage: "birthday-message --rm 2905cce7",
            description: "Supprime le(s) message(s) dont l'id est '2905cce7' ou commence par '2905cce7'"
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR | Permissions.FLAGS.MANAGE_MESSAGES, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "any" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
