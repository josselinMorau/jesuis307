const { Message, Permissions, GuildMember, TextChannel, DMChannel } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { BirthdayService } = require('../../misc/BirthdayService.js');
const { unaccent } = require('../../utils/helper.js');

const MONTHS = ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"];
const MONTH_ALIASES = [
    ["janvier", "january", "jan"],
    ["fev", "fevrier", "feb", "february"],
    ["mar", "mars", "march"],
    ["avr", "avril", "apr", "april"],
    ["mai", "may"],
    ["jun", "juin", "june"],
    ["jul", "juillet", "july"],
    ["aug", "aout", "august"],
    ["sep", "septembre", "september"],
    ["oct", "octobre", "october"],
    ["nov", "novembre", "november"],
    ["dec", "decembre", "december"]
];

/**
 * @param {string[]} args 
 * @returns {Date?}
 */
function parseBirthday(args) {
    var day = -1;
    var month = -1;

    if (args.length >= 2) {
        day = parseInt(args[0]);
        const input = unaccent(args[1]).toLowerCase();
        month = MONTH_ALIASES.findIndex(aliases => aliases.includes(input));
    }
    else {
        const split = args[0].split('/');
        day = parseInt(split[0]);
        month = parseInt(split[1]) - 1;
    }

    // Invalid.
    if (isNaN(day) || day <= 0 || day > 31 || isNaN(month) || month < 0 || month >= 12)
        return null;
    
    return new Date(2000, month, day, 10); // year 2000 and hour 10 are arbitrary values.
    // Update : Actually keep 2000 because it's a leap year and it accepts the 29th february.
}

/**
 * 
 * @param {ClientContext} ctx 
 * @param {TextChannel | DMChannel} channel 
 * @param {GuildMember} member 
 * @param {Date} date 
 */
function tryToUpdateBirthday(ctx, channel, member, date) {
    if (!date) {
        channel.send("La date donnée n'est pas valide.");
        return;
    }

    const birthdayService = ctx.getService(BirthdayService);
    birthdayService.updateBirthday(member, date)
        .then(() => {
            channel.send(`${member} La date de ton anniversaire a été mise à jour au ${date.getDate() === 1 ? '1er' : date.getDate()} ${MONTHS[date.getMonth()]}.`);
        })
        .catch(err => {
            logger.error(err);
            channel.send("Une erreur est survenue.");
        });
}

/**
 * 
 * @param {ClientContext} ctx 
 * @param {GuildMember} member 
 */
async function askBirthday(ctx, member) {
    await member.send("Salut :wave:. Dis moi ici ton anniversaire que je le mette à jour. :)");
    var isError = false;
    const dmChannel = member.user.dmChannel;
    const messages = await dmChannel.awaitMessages({ time: 120_000, max: 1 })
        .catch(() => { isError = true; });
    
    if (messages.size === 0 || isError) {
        member.send("L'opération a été annulée à cause d'une erreur ou d'une trop longue inactivité.");
        return;
    }
    
    const args = messages.first().content.split(/\s/).filter(str => !!str);
    const date = parseBirthday(args);
    tryToUpdateBirthday(ctx, dmChannel, member, date);
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    if (args.length === 0) {
        askBirthday(ctx, msg.member).catch(err => logger.error(err));
        return;
    }

    const date = parseBirthday(args);
    tryToUpdateBirthday(ctx, msg.channel, msg.member, date);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "birthday", // The command's name.
    aliases: ["bday", "annif", "anniv"], // A list of aliases for the command.
    usage: "birthday [<dd/mm> | <dd monthName>]", // The usage of the command.
    description: "Met à jour la date de l'anniversaire d'utilisateur.", // The command's description.
    examples: [
        {
            usage: "birthday",
            description: "Le bot envoie des messages en MP à l'utilisateur pour lui demander sa date d'anniversaire."
        },
        {
            usage: "birthday 12/05",
            description: "Règle la date de l'anniversaire de l'utilisateur au 12 mai."
        },
        {
            usage: "birthday 12 mai",
            description: "Règle la date de l'anniversaire de l'utilisateur au 12 mai."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
