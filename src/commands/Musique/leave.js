const { Message, Permissions, MessageEmbed } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { MusicService, MUSIC_ERRORS } = require('../../misc/MusicService.js');
const { logger } = require('../../logging/logging.js');
const { AudioPlayerStatus } = require('@discordjs/voice');

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const musicService = ctx.getService(MusicService);
    const musicState = musicService.getMusicState(msg.guildId);
    if (musicState.isConnected()) {
        musicState.destroy();
        msg.channel.send("Déconnecté.");
    }
    else {
        msg.channel.send("Le bot n'est dans aucun vocal actuellement.");
    }
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "leave", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "leave", // The usage of the command.
    description: "Finit de jouer la musique et quitte le salon vocal.", // The command's description.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
