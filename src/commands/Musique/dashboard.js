const { Message, Permissions, MessageEmbed } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { MusicService, MUSIC_ERRORS } = require('../../misc/MusicService.js');
const { logger } = require('../../logging/logging.js');
const { AudioPlayerStatus } = require('@discordjs/voice');

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const voiceChannel = msg.member.voice.channel;
    if (!voiceChannel) {
        msg.channel.send("Il faut être dans un channel vocal pour appeler cette commande.");
        return;
    }

    const musicService = ctx.getService(MusicService);
    const musicState = musicService.getMusicState(msg.guildId);
    if (musicState.isConnected()) {
        musicState.refreshDashboardMsg(msg.channel).catch(err => {
            logger.error(err);
        });
    }
    else {
        msg.channel.send("Aucune musique n'est jouée actuellement.");
    }
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "dashboard", // The command's name.
    aliases: ["nowplaying", "playing", "np"], // A list of aliases for the command.
    usage: "dashboard", // The usage of the command.
    description: "Affiche les infos sur la chanson actuellement jouée ainsi que des boutons pour intéragir avec." + 
        "La fonction de chaque bouton est décrite dans les exemples.", // The command's description.
    examples: [
        {
            usage: "⏯️",
            description: "Met en pause la musique ou la continue.",
            noPrefix: true
        },
        {
            usage: "⏭️",
            description: "Passe la musique en cours.",
            noPrefix: true
        },
        {
            usage: "⏹️",
            description: "Arrête la musique et quitte le vocal.",
            noPrefix: true
        },
        {
            usage: "🔂",
            description: "Joue en boucle la musique en cours/Désactive la boucle.",
            noPrefix: true
        }
    ],
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
