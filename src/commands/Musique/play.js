const { Message, Permissions, MessageEmbed } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { MusicService, MUSIC_ERRORS, validateYtUrl } = require('../../misc/MusicService.js');
const { logger } = require('../../logging/logging.js');
const { CONSTANTS } = require('../../utils/helper.js');

/**
 * @param {import('../../misc/MusicService.js').SongInfo} songInfo 
 * @returns {MessageEmbed}
 */
function createEmbedFromSongInfo(songInfo) {
    const embed = new MessageEmbed()
        .setAuthor("Ajouté à la playlist:")
        .setTitle(songInfo.title || "Titre inconnu")
        .setURL(songInfo.url)
        .setColor(CONSTANTS.COLOR_MUSIC);
    if (songInfo.thumbnail)
        embed.setThumbnail(songInfo.thumbnail);
    return embed;
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const voiceChannel = msg.member.voice.channel;
    if (!voiceChannel) {
        msg.channel.send("Il faut être dans un channel vocal pour appeler cette commande.");
        return;
    }

    const musicService = ctx.getService(MusicService);
    const query = validateYtUrl(args[0]) ? args[0] : args.join(" ");
    musicService.play(msg.guild, voiceChannel, msg.member, query).then((songInfo) => {
        const musicState = musicService.getMusicState(msg.guildId);
        if (!musicState.dashboardMsg) {
            musicState.refreshDashboardMsg(msg.channel).catch(err => {
                logger.error(err);
            });
        }
        if (musicState.queue.size() > 0) {
            const embed = createEmbedFromSongInfo(songInfo);
            msg.channel.send({ /*content: "Ajouté à la playlist:",*/ embeds: [embed] });
        }
    })
    .catch(err => {
        switch (err) {
            case MUSIC_ERRORS.WRONG_VOICE_CHANNEL:
                msg.channel.send("Je ne peux pas jouer de chanson dans ce salon car je joue déjà dans un autre salon.");
                break;
            case MUSIC_ERRORS.UNJOINABLE_VOICE_CHANNEL:
                msg.channel.send("Je n'ai pas les autorisations pour rejoindre ce channel.");
                break;
            case MUSIC_ERRORS.INVALID_URL:
                msg.channel.send("L'url donnée est invalide ou ses infos sont irrécupérables.");
                break;
            case MUSIC_ERRORS.NO_RESULT:
                msg.channel.send("Aucun résultat trouvé.");
                break;
            default:
                msg.channel.send("Une erreur est survenue.");
                break;
        }
    });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "play", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "play <url | query>", // The usage of the command.
    description: "Ajoute une musique à jouer dans le channel vocal actuel.", // The command's description.
    examples: [
        {
            usage: "play https://www.youtube.com/watch?v=5SXX-pWzOY8",
            description: "Ajoute la musique avec l'url youtube donnée à jouer dans le channel vocal actuel."
        },
        {
            usage: "play Never gonna give you up",
            description: "Cherche 'Never gonna give you up' sur youtube et ajoute le 1er résultat dans les musiques à jouer."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
