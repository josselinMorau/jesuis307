const { Message, Permissions, MessageEmbed } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { MusicService, MUSIC_ERRORS, MusicState } = require('../../misc/MusicService.js');
const { logger } = require('../../logging/logging.js');
const { AudioPlayerStatus } = require('@discordjs/voice');

/**
 * 
 * @param {MusicState} musicState 
 * @param {number} from 
 * @param {number} to 
 * @returns {import('../../misc/MusicService.js').SongInfo}
 */
function moveTrack(musicState, from, to) {
    const tracks = musicState.queue.toArray();
    const toMove = tracks.splice(from-1, 1);
    tracks.splice(to-1,0,...toMove);
    musicState.queue.clear();
    musicState.queue.push(...tracks);
    return toMove.at(0);
}

/**
 * 
 * @param {MusicState} musicState 
 * @param {number} index 
 * @returns {import('../../misc/MusicService.js').SongInfo}
 */
function removeTrack(musicState, index) {
    const tracks = musicState.queue.toArray();
    const [deleted] = tracks.splice(index - 1,1);
    musicState.queue.clear();
    musicState.queue.push(...tracks);
    return deleted;
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const voiceChannel = msg.member.voice.channel;
    if (!voiceChannel) {
        msg.channel.send("Il faut être dans un channel vocal pour appeler cette commande.");
        return;
    }

    const musicService = ctx.getService(MusicService);
    const musicState = musicService.getMusicState(msg.guildId);
    if (musicState.isConnected()) {
        const { mv, rm } = kwargs;
        const queueSize = musicState.queue.size();

        if (rm) {
            const index = parseInt(rm.at(0));
            if (index && index > 0 && index <= queueSize) {
                const deletedTrack = removeTrack(musicState, index);
                msg.channel.send(`\`${deletedTrack.title}\` a été supprimée de la playlist.`);
            }
            else {
                msg.channel.send("L'option --rm attend un index valide.");
            }
        }
        else if (mv) {
            const from = parseInt(mv.at(0));
            const to = parseInt(mv.at(1)) || 1;
            if (from && from > 0 && from <= queueSize && to > 0 && to <= queueSize) {
                const movedTrack = moveTrack(musicState, from, to);
                msg.channel.send(`\`${movedTrack.title}\` a été déplacée à l'index ${to} de la playlist.`);
            }
            else {
                msg.channel.send("L'option --mv attend un ou deux index valides.");
            }
        }
        
        musicState.refreshQueueMsg(msg.channel).catch(err => {
            logger.error(err);
        });
    }
    else {
        msg.channel.send("Aucune musique n'est jouée actuellement.");
    }
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "queue", // The command's name.
    aliases: ["playlist"], // A list of aliases for the command.
    usage: "queue [--rm <id>] [--mv <id_from> [<id_to>]]", // The usage of the command.
    description: "Affiche la liste des chansons dans la file d'attente ainsi que des boutons pour intéragir avec." + 
        "La fonction de chaque bouton est décrite dans les exemples.", // The command's description.
    examples: [
        {
            usage: "queue --rm 3",
            description: "Retire la musique à l'index 3 de la playlist."
        },
        {
            usage: "queue --mv 3",
            description: "Déplace la musique à l'index 3 au début de la playlist."
        },
        {
            usage: "queue --mv 3 5",
            description: "Déplace la musique à l'index 3 à l'index 5 de la playlist."
        },
        {
            usage: "⬅️",
            description: "Affiche la page précédente de la playlist.",
            noPrefix: true
        },
        {
            usage: "➡️",
            description: "Affiche la page suivante de la playlist.",
            noPrefix: true
        },
        {
            usage: "🔀",
            description: "Mélange les musiques de la playlist.",
            noPrefix: true
        },
        {
            usage: "🔁",
            description: "Active/désactive le mode loop. Quand une musique sera finie, elle sera réinsérée à la fin de la playlist.",
            noPrefix: true
        },
        {
            usage: "🚮",
            description: "Retire toutes les musiques à venir de la playlist.",
            noPrefix: true
        }
    ],
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
