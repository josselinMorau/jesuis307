const { Message, MessageEmbed, Permissions, TextChannel, GuildMember } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { SettingService } = require('../../utils/guildSettings/SettingsService.js');
const { CommandService } = require('../CommandService.js');
const { CONSTANTS, createEmbedsFromDataAsync, asyncFilter } = require('../../utils/helper.js');
const { options } = require('../../../options.js');

/**
 * Display the list of all command categories.
 * @param {ClientContext} ctx
 * @param {GuildMember} member 
 * @param {TextChannel} channel 
 */
async function displayCategoryList(ctx, member, channel) {
    const commandService = ctx.getService(CommandService);
    const categories = commandService.getCategories().filter(category => category.toLowerCase() !== "secret");
    const validCategories = await asyncFilter(categories, async (category) => { return await commandService.hasPermissionForCategory(member, channel, category); });
    
    const embeds = await createEmbedsFromDataAsync({ title: "Help", color: CONSTANTS.COLOR_SETTING }, validCategories, async (embed, category) => {
        const commands = commandService.getUniqueCommandList(category);
        const allowedCommands = await asyncFilter(commands, async (cmd) => { return await commandService.hasPermission(member, channel, cmd); });
        embed.addField(category, allowedCommands.map(cmd => `\`${cmd.help.name}\``).join(', '));
    });
    channel.send({embeds: embeds});
}

/**
 * Display the list of all available commands in a category.
 * @param {ClientContext} ctx
 * @param {GuildMember} member
 * @param {string} category 
 * @param {TextChannel} channel 
 */
async function displayCommandList(ctx, member, category, channel) {
    const commandService = ctx.getService(CommandService);
    const settingService = ctx.getService(SettingService);

    const prefix = await settingService.getPrefix(channel.guildId);
    const commands = commandService.getUniqueCommandList(category);

    const embeds = await createEmbedsFromDataAsync({ 
        title: "Help",
        color: CONSTANTS.COLOR_SETTING,
        fields: [{  name: "\u200b", value: `**__${category.toUpperCase()}__**` }]
    }, commands, async (embed, cmd) => {
        let allow = await commandService.hasPermission(member, channel, cmd);
        if (!allow)
            return;
        
        embed.addField(`\`${prefix}${cmd.help.usage}\``, cmd.help.description || "Aucune description.");
    });

    channel.send({ embeds: embeds });
}

/**
 * Display the help for a specific command.
 * @param {ClientContext} ctx
 * @param {GuildMember} member
 * @param {import('../CommandService.js').Command | import('../CommandService.js').CustomAlias | import('../CommandService.js').CustomCommand} command 
 * @param {TextChannel} channel 
 */
async function displayCommandHelp(ctx, member, command, channel) {
    const commandService = ctx.getService(CommandService);
    if (command.help.onTest && !options.test) {
        channel.send("Cette commande est encore en développement.");
        return;
    }
    const allow = await commandService.hasPermission(member, channel, command);
    if (!allow) {
        channel.send(`${member} tu n'as pas les autorisations nécessaires pour cette commande.`);
        return;
    }

    const settingService = ctx.getService(SettingService);
    const prefix = await settingService.getPrefix(channel.guildId);
    const embed = new MessageEmbed()
        .setTitle(`${prefix}${command.help.name}`)
        .setColor(CONSTANTS.COLOR_SETTING)
        .setDescription(command.help.description);
    
    if (command.help.aliases && command.help.aliases.length > 0)
        embed.addField("Alias:", command.help.aliases.map(alias => prefix + alias).join(', '));
    
    if (command.help.usage)
        embed.addField("**Mode d'emploi**", `\`${prefix}${command.help.usage}\``);

    if (command.help.examples && command.help.examples.length > 0) {
        embed.addField("\u200b", "**Exemples:**");
        command.help.examples.forEach(example => {
            embed.addField(`\`${example.noPrefix ? '' : prefix}${example.usage}\``, example.description || "...");
        });
    }
    
    channel.send({ embeds: [embed] });
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    if (args.length === 0) {
        displayCategoryList(ctx, msg.member, msg.channel);
        return;
    }

    const commandService = ctx.getService(CommandService);

    const name = args[0];
    if (commandService.isCategory(name)) {
        displayCommandList(ctx, msg.member, name, msg.channel);
        return;
    }

    const command = commandService.getCommand(msg.guildId, name);
    if (!command) {
        msg.channel.send(`'${name}' n'est pas un nom de commande ou de catégorie valide.`);
        return;
    }
    displayCommandHelp(ctx, msg.member, command, msg.channel);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "help", // The command's name.
    aliases: ["h", "aled"], // A list of aliases for the command.
    usage: "help [<commandName> | <category>]", // The usage of the command.
    description: "Affiche l'aide d'une commande, la liste des catégories ou la liste des commandes disponibles par catégorie.", // The command's description.
    examples: [
        {
            usage: "help",
            description: "Affiche la liste des catégories de commandes disponibles."
        },
        {
            usage: "help Catégorie",
            description: "Affiche la liste des commandes disponibles dans la catégorie 'Catégorie'."
        },
        {
            usage: "help maCommande",
            description: "Affiche l'aide de la commande \`maCommande\`."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
