const { Message, Permissions } = require('discord.js');
const { ClientContext } = require('../ClientContext.js');

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    console.log("Modele pour la commande de base");
}

/**
 * @type {import('./CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "x", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "x", // The usage of the command.
    description: "x", // The command's description.
    examples: [
        {
            usage: "x",
            description: "x"
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.DEFAULT, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: null // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: true, // Whether the command should only be available in testing mode.
}
