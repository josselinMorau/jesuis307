const { Message, Permissions } = require('discord.js');
const { runMoveCmd } = require('./move.js');

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    runMoveCmd(ctx, msg, args, kwargs, true);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "copy", // The command's name.
    aliases: ["cp"], // A list of aliases for the command.
    usage: "copy <#destChannel> [<count>] [--from] [<fromDate> | <fromMessageLink>] [--to] [<toDate> | <toMessageLink>]", // The usage of the command.
    description: "Copie des messages du channel actuel dans le channel donné..", // The command's description.
    examples: [
        {
            usage: "copy #other-channel 10",
            description: "Copie les 10 derniers messages du channel dans #other-channel."
        },
        {
            usage: "copy #other-channel --from 28/11/2021",
            description: "Copie tous les messages à partir du 28 novembre 2021 dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "copy #other-channel --from 28/11/2021 16:00",
            description: "Copie tous les messages à partir du 28 novembre 2021 à 16h00 dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "copy #other-channel --from 16:00",
            description: "Copie tous les messages d'aujourd'hui à partir de 16h00 dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "copy #other-channel --from https://discord.com/channels/123456789/123456789/123456789",
            description: "Copie tous les messages à partir du message correspondant au lien donné dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "copy #other-channel --from 123456789",
            description: "Copie tous les messages à partir du message dont l'id est '123456789' dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "copy #other-channel 20 --from 000000001 --to 123456789",
            description: "Copie les 20 messages les plus récents entre le message dont l'id est '000000001' et le message dont l'id est '123456789' dans #other-channel."
        }
    ],
    permissions: {
        flag: Permissions.FLAGS.MANAGE_MESSAGES,
        op: "all"
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
