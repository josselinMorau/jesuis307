const { Message, Permissions, TextChannel } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { NotImplemented, OutOfRange } = require('../../utils/errors.js');

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const page = args.length > 0 ? parseInt(args[0]) : 0;
    if (isNaN(page) || page < 0) {
        msg.channel.send("L'index de la page n'est pas valide.");
        return;
    }

    logger.fetchLogs(page).then(fileOptions => {
        msg.channel.send({ files: [fileOptions]}).catch(err => {
            logger.error(err);
            msg.channel.send("Une erreur est survenue.");
        });
    })
    .catch(err => {
        if (err instanceof NotImplemented) {
            msg.channel.send("Impossible de récupérer les logs avec le mode actuel.");
            return;
        }
        if (err instanceof OutOfRange) {
            msg.channel.send("Il n'existe pas de log pour la page donnée.");
            return;
        }
        logger.error(err);
        msg.channel.send("Une erreur est survenue.");
    });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "logs", // The command's name.
    aliases: ["log"], // A list of aliases for the command.
    usage: "logs [<page>]", // The usage of the command.
    description: "Affiche les logs du bot.", // The command's description.
    examples: [
        {
            usage: "logs",
            description: "Affiche les derniers logs du bot."
        },
        {
            usage: "logs 2",
            description: "Affiche la 2e page de logs du bot. L'index commence par 0."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "all" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
