const { Message, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { JailService, JAIL_SERVICE_ERRORS } = require('../../misc/JailService.js');
const { ChannelService, CHANNEL_CATEGORIES } = require('../../utils/channels/ChannelService.js');
const { SettingService, SETTING_NAMES } = require('../../utils/guildSettings/SettingsService.js');
const { parseUserId } = require('../../utils/helper.js');

/**
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
async function runCmd(ctx, msg, args, kwargs) {
    const author = msg.member;
    const target = msg.guild.members.cache.get(parseUserId(args[0]));

    if (!target) {
        msg.channel.send(`Tu n'as pas précisé de cible valide.`);
        return;
    }

    if (target.id === msg.guild.me.id) {
        msg.channel.send("T'as cru que tu pouvais m'envoyer en prison ? :rofl:");
        return;
    }

    if (target.id === author.id) {
        msg.channel.send(`Désolé ${author} mais je vais pas te laisser t'envoyer toi-même en prison.`);
        return;
    }

    if (target.user.bot) {
        msg.channel.send("Désolé mais je n'envoie pas en prison mes camarades robots. :robot:");
        return;
    }

    // Check if the user has a higher role than the target.
    if (author.roles.highest.comparePositionTo(target.roles.highest) <= 0) {
        msg.channel.send(`${author} Tu n'as pas les autorisations nécessaire pour mettre en prison ${target}.`);
        return;
    }

    var duration = 0;
    var rawDuration = 0;
    var isHour = false;

    if (args.length >= 2) {
        duration = parseInt(args[1]);
        rawDuration = duration;
        isHour = args[1].toLowerCase().endsWith("h") || (args[2] && args[2].toLowerCase() === "h");
        if (isHour)
            duration *= 60;
    }
    else {
        const settingService = ctx.getService(SettingService);
        duration = await settingService.getSetting(msg.guildId, SETTING_NAMES.JAIL_DEFAULT_DURATION);
        rawDuration = duration;
    }

    if (!duration || duration <= 0) {
        msg.channel.send("La durée donnée n'est pas valide.");
        return;
    }
    // convert to ms
    duration *= 60_000;

    const reason = kwargs.reason ? kwargs.reason.join(" ") : null;
    
    const jailService = ctx.getService(JailService);
    jailService.jail(target, { duration: duration, channel: msg.channel })
        .then(async () => {
            const { guild } = msg;
            const channelService = ctx.getService(ChannelService);
            const jailChannel = await channelService.getChannelWithCategory(guild, CHANNEL_CATEGORIES.JAIL);
            if (!jailChannel)
                return;
            const ruleChannel = (await channelService.getChannelWithCategory(guild, CHANNEL_CATEGORIES.RULE)) || guild.rulesChannel;
            const content = `${target}, tu as été envoyé(e) en prison par ${author} pour ${rawDuration} ${isHour ? 'heure' : 'minute'}${rawDuration > 1 ? 's' : ''}` + 
                (reason ? ` pour la raison suivante : \n${reason}` : ".") + 
                "\nTu ferais mieux de réfléchir à ton comportement jeune rebut de la société. :angry:" + 
                (ruleChannel ? `\nJe t'invite à lire le règlement ici :point_right: ${ruleChannel}` : "");
            jailChannel.send(content)
                .catch((_err) => logger.error(_err));
        })
        .catch((error) => {
            switch (error) {
                case JAIL_SERVICE_ERRORS.ALREADY_IN_JAIL: {
                    msg.channel.send(`${target} est déjà en prison !`);
                    break;
                }
                case JAIL_SERVICE_ERRORS.CANT_BE_MANAGED: {
                    msg.channel.send(`Désolé je ne peux pas envoyer ${target} en prison. Je ne suis pas assez puissant pour. :smiling_face_with_tear:`);
                    break;
                }
                case JAIL_SERVICE_ERRORS.NO_PRISONER_ROLE: {
                    msg.channel.send(`Impossible d'envoyer quelqu'un en prison tant qu'aucun rôle PRISONER n'a été défini.`);
                    break;
                }
                case JAIL_SERVICE_ERRORS.OPERATION_FAILED: default: {
                    msg.channel.send("Une erreur est survenue.");
                    logger.error(error);
                    break;
                }
            }
        });
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    runCmd(ctx, msg, args, kwargs);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "jail", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "jail <@target> [<durationInMin> | <durationInHour>h] [--reason] [<reason...>]", // The usage of the command.
    description: "Envoie temporairement un utilisateur dans le channel prison.", // The command's description.
    examples: [
        {
            usage: "jail @monUser",
            description: "Envoie le user `monUser` dans le channel prison. Sa durée d'emprisonnement dépend du setting 'jailDefaultDuration'."
        },
        {
            usage: "jail @monUser 10",
            description: "Envoie le user `monUser` dans le channel prison pendant 10 minutes."
        },
        {
            usage: "jail @monUser 10h",
            description: "Envoie le user `monUser` dans le channel prison pendant 10 heures."
        },
        {
            usage: "jail @monUser --reason Il a été irrespectueux.",
            description: "Envoie le user `monUser` dans le channel prison pour le motif 'Il a été irrespectueux.'. Sa durée d'emprisonnement dépend du setting 'jailDefaultDuration'."
        },
        {
            usage: "jail @monUser 10 --reason Il a été irrespectueux.",
            description: "Envoie le user `monUser` dans le channel prison pendant 10 minutes pour le motif 'Il a été irrespectueux.'."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR | Permissions.FLAGS.KICK_MEMBERS, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "any" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
