const { Message, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { JailService, JAIL_SERVICE_ERRORS } = require('../../misc/JailService.js');
const { parseUserId } = require('../../utils/helper.js');


/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const jailService = ctx.getService(JailService);

    const all = !!kwargs.all;
    if (all) {
        jailService.freeAll(msg.guild).then(() => {
            msg.channel.send("Vous êtes tous libres ! :dove:");
        })
        .catch(error => {
            switch (error) {
                case JAIL_SERVICE_ERRORS.NOT_IN_JAIL: {
                    msg.channel.send("Il n'y a personne en prison en ce moment.");
                    break;
                }
                default: {
                    msg.channel.send("Une erreur est survenue.");
                    logger.error(error);
                    break;
                }
            }
        });
        return;
    }

    const target = msg.guild.members.cache.get(parseUserId(args[0]));

    if (!target) {
        msg.channel.send(`Tu n'as pas précisé de cible valide.`);
        return;
    }
    
    jailService.free(target, { channel: msg.channel }).catch(error => {
        switch (error) {
            case JAIL_SERVICE_ERRORS.NOT_IN_JAIL: {
                msg.channel.send(`${target} n'est pas en prison.`);
                break;
            }
            case JAIL_SERVICE_ERRORS.CANT_BE_MANAGED: {
                msg.channel.send(`Désolé je ne peux pas libérer ${target} de prison. Je ne suis pas assez puissant pour. :smiling_face_with_tear:`);
                break;
            }
            case JAIL_SERVICE_ERRORS.NO_PRISONER_ROLE: {
                msg.channel.send(`Impossible de libérer quelqu'un de la prison tant qu'aucun rôle PRISONER n'a été défini.`);
                break;
            }
            case JAIL_SERVICE_ERRORS.OPERATION_FAILED: default: {
                msg.channel.send("Une erreur est survenue.");
                logger.error(error);
                break;
            }
        }
    });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "free", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "free [<@monUser>] [--all]", // The usage of the command.
    description: "Envoie temporairement un utilisateur dans le channel prison.", // The command's description.
    examples: [
        {
            usage: "free @monUser",
            description: "Libère `monUser` du channel prison."
        },
        {
            usage: "free --all",
            description: "Libère tous les membres du serveur actuellement en prison."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR | Permissions.FLAGS.KICK_MEMBERS, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "any" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
