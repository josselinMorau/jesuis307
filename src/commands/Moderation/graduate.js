const { Message, Permissions, Guild, TextChannel } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { waitForUserAnswer } = require('../../utils/helper.js');
const { RoleService, ROLE_NAMES } = require('../../utils/roles/RoleService.js');
const { TaskExecutor, TASK_TYPES } = require('../TaskExecutor.js');

/**
 * 
 * @param {ClientContext} ctx 
 * @param {Message} msg
 */
async function graduate(ctx, msg) {
    const { channel, author, guild } = msg;
    const roleService = ctx.getService(RoleService);
    const ing1 = await roleService.getRoleWithName(guild, ROLE_NAMES.ING1);
    const ing2 = await roleService.getRoleWithName(guild, ROLE_NAMES.ING2);
    const ing3 = await roleService.getRoleWithName(guild, ROLE_NAMES.ING3);
    const ancien = await roleService.getRoleWithName(guild, ROLE_NAMES.ANCIEN);

    if (!ing1 || !ing2 || !ing3 || !ancien) {
        channel.send("La promotion est impossible si tous les rôles de promo ne sont pas définis.");
        return;
    }

    const roles = [ing1, ing2, ing3, ancien];

    const [answer, confirMsg] = await waitForUserAnswer(author, channel, 120_000, { content: "Es-tu sûr(e) de vouloir faire passer de classe tous les membres du serveur ?" },
        [{ label: "Oui", style: "PRIMARY"},
        { label: "Non", style: "DANGER" }]
    );

    confirMsg.delete().catch(() => {});

    if (answer !== "Oui") {
        msg.channel.send("Promotion annulée.");
        return;
    }

    msg.channel.send("Promotion démarrée.");

    const members = await guild.members.fetch();
    const promises = [];
    for (let member of members.values()) {
        for (let i=0; i<roles.length-1; ++i) {
            if (member.roles.cache.has(roles[i].id)) {
                promises.push(
                    member.roles.remove(roles[i]).catch(() => {})
                );
                promises.push(
                    member.roles.add(roles[i+1]).catch(() => {})
                );
                break;
            }
        }
    }
    await Promise.all(promises);
    channel.send("@everyone Bravo à tous pour votre promotion !!! 🥳🥳🥳 \n ||Et tant pis aux anciens qui se font ping pour rien. :upside_down:||")
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const taskExecutor = ctx.getService(TaskExecutor);
    taskExecutor.tryExecuteTaskWithDefaultCallback(msg.channel, TASK_TYPES.MANAGE_ROLES, async () => {
        await graduate(ctx, msg).catch(err => {
            logger.error(err);
            msg.channel.send("Une erreur est survenue.");
        });
    });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "graduate", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "graduate", // The usage of the command.
    description: "Met à jour les rôles de promo de tous les membres du serveur.", // The command's description.
    examples: [], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "all" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
