const { Message, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { waitForUserAnswer } = require('../../utils/helper.js');

/**
 * 
 * @param {ClientContext} ctx 
 * @param {Message} msg
 * @param {boolean} skipValidation 
 * @param {boolean} shouldRestart
 */
async function kill(ctx, msg, skipValidation, shouldRestart) {
    if (!skipValidation) {
        const content = `${msg.author} Es-Tu sûr(e) de vouloir ${shouldRestart ? 'redémarrer' : 'arrêter'} le bot ?`;
        const [answer, confirMsg] = await waitForUserAnswer(msg.author, msg.channel, 30_000, { content: content }, [
            { label: "Oui", customId: "confirm", style: "PRIMARY" },
            { label: "Non", customId: "cancel", style: "DANGER"}
        ]);
        await confirMsg.delete().catch(_err => logger.error(_err));
        if (answer !== "confirm") {
            msg.channel.send("L'opération a été annulée.");
            return;
        }
    }

    await msg.channel.send("Bye bye ! :wave:");
    ctx.terminate(shouldRestart);
}

/**
 * Inner code command.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 * @param {boolean} restart 
 */
module.exports.runKillCmd = (ctx, msg, args, kwargs, restart = false) => {
    const skip = !!kwargs.skip;
    kill(ctx, msg, skip, restart);
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    this.runKillCmd(ctx, msg, args, kwargs, true);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "restart", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "restart [--skip]", // The usage of the command.
    description: "Redémarre le bot.", // The command's description.
    examples: [
        {
            usage: "restart",
            description: "Redémarre le bot. Un message de validation sera envoyé."
        },
        {
            usage: "restart --skip",
            description: "Redémarre le bot sans message de validation."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "all" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
