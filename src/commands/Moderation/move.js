const { Message, MessageEmbed, Permissions, TextChannel } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { InvalidParams } = require('../../utils/errors.js');
const { fetchMessages, waitForUserAnswer, sleep, parseChannelId, parseFetchOptions, CONSTANTS } = require('../../utils/helper.js');
const { TaskExecutor, TASK_TYPES } = require('../TaskExecutor.js');

/**
 * @typedef {object} MessageCopyInfo
 * @property {import('discord.js').MessageOptions} options
 * @property {string} originalId
 * @property {import('discord.js').EmojiIdentifierResolvable[]} emojis
 */

/**
 * 
 * @param {Message} msg 
 * @returns {MessageCopyInfo}
 */
function createMessageCopy(msg) {    
    const attachments = [...msg.attachments.values()];
    const embeds = [];
    var imageUrl = null;
    // This avoid to create copy of copy.
    if (msg.content || msg.author.id !== msg.guild.me.id) {
        // Create an embed to summarize the content of the original message.
        const embed = new MessageEmbed()
            .setDescription(msg.content || '\u200b') // Prevent empty content.
            .setTimestamp(msg.createdAt)
            .setAuthor(`${msg.member.displayName}`, msg.author.avatarURL())
            .setColor(CONSTANTS.COLOR_MESSAGE)
            .addField("De:", `${msg.member}`, true)
            .addField("Dans:", `${msg.channel}`, true);
        
        const image = attachments.find(attachment => attachment.contentType && attachment.contentType.includes("image"));
        imageUrl = image ? image.url : null;
        if (imageUrl)
            embed.setImage(imageUrl);
        embeds.push(embed);
    }

    return {
        options: {
            files: attachments.filter(attachment => attachment.url !== imageUrl),
            // We can't have more than 10 embeds per message so we have to get rid of the 10th one.
            embeds: embeds.concat(msg.embeds.slice(0, CONSTANTS.DISCORD_EMBED_PER_MESSAGE_MAX_COUNT-embeds.length)),
            reply: msg.type === "REPLY" ? {
                messageReference: msg.reference.messageId,
                failIfNotExists: false
            } : undefined,
        },
        originalId: msg.id,
        emojis: [...msg.reactions.cache.keys()]
    };
}

/**
 * Move messages.
 * @param {Message} commandMessage 
 * @param {TextChannel} destChannel
 * @param {object} fetchOptions 
 * @param {boolean} copy If `true` don't destroy the original messages.
 */
async function move(commandMessage, destChannel, fetchOptions, copy = false) {
    const { author, channel } = commandMessage;

    const confirmContent = `${author} Tu t'apprêtes à ${copy ? 'copier' : 'déplacer'} ${(fetchOptions.count !== undefined ? fetchOptions.count : 'des')} message(s).\n Es-tu sûr(e) de vouloir continuer ?`;
    const embeds = [];
    if (fetchOptions.from || fetchOptions.to) {
        const embed = new MessageEmbed()
            .setTitle("Détails de l'opération:");
        if (fetchOptions.from)
            embed.addField("À partir de:", `\u200b${fetchOptions.from}`); // \u200b is empty character to prevent empty string errors
        if (fetchOptions.to)
            embed.addField("Jusqu'à:", `\u200b${fetchOptions.to}`);
        embeds.push(embed);
    }

    const [answer, confirmMsg] = await waitForUserAnswer(author, channel, 60_000, { embeds: embeds, content: confirmContent }, [
        { customId: "confirm", label: "OUI", style: "PRIMARY" },
        { customId: "cancel", label: "NON", style: "DANGER" }
    ]);

    await confirmMsg.delete().catch(_err => logger.error(_err));
    
    if (!answer || answer !== "confirm") // Cancel operation.
    {
        channel.send("L'opération a été annulée.");
        return;
    }

    const copies = [];
    const deletePromises = [];
    
    for await (let msg of fetchMessages(channel, fetchOptions)) {
        copies.push(createMessageCopy(msg));
        if (!copy)
            deletePromises.push(
                msg.delete().catch(_err => logger.error(_err))
            );
    }
        
    const replyRemapper = new Map();
        
    const promises = [];
    // We have to collect and iterate backwards the copied messages because Discord's API
    // only allow to iterate from newer to older messages.
    for (let i=copies.length-1; i>=0; --i) {
        const copy = copies[i];
        if (copy.options.reply) {
            // If a reply is necessary we need to make sure the previous messages
            // were all posted in order to reproduce the reply correctly.
            await Promise.all(promises);
            promises.length = 0;
            // remap the reply to the copy
            copy.options.reply.messageReference = replyRemapper.get(copy.options.reply.messageReference);
        }
        const promise = destChannel.send(copy.options)
            .then((copyMsg) => {
                replyRemapper.set(copy.originalId, copyMsg.id);
                copy.emojis.forEach(emoji => {
                    copyMsg.react(emoji).catch(() => {});
                });
            })
            .catch(_err => logger.error(_err));
        promises.push(promise);
    }

    await Promise.all(promises);
    await Promise.all(deletePromises);
    const autoDestroyMsg = await commandMessage.channel.send(`Message(s) ${copy ? 'copié(s)' : 'déplacé(s)'} de ${channel} à ${destChannel}.\nCe message s'autodétruira dans 10 secondes...`);
    await sleep(10_000);
    autoDestroyMsg.delete().catch(_err => logger.error(_err));
}

/**
 * The internal command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 * @param {boolean} copy Whether the operation should be a copy instead of a move.
 */
module.exports.runMoveCmd = (ctx, msg, args, kwargs, copy = false) => {
    if (!copy && !msg.channel.permissionsFor(msg.guild.me).has(Permissions.FLAGS.MANAGE_CHANNELS)) {
        msg.channel.send(`Impossible d'exécuter la commande parce que je n'ai pas les droits de supprimer les messages dans ${msg.channel}.`);
        return;
    }

    if (args.length < 1) {
        msg.channel.send(`Tu n'as pas précisé de nom du channel.`);
        return;
    }

    const destChannelId = parseChannelId(args[0]);
    const destChannel = msg.guild.channels.cache.get(destChannelId);

    if (!destChannel) {
        msg.channel.send(`${args[0]} n'est pas un channel valide.`);
        return;
    }

    if (!destChannel.permissionsFor(msg.guild.me).has(Permissions.FLAGS.SEND_MESSAGES)) {
        msg.channel.send(`Impossible d'exécuter la commande parce que je n'ai pas les droits d'écrire dans ${destChannel}.`);
        return;
    }

    if (destChannel.id === msg.channelId) {
        msg.channel.send("Inutile de bouger les messages là où ils sont déjà.");
        return;
    }

    const fetchOptions = parseFetchOptions(args, kwargs, { countIndex: 1 });
    
    const taskExecutor = ctx.getService(TaskExecutor);
    taskExecutor.tryExecuteTaskWithDefaultCallback(msg.channel, TASK_TYPES.MANAGE_MESSAGES, async () => {
        await move(msg, destChannel, fetchOptions, copy).catch(_err => {
            logger.error(_err);
            if (_err instanceof InvalidParams) {
                msg.channel.send("Arguments invalides. Vérifie d'avoir donné l'argument 'count' ou l'option 'from'.");
                return;
            }
            msg.channel.send("Une erreur est survenue.");
        });
    });
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    this.runMoveCmd(ctx, msg, args, kwargs);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "move", // The command's name.
    aliases: ["mv"], // A list of aliases for the command.
    usage: "move <#destChannel> [<count>] [--from] [<fromDate> | <fromMessageLink>] [--to] [<toDate> | <toMessageLink>]", // The usage of the command.
    description: "Déplace des messages du channel actuel dans le channel donné..", // The command's description.
    examples: [
        {
            usage: "move #other-channel 10",
            description: "Déplace les 10 derniers messages du channel dans #other-channel."
        },
        {
            usage: "move #other-channel --from 28/11/2021",
            description: "Déplace tous les messages à partir du 28 novembre 2021 dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "move #other-channel --from 28/11/2021 16:00",
            description: "Déplace tous les messages à partir du 28 novembre 2021 à 16h00 dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "move #other-channel --from 16:00",
            description: "Déplace tous les messages d'aujourd'hui à partir de 16h00 dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "move #other-channel --from https://discord.com/channels/123456789/123456789/123456789",
            description: "Déplace tous les messages à partir du message correspondant au lien donné dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "move #other-channel --from 123456789",
            description: "Déplace tous les messages à partir du message dont l'id est '123456789' dans #other-channel. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "move #other-channel 20 --from 000000001 --to 123456789",
            description: "Déplace les 20 messages les plus récents entre le message dont l'id est '000000001' et le message dont l'id est '123456789' dans #other-channel."
        }
    ],
    permissions: {
        flag: Permissions.FLAGS.MANAGE_MESSAGES,
        op: "all"
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
