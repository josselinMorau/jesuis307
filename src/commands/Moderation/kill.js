const { Message, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { runKillCmd } = require('./restart.js');

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client context;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    runKillCmd(ctx, msg, args, kwargs, false);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "kill", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "kill [--skip]", // The usage of the command.
    description: "Arrête l'éxecution du bot.", // The command's description.
    examples: [
        {
            usage: "kill",
            description: "Arrête l'éxecution du bot. Un message de validation sera envoyé."
        },
        {
            usage: "kill --skip",
            description: "Arrête l'éxecution du bot sans message de validation."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "all" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
