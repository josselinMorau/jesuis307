const { Message, MessageEmbed, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { InvalidParams } = require('../../utils/errors.js');
const { fetchMessages, parseFetchOptions, sleep, waitForUserAnswer } = require('../../utils/helper.js');
const { TaskExecutor, TASK_TYPES } = require('../TaskExecutor.js');

/**
 * Clear messages.
 * @param {Message} commandMessage 
 * @param {object} fetchOptions 
 */
async function clear(commandMessage, fetchOptions) {
    const { author, channel } = commandMessage;

    const confirmContent = `${author} Tu t'apprêtes à supprimer ${(fetchOptions.count !== undefined ? fetchOptions.count : 'des')} message(s).\n Es-tu sûr(e) de vouloir continuer ?`;
    const embeds = [];
    if (fetchOptions.from || fetchOptions.to) {
        const embed = new MessageEmbed()
            .setTitle("Détails de l'opération:");
        if (fetchOptions.from)
            embed.addField("À partir de:", `\u200b${fetchOptions.from}`); // \u200b is empty character to prevent empty string errors
        if (fetchOptions.to)
            embed.addField("Jusqu'à:", `\u200b${fetchOptions.to}`);
        embeds.push(embed);
    }

    const [answer, confirmMsg] = await waitForUserAnswer(author, channel, 60_000, { embeds: embeds, content: confirmContent }, [
        { customId: "confirm", label: "OUI", style: "PRIMARY" },
        { customId: "cancel", label: "NON", style: "DANGER" }
    ]);

    confirmMsg.delete().catch(_err => logger.error(_err));
    
    if (!answer || answer !== "confirm") // Cancel operation.
    {
        channel.send("L'opération a été annulée.");
        return;
    }

    const promises = [];
    for await (let msg of fetchMessages(channel, fetchOptions)) {
        promises.push(
            msg.delete().catch(_err => logger.error(_err))
        );
    }

    await Promise.all(promises);
    const autoDestroyMsg = await commandMessage.channel.send("Le ménage a été fait dans ce channel ! :broom:\nCe message s'autodétruira dans 10 secondes...");
    await sleep(10_000);
    autoDestroyMsg.delete().catch(_err => logger.error(_err));
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    if (!msg.channel.permissionsFor(msg.guild.me).has(Permissions.FLAGS.MANAGE_CHANNELS)) {
        msg.channel.send(`Impossible d'effectuer la commande parce que je n'ai pas les droits de supprimer les messages dans ${msg.channel}.`);
        return;
    }

    const fetchOptions = parseFetchOptions(args, kwargs, { countIndex: 0 });

    const taskExecutor = ctx.getService(TaskExecutor);
    taskExecutor.tryExecuteTaskWithDefaultCallback(msg.channel, TASK_TYPES.MANAGE_MESSAGES, async () => {
        await clear(msg, fetchOptions).catch(_err => {
            logger.error(_err);
            if (_err instanceof InvalidParams) {
                msg.channel.send("Arguments invalides. Vérifie d'avoir donné l'argument 'count' ou l'option 'from'.");
                return;
            }
            msg.channel.send("Une erreur est survenue.");
        });
    });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "clear", // The command's name.
    aliases: ["clean", "remove", "rm"], // A list of aliases for the command.
    usage: "clear [<count>] [--from] [<fromDate> | <fromMessageLink>] [--to] [<toDate> | <toMessageLink>]", // The usage of the command.
    description: "Supprime des messages du channel actuel.", // The command's description.
    examples: [
        {
            usage: "clear 10",
            description: "Supprime les 10 derniers messages du channel."
        },
        {
            usage: "clear --from 28/11/2021",
            description: "Supprime tous les messages à partir du 28 novembre 2021. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "clear --from 28/11/2021 16:00",
            description: "Supprime tous les messages à partir du 28 novembre 2021 à 16h00. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "clear --from 16:00",
            description: "Supprime tous les messages d'aujourd'hui à partir de 16h00. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "clear --from https://discord.com/channels/123456789/123456789/123456789",
            description: "Supprime tous les messages à partir du message correspondant au lien donné. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "clear --from 123456789",
            description: "Supprime tous les messages à partir du message dont l'id est '123456789'. L'option `to` fonctionne de la même manière."
        },
        {
            usage: "clear 20 --from 000000001 --to 123456789",
            description: "Supprime les 20 messages les plus récents entre le message dont l'id est '000000001' et le message dont l'id est '123456789'."
        }
    ],
    permissions: {
        flag: Permissions.FLAGS.MANAGE_MESSAGES,
        op: "all"
    },
    delete: true, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
