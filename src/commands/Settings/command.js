const { Message, MessageEmbed, Permissions, TextChannel } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { CONSTANTS, parseBoolean, createEmbedsFromData } = require('../../utils/helper.js');
const { CommandService, COMMAND_ERRORS } = require('../CommandService.js');

/**
 * Display the list of all custom commands.
 * @param {CommandService} commandService
 * @param {TextChannel} channel 
 */
function displayCustomCommandList(commandService, channel) {
    const commands = commandService.getCustomCommandList(channel.guildId);
    if (!commands || commands.length === 0) {
        channel.send("Aucune commande custom n'est définie sur le serveur.");
        return;
    }

    const embeds = createEmbedsFromData({
        title: "Commandes customs",
        color: CONSTANTS.COLOR_SETTING
    }, commands, (embed, cmd) => {
        embed.addField(`\`${cmd.help.name}\``, `**Supprime le message:** ${cmd.help.delete ? '✅' : '❌'}`)
    });
    channel.send({embeds: embeds});
}

/**
 * Display infos on a custom command.
 * @param {import('../CommandService.js').CustomCommand} customCmd 
 * @param {TextChannel} channel 
 */
function displayCustomCommandInfo(customCmd, channel) {
    const embed = new MessageEmbed()
        .setTitle(customCmd.help.name)
        .setColor(CONSTANTS.COLOR_SETTING)
        .setDescription(customCmd.content)
        .addField("Supprime le message:", customCmd.help.delete ? '✅\u200b' : '❌\u200b'); // \u200b allow to avoid the emoji to be oversized on mobile screen
    channel.send({embeds: [embed]});
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const commandService = ctx.getService(CommandService);

    if (args.length === 0) {
        displayCustomCommandList(commandService, msg.channel);
        return;
    }

    const cmdName = args[0];

    const _new = kwargs.new;
    const rm = kwargs.remove || kwargs.rm;
    const option = _new || rm;

    if (!option) {
        const cmd = commandService.getCommand(msg.guildId, cmdName);
        if (!cmd || cmd.type !== "custom") {
            msg.channel.send(`'${cmdName}' n'est pas un nom de commande custom.`);
        }
        else {
            displayCustomCommandInfo(cmd, msg.channel);
        }
        return;
    }

    if (option === _new) {
        const content = _new.join(" ");
        const dl = kwargs.deleteMsg || kwargs.dlmsg;
        const deleteMsg = dl && (dl.length === 0 || parseBoolean(dl[0]));
        commandService.addCustomCommand(msg.guildId, cmdName, content, deleteMsg).then(([success, errMsg]) => {
            if (success) {
                msg.channel.send(`La commande '${cmdName}' a bien été créée.`);
                return;
            }

            switch (errMsg) {
                case COMMAND_ERRORS.INVALID_OPERATION_ON_BUILTIN_COMMAND:
                    msg.channel.send(`Impossible de créer la commande car '${cmdName}' est déjà une commande par défaut.`);
                    break;
                case COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND:
                    msg.channel.send(`Impossible de créer la commande car '${cmdName}' est déjà un nom d'alias.`);
                    break;
                case COMMAND_ERRORS.ALREADY_EXISTING_CUSTOM_COMMAND:
                    msg.channel.send(`Impossible de créer la commande car '${cmdName}' est déjà un nom de commande custom.`);
                    break;
                case COMMAND_ERRORS.INVALID_PARAMS:
                    msg.channel.send(`Tu n'as pas précisé le contenu du message de la commande.`);
                    break;
                case COMMAND_ERRORS.CREATE_FAILED: default:
                    msg.channel.send("La création de la commande a échoué.");
                    break;
            }
        });
    }
    else if (option === rm) {
        commandService.removeCustomCommand(msg.guildId, cmdName).then(([success, errMsg]) => {
            if (success) {
                msg.channel.send(`La commande '${cmdName}' a bien été supprimée.`);
                return;
            }

            switch (errMsg) {
                case COMMAND_ERRORS.INVALID_OPERATION_ON_BUILTIN_COMMAND:
                    msg.channel.send(`Impossible de supprimer '${cmdName}' car c'est une commande par défaut.`);
                    break;
                case COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND:
                    msg.channel.send(`Impossible de supprimer '${cmdName}' car c'est un alias.`);
                    break;
                case COMMAND_ERRORS.NO_SUCH_CUSTOM_COMMAND:
                    msg.channel.send(`'${cmdName}' n'est pas un nom de commande custom.`);
                    break;
                case COMMAND_ERRORS.DELETE_FAILED: default:
                    msg.channel.send("La suppression de la commande a échoué.");
                    break;
            }
        });
    }
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "command", // The command's name.
    aliases: ["cmd"], // A list of aliases for the command.
    usage: "command [<customCommandName>] [--new] [--remove | --rm] [<content...>] [--deleteMsg | --dlmsg] [<deleteMessage>]", // The usage of the command.
    description: "Gère les commandes customs du serveur.", // The command's description.
    examples: [
        {
            usage: "command",
            description: "Affiche la liste de toutes les commandes customs définies sur le serveur."
        },
        {
            usage: "command maCommande",
            description: "Affiche les infos concernant la commande custome `maCommande`."
        },
        {
            usage: "command maCommande --new Bonjour tout le monde.",
            description: "Crée une commande custom `maCommande` qui envoie le message `Bonjour tout le monde.`."
        },
        {
            usage: "command maCommande --new Bonjour tout le monde. --deleteMsg true",
            description: "Crée une commande custom `maCommande` qui envoie le message `Bonjour tout le monde.`." + 
                "\n`--deleteMsg true` permet de préciser que le message de la commande sera supprimé une fois exécutée. Par défaut `--deleteMsg` vaut `false`."
        },
        {
            usage: "command maCommande --remove",
            description: "Supprime la commande custom `maCommande`."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "all" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
