const { Message, MessageEmbed, Permissions, TextChannel } = require('discord.js');
const { CONSTANTS, createEmbedsFromData } = require('../../utils/helper.js');
const { CommandService, COMMAND_ERRORS } = require('../CommandService.js');
const { SettingService } = require('../../utils/guildSettings/SettingsService.js');
const { ClientContext } = require('../../ClientContext.js');

/**
 * Display all existing aliases in a guild.
 * @param {ClientContext} ctx
 * @param {TextChannel} channelToWrite 
 */
async function displayAllGuildAliases(ctx, channelToWrite) {
    const settingService = ctx.getService(SettingService);
    const commandService = ctx.getService(CommandService);

    const prefix = await settingService.getPrefix(channelToWrite.guildId);
    const aliases = commandService.getAllCustomAliases(channelToWrite.guildId);
    if (!aliases || aliases.length === 0) {
        channelToWrite.send("Aucun alias n'a été défini.");
        return;
    }

    const embeds = createEmbedsFromData({ title: "Alias", color: CONSTANTS.COLOR_SETTING }, aliases, (embed, alias) => {
        embed.addField(`${prefix}${alias.aliasName}`, `${prefix}${alias.help.name} ${alias.aliasArgs}`);
    });
    
    channelToWrite.send({ embeds: embeds });
}

/**
 * Display infos one alias.
 * @param {ClientContext} ctx
 * @param {import('../CommandService.js').CustomAlias} alias
 * @param {TextChannel} channelToWrite 
 */
async function displayOneAlias(ctx, alias, channelToWrite) {
    const settingService = ctx.getService(SettingService);
    const prefix = await settingService.getPrefix(channelToWrite.guildId);
    const embed = new MessageEmbed()
        .setTitle(`${prefix}${alias.aliasName}`)
        .setColor(CONSTANTS.COLOR_SETTING)
        .setDescription(`${prefix}${alias.help.name} ${alias.aliasArgs}`);
    channelToWrite.send({ embeds: [embed] });
}

/**
 * Display all aliases of a command.
 * @param {ClientContext} ctx
 * @param {string} cmdName 
 * @param {TextChannel} channelToWrite 
 */
async function displayAllAliasesOfCommand(ctx, cmdName, channelToWrite) {
    const settingService = ctx.getService(SettingService);
    const commandService = ctx.getService(CommandService);

    const prefix = await settingService.getPrefix(channelToWrite.guildId);
    const aliases = commandService.getAllCustomAliasesOfCommand(channelToWrite.guildId, cmdName);
    if (!aliases || aliases.length === 0) {
        channelToWrite.send(`Aucun alias n'a été défini pour la commande '${cmdName}'.`);
        return;
    }

    const embeds = [];
    var embed = new MessageEmbed()
        .setTitle(`Alias de '${cmdName}'`)
        .setColor(CONSTANTS.COLOR_SETTING);
    
    aliases.forEach(alias => {
        if (embed.fields.length >= CONSTANTS.DISCORD_EMBED_FIELD_MAX_COUNT) {
            embeds.push(embed);
            embed = new MessageEmbed()
                .setTitle(`Alias de '${cmdName}'`)
                .setColor(CONSTANTS.COLOR_SETTING);
        }

        embed.addField(`${prefix}${alias.aliasName}`, `${prefix}${alias.help.name} ${alias.aliasArgs}`);
    });

    if (embed.fields.length > 0)
        embeds.push(embed);
    
    channelToWrite.send({ embeds: embeds });
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    if (args.length === 0) {
        displayAllGuildAliases(ctx, msg.channel);
        return;
    }

    const commandService = ctx.getService(CommandService);

    const name = args[0];
    const remove = !!(kwargs.remove || kwargs.rm);
    if (remove) {
        commandService.removeOneCustomAlias(msg.guildId, name).then(([success, errorMsg]) => {
            if (success) {
                msg.channel.send(`L'alias '${name}' a bien été supprimé.`);
                return;
            }
            
            switch (errorMsg) {
                case COMMAND_ERRORS.INVALID_OPERATION_ON_BUILTIN_COMMAND:
                    msg.channel.send(`Impossible de supprimer '${name}' car c'est une commande par défaut.`);
                    break;
                case COMMAND_ERRORS.INVALID_OPERATION_ON_BUILTIN_COMMAND:
                    msg.channel.send(`Impossible de supprimer '${name}' car c'est une commande custom.`);
                    break;
                case COMMAND_ERRORS.NO_SUCH_ALIAS:
                    msg.channel.send(`Aucun alias ne s'appelle '${name}'.`);
                    break;
                case COMMAND_ERRORS.DELETE_FAILED: default:
                    msg.channel.send(`La suppression de l'alias a échoué.`);
                    break;
            }
        });
        return;
    }

    const cmd = commandService.getCommand(msg.guildId, name);
    if (!cmd) {
        msg.channel.send(`Aucune commande ni alias ne s'appelle '${name}'.`);
        return;
    }

    if (cmd.type === "alias") {
        displayOneAlias(ctx, cmd, msg.channel);
    }
    else {
        displayAllAliasesOfCommand(ctx, cmd.help.name, msg.channel);
    }
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "alias", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "alias [<commandName> | <aliasName>] [--remove | --rm]", // The usage of the command.
    description: "Gère les alias de commandes existants. Pour créer une commande voir `add-alias`.", // The command's description.
    examples: [
        {
            usage: "alias",
            description: "Affiche la liste de tous les alias définis sur le serveur."
        },
        {
            usage: "alias maCommande",
            description: "Affiche la liste de tous les alias de `maCommande`"
        },
        {
            usage: "alias monAlias",
            description: "Affiche les informations sur `monAlias`"
        },
        {
            usage: "alias monAlias --rm",
            description: "Supprime l'alias `monAlias`."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "all" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
