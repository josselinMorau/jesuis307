const { Message, Permissions, MessageEmbed, TextChannel, Role } = require('discord.js');
const { RoleService, isValidRoleName, getRoleNames } = require('../../utils/roles/RoleService.js');
const { parseRoleId, CONSTANTS, createEmbedsFromData } = require('../../utils/helper.js');
const { logger } = require('../../logging/logging.js');
const { ClientContext } = require('../../ClientContext.js');

/**
 * Affiche les informations existantes sur le rôle donné.
 * @param {RoleService} roleService
 * @param {Role} role Le rôle dont il faut afficher les infos.
 * @param {TextChannel} channelToWrite Le channel dans lequel afficher les infos.
 */
async function displayRoleInfo(roleService, role, channelToWrite) {
    const roleInfo = await roleService.findOneRoleInfo(role.id);
    if (!roleInfo) {
        channelToWrite.send(`Aucune info disponible sur @${role.name}.`);
        return;
    }

    const proxy = roleInfo.createProxy();
    const embed = new MessageEmbed()
        .setTitle(`@${role.name}`)
        .setColor(CONSTANTS.COLOR_SETTING)
        .addField("Type de rôle", proxy.roleName || "Aucun");
    channelToWrite.send({ embeds: [embed] });
}

/**
 * Affiche tous les rôles enregistrés.
 * @param {RoleService} roleService
 * @param {Message} msg 
 */
async function displayAllRoleInfos(roleService, msg) {
    const infos = await roleService.findAllRoleInfos(msg.guildId);
    if (infos.length === 0) {
        msg.channel.send("Aucune info de rôle trouvée.");
        return;
    }
    
    const roleNames = getRoleNames();
    const embeds = createEmbedsFromData({ title: "Rôles", color: CONSTANTS.COLOR_SETTING }, roleNames, (embed, roleName) => {
        const roles = infos.filter(info => info.roleName === roleName)
            .map(info => msg.guild.roles.cache.get(info.roleId))
            .filter(role => !!role);

        embed.addField(roleName, roles.length > 0 ? roles.join(', ') : 'Aucun');
    });
    
    msg.channel.send({ embeds: embeds });
}

/**
 * Code de la commande.
 * @param {ClientContext} ctx L'instance du client.
 * @param {Message} msg Le message de la commande.
 * @param {string[]} args Les arguments de la commande.
 * @param {Object<string, string[]>} kwargs Les keyword arguments de la commande.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const roleService = ctx.getService(RoleService);

    if (args.length === 0) {
        displayAllRoleInfos(roleService, msg);
        return;
    }

    const role = msg.guild.roles.cache.get(parseRoleId(args[0]));
    if (!role) {
        msg.reply(`${args[0]} n'est pas un rôle valide.`);
        return;
    }

    const { set } = kwargs;

    if (set) {
        if (set.length < 1) {
            msg.reply(`Tu n'as pas précisé le type de rôle.`);
            return;
        }

        const roleName = set[0].toUpperCase();
        if (!isValidRoleName(roleName)) {
            msg.reply(`${set[0]} n'est pas un type de rôle valide.`);
            return;
        }
        roleService.setRoleName(role, roleName)
            .then(success => { 
                if (success) {
                    msg.reply(`Les informations du rôle ont bien été mises à jour.`);
                }
                else {
                    msg.reply(`Une erreur est survenue.`);
                }
            });
        return;
    }

    // Insérer d'autres options ici.

    displayRoleInfo(roleService, role, msg.channel);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "role", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "role [<@role>] [--set] [<roleName>]", // The usage of the command.
    description: "Gère les infos concernant un rôle.", // The command's description.
    examples: [
        {
            usage: "role",
            description: "Affiche tous les rôles spéciaux du serveur."
        },
        {
            usage: "role @role",
            description: "Affiche toutes les infos enregistrées sur le rôle @role."
        },
        {
            usage: "role @role --set roleName",
            description: "Associe le rôle @role avec le nom de rôle roleName." + 
                `\nLes noms de rôles disponibles sont : ${getRoleNames().join(', ')}.`
        }
    ],
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR,
        op: "all"
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
