const { Message, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext');
const { CommandService, COMMAND_ERRORS } = require('../CommandService');

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    // We don't use what has been parsed in args and kwargs because `aliasArgs` might contain kwargs.
    const rawArgs = msg.content.split(/\s/).filter(elem => !!elem).slice(1); // First element is the command name.
    if (rawArgs.length < 2) {
        msg.channel.send(`La commande attend au moins 2 arguments (nom de la commande et nom de l'alias).`);
        return;
    }

    const aliasName = rawArgs[0];
    const cmdName = rawArgs[1];
    const aliasArgs = rawArgs.slice(2).join(" ");

    const commandService = ctx.getService(CommandService);
    commandService.addCustomAlias(msg.guildId, cmdName, aliasName, aliasArgs).then(([success, errorMsg]) => {
        if (success) {
            msg.channel.send(`L'alias '${aliasName}' de la commande '${cmdName}' a bien été créé.`);
            return;
        }

        switch (errorMsg) {
            case COMMAND_ERRORS.NO_SUCH_COMMAND:
                msg.channel.send(`Aucune commande ne s'appelle '${cmdName}'.`);
                break;
            case COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND:
                msg.channel.send(`Impossible de créer un alias car '${cmdName}' est déjà un alias.`);
                break;
            case COMMAND_ERRORS.INVALID_OPERATION_ON_CUSTOM_COMMAND:
                msg.channel.send(`Impossible de créer un alias car '${cmdName}' est une commande custom.`);
                break;
            case COMMAND_ERRORS.ALREADY_EXISTING_ALIAS_COMMAND:
                msg.channel.send(`Impossible de créer un alias car '${aliasName}' existe déjà.`);
                break;
            case COMMAND_ERRORS.ALREADY_EXISTING_BUILTIN_COMMAND:
                msg.channel.send(`Impossible de créer un alias car '${aliasName}' est un nom de commande par défaut.`);
                break;
            case COMMAND_ERRORS.ALREADY_EXISTING_CUSTOM_COMMAND:
                msg.channel.send(`Impossible de créer un alias car '${aliasName}' est un nom de commande custom.`);
                break;
            case COMMAND_ERRORS.CREATE_FAILED: default:
                msg.channel.send("La création de l'alias a échoué.");
                break;
        }
    });
}

// We have to make a separate command from 'alias' because we it's to hard for kwargs parsing.
/**
 * @type {import('../CommandService').CommandHelp}
 */
module.exports.help = {
    name: "add-alias", // The command's name.
    aliases: ["addAlias"], // A list of aliases for the command.
    usage: "add-alias <aliasName> <commandName> [<aliasArgs...>]", // The usage of the command.
    description: "Créer un alias de commande.", // The command's description.
    examples: [
        {
            usage: "add-alias monAlias maCommande",
            description: "Crée un alias \`monAlias\` pour la commande \`maCommande\`."
        },
        {
            usage: "add-alias monAlias2 maCommande param1",
            description: "Crée un alias \`monAlias2\` pour la commande \`maCommande\` avec comme 1er argument \`param1\`" +
                "\nAppelez \`monAlias2\` est donc équivalent à appeler \`maCommande param1\`."
        },
        {
            usage: "add-alias monAlias3 maCommande param1 --option {0}",
            description: "Crée un alias \`monAlias3\` pour la commande \`maCommande\` avec des arguments substituables." +
                "\n Chaque {i} (où i >= 0) sera substitué au moment de taper la commande par le ie argument passé à l'alias." +
                "\nAppelez \`monAlias3 monOption\` est donc équivalent à appeler \`maCommande param1 --option monOption\`."
        }
    ], // A list of specific examples of usage of the command.
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR, // The Discord's flag that allow an user to run the command. The flag is a bitfield of values in Permission.FLAGS.
        op: "all" // The type of operation when checking the permissions. If undefined or null, no check is done.
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
