const { Message, MessageEmbed, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { SETTINGS, parseSettings, getCaseInsensitiveSettingName } = require('../../utils/guildSettings/settings.js');
const { SettingService } = require('../../utils/guildSettings/SettingsService.js');
const { repr, CONSTANTS, createEmbedsFromData } = require('../../utils/helper.js');

/**
 * Code de la commande.
 * @param {ClientContext} ctx L'instance du client.
 * @param {Message} msg Le message de la commande.
 * @param {string[]} args Les arguments de la commande.
 * @param {Object<string, string[]>} kwargs Les keyword arguments de la commande.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const { set, get } = kwargs;

    const option = set || get;

    const settingService = ctx.getService(SettingService);

    if (option) {
        if (option.length < 1) {
            msg.reply("Tu n'as pas précisé le nom du setting.");
            return;
        }

        const settingName = getCaseInsensitiveSettingName(option[0]);
        if (!settingName) {
            msg.reply(`${option[0]} n'est pas un nom d'option valide.`);
            return;
        }

        const setting = SETTINGS[settingName];
        if (option === set) {
            const values = option.slice(1);
            if (Number.isInteger(setting.argsCount) && values.length < setting.argsCount) {
                msg.reply(`L'option ${settingName} attend au moins ${setting.argsCount} argument(s) et ${values.length} ont été données.`);
                return;
            }

            const [parsed, val] = parseSettings(settingName, values);
            if (!parsed) {
                msg.reply(`La valeur passée en argument n'est pas reconnue.`);
                return;
            }

            settingService.setSetting(msg.guildId, settingName, val).then(success => {
                if (success) {
                    msg.reply(`L'option ${settingName} a été mise à jour à la valeur '${val}'`);
                }
                else {
                    msg.reply("Une erreur est survenue.");
                }
            });
        }
        else { // option === get
            settingService.getSetting(msg.guildId, settingName).then(settingValue => {
                if (settingValue !== undefined) {
                    const embed = new MessageEmbed()
                        .setTitle("Settings")
                        .setColor(CONSTANTS.COLOR_SETTING)
                        .addField(settingName, repr(settingValue))
                        .setDescription(setting.description || 'Aucune description.');
                    msg.channel.send({ embeds: [embed] });
                }
                else {
                    msg.reply(`Aucune valeur n'a été trouvée pour ${settingName}.`);
                }
            });
        }

        return;
    }

    settingService.getSettingMap(msg.guildId).then(map => {
        const entries = [...map.entries()];
        const embeds = createEmbedsFromData({ title: "Settings", color: CONSTANTS.COLOR_SETTING }, entries, (embed, [key, val]) => {
            embed.addField(key, repr(val));
        });
        msg.channel.send({ embeds: embeds });
    });
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "settings", // The command's name.
    aliases: ["config", "conf", "setting"], // A list of aliases for the command.
    usage: "settings [--set] [--get] [<settingName>] [<settingValues...>]", // The usage of the command.
    description: "Gère les settings du serveur.", // The command's description.
    examples: [
        {
            usage: "settings",
            description: "Affiche la valeur de tous les settings sur le serveur."
        },
        {
            usage: "settings --set prefix !",
            description: "Met la valeur du setting `prefix` à `!`."
        },
        {
            usage: "settings --get prefix",
            description: "Affiche la valeur du setting `prefix`."
        }
    ],
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR,
        op: "all"
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
