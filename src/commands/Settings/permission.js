const { Message, Permissions, MessageEmbed, TextChannel } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { logger } = require('../../logging/logging.js');
const { parseUserId, parseRoleId, createEmbedsFromData, CONSTANTS } = require('../../utils/helper.js');
const { CommandService, COMMAND_ERRORS } = require('../CommandService.js');

/**
 * Display all permissions defined in the guild.
 * @param {CommandService} commandService
 * @param {TextChannel} channelToWrite 
 */
async function displayAllPermissions(commandService, channelToWrite) {
    const permissions = (await commandService.getAllPermissions(channelToWrite.guildId))
        .filter(perm => (perm.MemberInfos && perm.MemberInfos.length > 0) || (perm.RoleInfos && perm.RoleInfos.length > 0));
    
    if (!permissions || permissions.length === 0) {
        channelToWrite.send("Aucune permission n'a été définie.");
        return;
    }

    const embeds = createEmbedsFromData({ title: `Permissions de commandes.`, color: CONSTANTS.COLOR_SETTING }, permissions, (embed, perm) => {
        let ids = [];
        perm.MemberInfos.forEach(mInfo => ids.push(`<@${mInfo.userId}>`));
        perm.RoleInfos.forEach(rInfo => ids.push(`<@&${rInfo.roleId}>`));
        embed.addField(`${perm.commandName}`, ids.length > 0 ? ids.join(', ') : "Aucune.");
    });
    
    channelToWrite.send({ embeds: embeds });
}

/**
 * Display all permissions defined for a specific command.
 * @param {CommandService} commandService
 * @param {string} commandName 
 * @param {TextChannel} channelToWrite 
 */
async function displayOnePermission(commandService, commandName, channelToWrite) {
    const command = commandService.getCommand(channelToWrite.guildId, commandName);
    if (!command) {
        channelToWrite.send(`Aucune commande ne s'appelle '${commandName}'.`);
        return;
    }

    const permission = await commandService.getPermissionWithCommandName(channelToWrite.guildId, commandName);

    if (!permission) {
        channelToWrite.send(`Aucune permission n'a été définie pour la commande '${commandName}'.`);
        return;
    }

    const embed = new MessageEmbed()
        .setTitle(`Permissions de ${commandName}`)
        .setColor(CONSTANTS.COLOR_SETTING);
    
    const users = permission.MemberInfos && permission.MemberInfos.length > 0 ?
        permission.MemberInfos.map(mInfo => `<@${mInfo.userId}>`).join(', ') : "Aucun";
    const roles = permission.RoleInfos && permission.RoleInfos.length > 0 ?
        permission.RoleInfos.map(rInfo => `<@&${rInfo.roleId}>`).join(', ') : "Aucun";

    embed.addField("Users autorisés", users)
        .addField("Rôles autorisés", roles);
    
    channelToWrite.send({ embeds: [embed] });
}

/**
 * Code de la commande.
 * @param {ClientContext} ctx L'instance du client.
 * @param {Message} msg Le message de la commande.
 * @param {string[]} args Les arguments de la commande.
 * @param {Object<string, string[]>} kwargs Les keyword arguments de la commande.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const commandService = ctx.getService(CommandService);

    if (args.length === 0) {
        displayAllPermissions(commandService, msg.channel);
        return;
    }

    const commandName = args[0];

    const add = kwargs.add;
    const rm = kwargs.remove || kwargs.rm;
    const option = add || rm;

    if (!option) {
        displayOnePermission(commandService, commandName, msg.channel);
        return;
    }

    const infos = [];

    for (let arg of option) {
        let userId = parseUserId(arg);
        if (userId) {
            if (!msg.guild.members.cache.has(userId)) {
                msg.channel.send(`${arg} n'est pas un nom de user valide.`);
                return;
            }
            infos.push({ id: userId, type: "user" });
        }
        else {
            let roleId = parseRoleId(arg);
            if (!roleId) {
                msg.channel.send(`${arg} n'est ni un user ni un rôle valide.`);
                return;
            }
            if (!msg.guild.roles.cache.has(roleId)) {
                msg.channel.send(`${arg} n'est pas un nom de rôle valide.`);
                return;
            }
            infos.push({ id: roleId, type: "role" });
        }
    }

    if (option === add) {
        commandService.addCommandPermission(msg.guildId, commandName, ...infos).then(([success, errorMsg]) => {
            if (success) {
                msg.channel.send(`Permission(s) ajoutée(s) pour la commande '${commandName}'.`);
                return;
            }

            switch (errorMsg) {
                case COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND:
                    msg.channel.send(`Impossible d'ajouter des permissions pour '${commandName}' car c'est un alias et pas une commande par défaut.`);
                    break;
                case COMMAND_ERRORS.INVALID_OPERATION_ON_CUSTOM_COMMAND:
                    msg.channel.send(`Impossible d'ajouter des permissions pour '${commandName}' car c'est une commande custom et pas une commande par défaut.`);
                    break;
                case COMMAND_ERRORS.NO_SUCH_COMMAND:
                    msg.channel.send(`Aucune commande ne s'appelle '${commandName}'.`);
                    break;
                case COMMAND_ERRORS.ADD_FAILED: default:
                    msg.channel.send("L'ajout de permission(s) a échoué.");
                    break;
            }
        });
    }
    else if (option === rm) {
        commandService.removeCommandPermission(msg.guildId, commandName, ...infos).then(([success, errorMsg]) => {
            if (success) {
                msg.channel.send(`Permission(s) supprimée(s) pour la commande '${commandName}'.`);
                return;
            }

            switch (errorMsg) {
                case COMMAND_ERRORS.INVALID_OPERATION_ON_ALIAS_COMMAND:
                    msg.channel.send(`Impossible de supprimer des permissions pour '${commandName}' car c'est un alias et pas une commande par défaut.`);
                    break;
                case COMMAND_ERRORS.INVALID_OPERATION_ON_CUSTOM_COMMAND:
                    msg.channel.send(`Impossible de supprimer des permissions pour '${commandName}' car c'est une commande custom et pas une commande par défaut.`);
                    break;
                case COMMAND_ERRORS.NO_SUCH_COMMAND:
                    msg.channel.send(`Aucune commande ne s'appelle '${commandName}'.`);
                    break;
                case COMMAND_ERRORS.DELETE_FAILED: default:
                    msg.channel.send("La suppression de permission(s) a échoué.");
                    break;
            }
        });
    }
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "permission", // The command's name.
    aliases: ["perm"], // A list of aliases for the command.
    usage: "permission [<commandName>] [--add] [--remove | --rm] [<@user> | <@role>...]", // The usage of the command.
    description: "Gère les permissions de commandes.", // The command's description.
    examples: [
        {
            usage: "permission",
            description: "Affiche la liste de toutes les permissions de commandes définies sur le serveur."
        },
        {
            usage: "permission maCommande",
            description: "Affiche les rôles et les users qui ont une permission spéciale pour `maCommande`."
        },
        {
            usage: "permission maCommande --add @monUser",
            description: "Accorde une permission spéciale à @monUser pour utiliser la commande `maCommande`."
        },
        {
            usage: "permission maCommande --add @monRole",
            description: "Accorde une permission spéciale aux détenteurs du rôle @monRole pour utiliser la commande `maCommande`."
        },
        {
            usage: "permission maCommande --add @monUser @monUser2 @monRole @monRole2 ...",
            description: "Accorde une permission spéciale à tous les rôles et utilisateurs donnés pour utiliser la commande `maCommande`."
        },
        {
            usage: "permission maCommande --remove @monUser",
            description: "Retire une permission spéciale à @monUser pour utiliser la commande `maCommande`."
        },
        {
            usage: "permission maCommande --remove @monRole",
            description: "Retire une permission spéciale aux détenteurs du rôle @monRole pour utiliser la commande `maCommande`."
        },
        {
            usage: "permission maCommande --remove @monUser @monUser2 @monRole @monRole2 ...",
            description: "Retire une permission spéciale à tous les rôles et utilisateurs donnés pour utiliser la commande `maCommande`."
        }
    ],
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR,
        op: "all"
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false // Whether the command should only be available in testing mode.
}
