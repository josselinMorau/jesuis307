const { Message, Channel, MessageEmbed, TextChannel, Permissions } = require('discord.js');
const { ChannelService } = require('../../utils/channels/ChannelService.js');
const { isValidCategory, getCategories } = require('../../utils/channels/channelUtils.js');
const { parseChannelId, CONSTANTS, createEmbedsFromData } = require('../../utils/helper.js');
const { logger } = require('../../logging/logging.js');
const { ClientContext } = require('../../ClientContext.js');

/**
 * Ajoute ou supprime une catégorie à un channel.
 * @param {ChannelService} channelService
 * @param {Channel} channel Le channel à modifier.
 * @param {string} category La catégorie à ajouter/supprimer.
 * @param {boolean} add Ajoute si `true`.
 */
async function addOrRemoveCategory(channelService, channel, category, add) {
    var isSuccess = false;
    if (add) {
        isSuccess = await channelService.addCategories(channel, category);
    }
    else {
        isSuccess = await channelService.removeCategories(channel, category);
    }
    if (!isSuccess) {
        throw `Error when adding/removing category '${category}' to channel #${channel.name}`;
    }
}

/**
 * Affiche les informations existantes sur le channel donné.
 * @param {ChannelService} channelService
 * @param {Channel} channel Le channel dont il faut afficher les infos.
 * @param {TextChannel} channelToWrite Le channel dans lequel afficher les infos.
 */
async function displayChannelInfo(channelService, channel, channelToWrite) {
    const channelInfo = await channelService.findOneChannelInfo(channel.id);
    if (!channelInfo) {
        channelToWrite.send(`Aucune info disponible sur ${channel}.`);
        return;
    }

    const categories = channelInfo.getCategories();
    const embed = new MessageEmbed()
        .setTitle(channel.name)
        .setColor(CONSTANTS.COLOR_SETTING)
        .addField("Catégories", categories.length > 0 ? categories.join(', ') : "Aucune catégorie.")
        .setDescription(channelInfo.description || "Aucune description.");
    channelToWrite.send({ embeds: [embed] });
}

/**
 * Affiche les infos de tous les channels du serveur.
 * @param {ChannelService} channelService
 * @param {TextChannel} channelToWrite Le channel dans lequel afficher les infos.
 */
async function displayAllChannelInfo(channelService, channelToWrite) {
    const infos = await channelService.findAllChannelInfos(channelToWrite.guildId);
    if (infos.length === 0) {
        channelToWrite.send("Aucune info de channel trouvée.");
        return;
    }
    
    const embeds = createEmbedsFromData({ title: "Channels", color: CONSTANTS.COLOR_SETTING }, infos, (embed, info) => {
        const channel = channelToWrite.guild.channels.cache.get(info.channelId);
        if (!channel)
            return;
        const categories = info.getCategories();
        embed.addField(`#${channel.name}`, categories.length > 0 ? categories.join(', ') : "Aucune catégorie");
    });
    
    channelToWrite.send({ embeds: embeds });
}

/**
 * Code de la commande.
 * @param {ClientContext} ctx L'instance du client.
 * @param {Message} msg Le message de la commande.
 * @param {string[]} args Les arguments de la commande.
 * @param {Object<string, string[]>} kwargs Les keyword arguments de la commande.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const channelService = ctx.getService(ChannelService);

    if (args.length === 0) {
        displayAllChannelInfo(channelService, msg.channel);
        return;
    }

    const channel = msg.guild.channels.cache.get(parseChannelId(args[0]));
    if (!channel || channel.type !== "GUILD_TEXT") {
        msg.reply(`${args[0]} n'est pas un channel valide.`);
        return;
    }

    const { add } = kwargs;
    const remove = kwargs.remove || kwargs.rm;
    const option = add || remove;

    if (option) {
        if (option.length < 1) {
            msg.reply(`Tu n'as pas précisé la catégorie.`);
            return;
        }

        const category = option[0].toUpperCase();
        if (!isValidCategory(category)) {
            msg.reply(`${option[0]} n'est pas une catégorie valide.`);
            return;
        }
        addOrRemoveCategory(channelService, channel, category, option === add)
            .then(() => { msg.reply("Les informations du channel ont bien été mises à jour."); })
            .catch((_err) => { logger.error(_err); msg.reply("Une erreur est survenue."); });
        return;
    }

    const optDesc = kwargs.description || kwargs.d;

    if (optDesc) {
        if (optDesc.length === 0) {
            msg.reply("Tu n'as pas précisé de description.");
            return;
        }

        const description = optDesc.join(" ");
        channelService.setDescription(channel, description).then(success => {
            if (success) {
                msg.reply(`Description de ${channel} mise à jour.`);
            }
            else {
                msg.reply("Une erreur est survenue.");
            }
        });
        return;
    }

    // Insérer d'autres options ici.

    displayChannelInfo(channelService, channel, msg.channel);
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "channel", // The command's name.
    aliases: [], // A list of aliases for the command.
    usage: "channel <#channel> [--add] [--remove | --rm] [<category>] [--description | --d] [<description>]", // The usage of the command.
    description: "Gère les infos concernant un channel.", // The command's description.
    examples: [
        {
            usage: "channel",
            description: "Affiche toutes les infos enregistrées sur les channels du serveur."
        },
        {
            usage: "channel #monChannel",
            description: "Affiche les infos enregistrées sur #monChannel."
        },
        {
            usage: "channel #monChannel --add maCategorie",
            description: `Ajoute la catégorie \`maCategorie\` à #monChannel. Les catégories disponibles sont ${getCategories().join(', ')}.`
        },
        {
            usage: "channel #monChannel --remove maCategorie",
            description: "Retire la catégorie `maCategorie` de #monChannel."
        },
        {
            usage: "channel #monChannel --description Ceci est la description de mon channel.",
            description: "Définis la description de #monChannel à `Ceci est la description de mon channel.`"
        }
    ],
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR,
        op: "all"
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}