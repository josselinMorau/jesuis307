const { Message, MessageEmbed, Guild, TextChannel, Permissions } = require('discord.js');
const { ClientContext } = require('../../ClientContext.js');
const { RoutineService, ROUTINE_ERRORS } = require('../../routine/RoutineService.js');
const { CONSTANTS, parseBoolean, parseChannelId, createEmbedsFromData } = require('../../utils/helper.js');

/**
 * Display infos on all routines in a guild.
 * @param {RoutineService} routineService
 * @param {Guild} guild The id of the guild.
 * @param {TextChannel} channel The channel to write infos in.
 */
async function displayAllRoutines(routineService, guild, channel) {
    const routineInfos = await routineService.getAllRoutineInfos(guild.id);
    if (!routineInfos || routineInfos.length === 0) {
        channel.send("Aucune routine n'est enregistrée sur ce serveur.");
        return;
    }

    const embeds = createEmbedsFromData({ title: "Routines" , color: CONSTANTS.COLOR_SETTING }, routineInfos, (embed, info) => {
        embed.addField(info.name, info.enabled ? 'Activée' : 'Désactivée', true);
    });
    
    channel.send({ embeds: embeds });
}

/**
 * Display infos on one routine.
 * @param {RoutineService} routineService
 * @param {Guild} guild The id of the guild.
 * @param {string} name The name of the routine.
 * @param {TextChannel} channel The channel to write infos in.
 */
async function displayOneRoutine(routineService, guild, name, channel) {
    const routineInfo = await routineService.getOneRoutineInfo(guild.id, name);
    if (!routineInfo) {
        channel.send(`Aucune routine ne s'appelle '${name}'.`);
        return;
    }

    const embed = new MessageEmbed()
        .setTitle(routineInfo.name)
        .setColor(CONSTANTS.COLOR_SETTING)
        .addField("Activée", routineInfo.enabled ? 'Oui' : 'Non', true)
        .addField("Règle d'occurence", routineInfo.rule, true);
    
    if (routineInfo.messageChannelId)
        embed.addField("channel", `<#${routineInfo.messageChannelId}>`, true);

    const date = routineService.getNextInvocationDate(guild.id, name);
    if (date)
        embed.addField("Prochain événement", date.toLocaleString());
    
    channel.send({ embeds: [embed] });
}

/**
 * The command's code.
 * @param {ClientContext} ctx The application's client;
 * @param {Message} msg The message the command originated from.
 * @param {string[]} args The command's arguments.
 * @param {Object<string, string[]>} kwargs The command's keyword arguments.
 */
module.exports.run = (ctx, msg, args, kwargs) => {
    const routineService = ctx.getService(RoutineService);

    if (args.length === 0) {
        displayAllRoutines(routineService, msg.guild, msg.channel);
        return;
    }

    const routineName = args[0];

    const _new = kwargs.new;
    const edit = kwargs.edit;
    const rm = kwargs.remove || kwargs.rm;

    const mainOption = _new || edit || rm;

    if (!mainOption) {
        displayOneRoutine(routineService, msg.guild, routineName, msg.channel);
        return;
    }

    if (mainOption === rm) {
        routineService.removeCustomRoutine(msg.guildId, routineName).then(([success, errorMsg]) => {
            if (success) {
                msg.channel.send(`La routine '${routineName}' a bien été supprimée.`);
                return;
            }
            switch (errorMsg) {
                case ROUTINE_ERRORS.CANT_DELETE_BUILTIN_ROUTINE:
                    msg.channel.send(`Impossible de supprimer '${routineName}' car il s'agit d'une routine par défaut.`);
                    break;
                case ROUTINE_ERRORS.NOT_FOUND:
                    msg.channel.send(`Aucune routine ne s'appelle '${routineName}'.`);
                    break;
                case ROUTINE_ERRORS.DELETE_FAILED: default:
                    msg.channel.send("La suppression de la routine a échoué.");
                    break;
            }
        });
        return;
    }

    const { enabled, rule, channel } = kwargs;
    const message = kwargs.message || kwargs.msg;
    const instanceOptions = {
        enabled: enabled ? parseBoolean(enabled[0]) : undefined,
        rule: rule ? rule.join(' ') : undefined,
        message: message ? message.join(' ') : undefined,
        messageChannelId: channel ? parseChannelId(channel[0]) : undefined
    };

    if (instanceOptions.channel) {
        let chan = msg.guild.channels.cache.get(instanceOptions.channel);
        if (!chan || chan.type !== "GUILD_TEXT") {
            msg.channel.send(`${channel[0]} n'est pas un channel valide.`);
            return;
        }
    }

    if (mainOption === _new) {
        routineService.createCustomRoutine(msg.guildId, routineName, instanceOptions).then(([success, errorMsg]) => {
            if (success) {
                const nextInvocation = routineService.getNextInvocationDate(msg.guildId, routineName);
                const content = !!nextInvocation ? `La routine se déclenchera la prochaine fois le ${nextInvocation.toLocaleString()}.` : "La routine est désactivée pour l'instant.";
                msg.channel.send(`La routine '${routineName}' a bien été créée.\n${content}`);
                return;
            }
            switch (errorMsg) {
                case ROUTINE_ERRORS.MISSING_PARAMETERS:
                    msg.channel.send(`La création de '${routineName}' a échoué car il manque des paramètres nécessaires.\nLes paramètres 'rule', 'message' et 'channel' sont requis.`);
                    break;
                case ROUTINE_ERRORS.WRONG_PARAMETERS:
                    msg.channel.send(`La création de '${routineName}' a échoué car les paramètres n'ont pas le bon format.\nVérifie que 'rule' respecte le format cron.`);
                    break;
                case ROUTINE_ERRORS.ALREADY_EXISTING_ROUTINE_NAME:
                    msg.channel.send(`Tu ne peux pas créer '${routineName}' car une routine existante porte déjà ce nom.`);
                    break;
                case ROUTINE_ERRORS.CREATION_FAILED: default:
                    msg.channel.send("La création de la routine a échoué.");
                    break;
            }
        });
    }
    else { // mainOption === edit
        routineService.editRoutine(msg.guildId, routineName, instanceOptions).then(([success, errorMsg]) => {
            if (success) {
                const nextInvocation = routineService.getNextInvocationDate(msg.guildId, routineName);
                const content = !!nextInvocation ? `La routine se déclenchera la prochaine fois le ${nextInvocation.toLocaleString()}.` : "La routine est désactivée pour l'instant.";
                msg.channel.send(`La mise à jour de '${routineName}' a réussi.\n${content}`);
                return;
            }
            switch (errorMsg) {
                case ROUTINE_ERRORS.MISSING_PARAMETERS:
                    msg.channel.send(`La mise à jour de '${routineName}' a échoué car aucun paramètre n'a été passé.`);
                    break;
                case ROUTINE_ERRORS.WRONG_PARAMETERS:
                    msg.channel.send(`La mise à jour de '${routineName}' a échoué car les paramètres n'ont pas le bon format.\Vérifie que 'rule' respecte le format cron.`);
                    break;
                case ROUTINE_ERRORS.NOT_FOUND:
                    msg.channel.send(`Aucune routine ne s'appelle '${routineName}'.`);
                    break;
                case ROUTINE_ERRORS.UPDATE_FAILED: default:
                    msg.channel.send("La mise à jour de la routine a échouée.");
                    break;
            }
        });
    }
}

/**
 * @type {import('../CommandService.js').CommandHelp}
 */
module.exports.help = {
    name: "schedule", // The command's name.
    aliases: ["routine"], // A list of aliases for the command.
    usage: "schedule [<routineName>] [--new] [--remove | --rm] [--edit] [--enabled] [<enabled>] [--rule] [<cronRule>] [--message | --msg] [<message...>] [--channel] [<#channel>]", // The usage of the command.
    description: "Gère les routines du serveur.", // The command's description.
    examples: [
        {
            usage: "schedule",
            description: "Affiche la liste de toutes les routines disponibles sur le serveur."
        },
        {
            usage: "schedule maRoutine",
            description: "Affiche les informations concernant la routine `maRoutine`."
        },
        {
            usage: "schedule maRoutine --new --enabled true --rule 0 15 * * 1 --channel #monChannel --message Bonjour les amis.",
            description: "Crée une routine `maRoutine` qui affichera le message `Bonjour les amis.` tous les lundis à 15h dans le channel #monChannel." + 
                "Le paramètre `--enabled` est optionnel (par défaut à false). Le paramètre `--rule` doit être [une chaîne cron](https://fr.wikipedia.org/wiki/Cron#Syntaxe_de_la_table)."
        },
        {
            usage: "schedule maRoutine --edit --enabled false",
            description: "Désactive la routine `maRoutine`. Les paramètres `--rule`, `--channel` et `--message` sont aussi éditables via l'option `--edit`."
        },
        {
            usage: "schedule maRoutine --remove",
            description: "Supprime la routine `maRoutine`."
        }
    ],
    permissions: {
        flag: Permissions.FLAGS.ADMINISTRATOR,
        op: "all"
    },
    delete: false, // Whether the message of the command should be deleted.
    onTest: false, // Whether the command should only be available in testing mode.
}
