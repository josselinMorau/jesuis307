const { ChannelInfo } = require('../../database/models/ChannelInfo.js');
const { logger } = require("../../logging/logging.js");
const EventEmitter = require('events');
const { Op } = require('sequelize');
const { TextChannel, Guild, ThreadChannel } = require('discord.js');
const { BaseService } = require('../../BaseService.js');
const { CHANNEL_CATEGORIES } = require('./channelUtils.js');

/**
 * @class Service class handling the metadata for each channel of each managed guild.
 */
class ChannelService extends BaseService
{
    /**
     * 
     * @param {*} ctx The ClientContext. 
     */
    constructor(ctx) {
        super(ctx);

        ChannelInfo.on(this.clientContext, 'beforeSave', instance => { this.__onBeforeSave(instance); });
        ChannelInfo.on(this.clientContext, 'afterSave', instance => { this.__onAfterSave(instance); });
        ChannelInfo.on(this.clientContext, 'afterDestroy', instance => { this.__onAfterDestroy(instance); });
        this.__emitter = new EventEmitter();
        /**
         * A map to temporary save ChannelInfos.
         * @type {Map<string, ChannelInfo>}
         */
        this.__tempMap = new Map();

        // Register Discord's callback.
        this.clientContext.client.on('channelDelete', channel => {
            this.deleteOneChannelInfo(channel.id);
        });
    }

    /**
     * Callback for the `beforeSave` hook.
     * @param {ChannelInfo} instance 
     */
    __onBeforeSave(instance) {
        this.__tempMap.set(instance.channelId, instance);
    }

    /**
     * Callback for the `afterSave` hook.
     * @param {ChannelInfo} instance 
     */
    __onAfterSave(instance) {
        const oldInfo = this.__tempMap.get(instance.channelId);
        this.__tempMap.delete(instance.channelId);
        // Invoque l'event.
        this.__emitter.emit('channelInfoUpdate', oldInfo, instance);
    }

    /**
     * Callback for the `afterDestroy` hook.
     * @param {ChannelInfo} instance 
     */
    __onAfterDestroy(instance) {
        this.__emitter.emit('channelInfoDestroy', instance);
    }

    /**
     * Register a callback invoked when a `ChannelInfo` is updated.
     * @param {(oldInfo : ChannelInfo, newInfo : ChannelInfo) => void} callback The callback to register.
     */
    onChannelInfoUpdate(callback) {
        this.__emitter.on('channelInfoUpdate', callback);
    }

    /**
     * Register a callback invoked when a `ChannelInfo` is destroyed.
     * @param {(info : ChannelInfo) => void} callback The callback to register.
     */
    onChannelInfoDestroy(callback) {
        this.__emitter.on('channelInfoDestroy', callback);
    }

    /**
     * Get the channel with the given category from the given guild.
     * @param {Guild} guild The guild to search the channel in.
     * @param {CHANNEL_CATEGORIES} category The category to check.
     * @returns {Promise<TextChannel?>} Returns `null` if no channel has been found.
     */
    async getChannelWithCategory(guild, category) {
        const channelInfo = await this.findOneChannelInfoWithCategory(guild.id, category);
        if (!channelInfo)
            return null;
        return (await guild.channels.fetch(channelInfo.channelId)) || null;
    }

    /**
     * Get the channel with the given categories from the given guild.
     * @param {Guild} guild The guild to search the channel in.
     * @param {CHANNEL_CATEGORIES[]} categories The categories to check.
     * @returns {Promise<TextChannel?>} Returns `null` if no channel has been found.
     */
    async getChannelWithCategories(guild, categories) {
        const channelInfo = await this.findOneChannelInfoWithCategories(guild.id, categories);
        if (!channelInfo)
            return null;
        return (await guild.channels.fetch(channelInfo.channelId)) || null;
    }

    /**
     * Get all the channels with the given category from the given guild.
     * @param {Guild} guild The guild to search the channels in.
     * @param {CHANNEL_CATEGORIES} category The category to check.
     * @returns {Promise<TextChannel[]>}
     */
    async getAllChannelsWithCategory(guild, category) {
        const channelInfos = await this.findAllChannelInfosWithCategory(guild.id, category);
        return channelInfos.map(info => guild.channels.cache.get(info.channelId))
            .filter(channel => !!channel);
    }

    /**
     * Get all the channels with the given categories from the given guild.
     * @param {Guild} guild The guild to search the channels in.
     * @param {CHANNEL_CATEGORIES[]} categories The categories to check.
     * @returns {Promise<TextChannel[]>}
     */
    async getAllChannelsWithCategories(guild, categories) {
        const channelInfos = await this.findAllChannelInfosWithCategories(guild.id, categories);
        return channelInfos.map(info => guild.channels.cache.get(info.channelId))
            .filter(channel => !!channel);
    }

    /**
     * Returns whether the given channel has the given category.
     * If the channel is a thread channel, checks the category of the parent channel.
     * @param {TextChannel | ThreadChannel} channel 
     * @param {CHANNEL_CATEGORIES} category 
     * @returns {Promise<boolean>}
     */
    async hasCategory(channel, category) {
        const channelToCheck = channel.isThread() ? channel.parent : channel;
        const channelInfo = await this.findOneChannelInfo(channelToCheck.id).catch(err => {
            logger.error(err);
            return null;
        });
        return !!channelInfo && channelInfo.hasCategory(category);
    }

    /**
     * Returns a list of all `ChannelInfos` with a description.
     * @returns {Promise<ChannelInfo[]>} 
     */
    async findAllChannelInfosWithDescription() {
        return await ChannelInfo.findAll({where: {
            [Op.and]: [
                { description: { [Op.not]: null } },
                { description: { [Op.not]: "" } }
            ]
        }})
        .catch(_err => { logger.error(_err); return []; });
    }

    /**
     * Returns the `ChannelInfo` with the given id.
     * @param {string} channelId The id of the channel.
     * @returns {Promise<ChannelInfo?>} Returns `null` if not found.
     */
    async findOneChannelInfo(channelId) {
        const found = await ChannelInfo.findByPk(channelId).catch(_err => { logger.error(_err); });
        return found || null;
    }

    /**
     * Returns a list of all the `ChannelInfos` with the given guild id.
     * @param {string} guildId The id of the guild.
     * @returns {Promise<ChannelInfo[]>}
     */
    async findAllChannelInfos(guildId) {
        return await ChannelInfo.findAll({ where: { guildId: guildId } }).catch(_err => { logger.error(_err); return []; });
    }

    /**
     * Delete the `ChannelInfo` with the given id.
     * @param {string} channelId The id of the channel. 
     */
    async deleteOneChannelInfo(channelId) {
        await ChannelInfo.destroy({where: { channelId: channelId }}).catch(_err => { logger.error(_err); });
    }

    /**
     * Returns the `ChannelInfo` with the given category.
     * @param {string} guildId The id of the guild.
     * @param {CHANNEL_CATEGORIES} category The category to search.
     * @returns {Promise<ChannelInfo?>} Returns `null` if not found.
     */
    async findOneChannelInfoWithCategory(guildId, category) {
        const found = await ChannelInfo.findOne({where: { guildId: guildId, categories: { [Op.substring]: category } }})
            .catch(_err => { logger.error(_err); });
        return found || null;
    }

    /**
     * Returns the `ChannelInfo` with all the given categories.
     * @param {string} guildId The id of the guild.
     * @param {CHANNEL_CATEGORIES[]} categories The categories to search.
     * @returns {Promise<ChannelInfo?>} Returns `null` if not found.
     */
    async findOneChannelInfoWithCategories(guildId, categories) {
        const andConditions = [
            { guildId: guildId }
        ];
        categories.forEach(category => {
            andConditions.push({ categories: { [Op.substring]: category } });
        });
        const found = await ChannelInfo.findOne({ where: { [Op.and]: andConditions } })
            .catch(_err => { logger.error(_err); });
        return found || null;
    }

    /**
     * Returns a list of all the `ChannelInfos` with the given category.
     * @param {string} guildId The id of the guild.
     * @param {CHANNEL_CATEGORIES} category The category to search.
     * @returns {Promise<ChannelInfo[]>}
     */
    async findAllChannelInfosWithCategory(guildId, category) {
        return await ChannelInfo.findAll({where: { guildId: guildId, categories: { [Op.substring]: category } }})
            .catch(_err => { logger.error(_err); return []; });
    }

    /**
     * Returns a list of all the `ChannelInfos` with the given category no matter the guild.
     * @param {CHANNEL_CATEGORIES} category The category to search.
     * @param {boolean} logIfFail Whether a log should be made if an error occurs.
     * @returns {Promise<ChannelInfo[]>}
     */
    async findAllChannelInfosWithCategoryWithoutGuild(category, logIfFail = true) {
        return await ChannelInfo.findAll({where: { categories: { [Op.substring]: category } }})
            .catch(_err => { 
                if (logIfFail)
                    logger.error(_err);
                return []; 
            });
    }

    /**
     * Returns a list of all the `ChannelInfos` with the given categories.
     * @param {string} guildId The id of the guild.
     * @param {CHANNEL_CATEGORIES[]} categories The categories to search.
     * @returns {Promise<ChannelInfo[]>}
     */
    async findAllChannelInfosWithCategories(guildId, categories) {
        const andConditions = [
            { guildId: guildId }
        ];
        categories.forEach(category => {
            andConditions.push({ categories: { [Op.substring]: category } });
        });
        return await ChannelInfo.findAll({ where: { [Op.and]: andConditions } })
            .catch(_err => { logger.error(_err); return []; });
    }

    /**
     * Update the `ChannelInfo` corresponding to the given channel. If none exists it will be created.
     * @param {TextChannel} channel The channel whose infos to update.
     * @param {(info : ChannelInfo) => void} updateFn The update fonction.
     * @returns {Promise<boolean>} Returns `true` if the upfate is successfull.
     */
    async upsertChannelInfo(channel, updateFn) {
        const [channelInfo, _created] = await ChannelInfo.findOrCreate({ where: { channelId: channel.id, guildId: channel.guildId } })
            .catch(_err => { logger.error(_err); return [null, false]; });
        
        if (!channelInfo)
            return false;
        
        updateFn(channelInfo);
        const success = await channelInfo.save().catch(_err => logger.error(_err));
        return !!success;
    }

    /**
     * Add categories to the corresponding `ChannelInfo`.
     * @param {TextChannel} channel The channel to update.
     * @param  {...CHANNEL_CATEGORIES} categories The categories to add.
     * @returns {Promise<boolean>} Returns `true` if the update is successfull.
     */
    async addCategories(channel, ...categories) {
        if (categories.length === 0)
            return false;
        
        return await this.upsertChannelInfo(channel, (info) => {
            categories.forEach(category => info.addCategory(category));
        })
        .catch(_err => {
            logger.error(_err);
            return false;
        });
    }

    /**
     * Remove categories from the corresponding `ChannelInfo`.
     * @param {TextChannel} channel The channel to update.
     * @param  {...CHANNEL_CATEGORIES} categories The categories to remove.
     * @returns {Promise<boolean>} Returns `true` if the update is successfull.
     */
    async removeCategories(channel, ...categories) {
        if (categories.length === 0)
            return false;
        
        return await this.upsertChannelInfo(channel, (info) => {
            categories.forEach(category => info.removeCategory(category));
        })
        .catch(_err => {
            logger.error(_err);
            return false;
        });
    }

    /**
     * Set the description of a channel.
     * @param {TextChannel} channel The channel to update.
     * @param {string} description The description of the channel.
     * @returns {Promise<boolean>} Returns `true` if the update is successfull.
     */
    async setDescription(channel, description) {
        if (!description)
            return false;
        
        return await this.upsertChannelInfo(channel, (info) => {
            info.description = description;
        })
        .catch(_err => {
            logger.error(_err);
            return false;
        });
    }
}

module.exports = {
    ChannelService,
    CHANNEL_CATEGORIES
}