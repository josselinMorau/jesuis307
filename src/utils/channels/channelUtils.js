/**
 * Ensemble de structures de données et de fonctions concernant les channels.
 */

/**
 * @enum {string} Enum of all the channel categories.
 */
const CHANNEL_CATEGORIES = {
    NEWCOMER: "NEWCOMER",
    SPAM: "SPAM",
    JAIL: "JAIL",
    RULE: "RULE",
    LOG: "LOG",
    BIRTHDAY: "BIRTHDAY",
    SHITPOST: "SHITPOST"
};

/**
 * Returns a list of all the categories.
 */
function getCategories() {
    return Object.values(CHANNEL_CATEGORIES);
}

/**
 * Retourne `true` si le nom de catégorie donné est valide.
 * @param {string} category  
 */
function isValidCategory(category) {
    return getCategories().includes(category);
}

module.exports = {
    CHANNEL_CATEGORIES,
    isValidCategory,
    getCategories
}