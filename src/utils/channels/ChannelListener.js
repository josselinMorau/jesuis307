const { Message } = require('discord.js');
const EventEmitter = require('events');
const { BaseService } = require('../../BaseService.js');
const { logger } = require('../../logging/logging.js');

/**
 * @class A service where you can register callbacks for when messages are sent in specific channels.
 */
class ChannelListener extends BaseService {

    /**
     * @typedef {(msg : Message) => void} ChannelCallback
     */

    /**
     * 
     * @param {*} ctx The parent ClientContext. 
     */
    constructor(ctx) {
        super(ctx);
        /**
         * @type {Map<string, EventEmitter>}
         */
        this.singleChannelCallbacks = new Map();
        /**
         * @type {Map<string, EventEmitter>}
         */
        this.multipleChannelCallbacks = new Map();
    }

    /**
     * Inner register method.
     * @param {Map<string, EventEmitter} map
     * @param {string} id The key id.
     * @param {ChannelCallback} callback The callback to register.
     */
    __registerCallback(map, id, callback) {
        // We create a new event emitter if no callback has been registered for the given channel.
        if (!map.has(id)) {
            const emitter = new EventEmitter({captureRejections: true}); // Necessary to catch errors in asynchronous callbacks.
            emitter.on('error', err => logger.error(err)); // To prevent errors during a callback.
            map.set(id, emitter);
        }
        const eventEmitter = map.get(id);
        if (!eventEmitter.listeners('message').includes(callback)) { // Don't register the same function twice.
            eventEmitter.on('message', callback);
        }
    }

    /**
     * Inner unregister method.
     * @param {Map<string, EventEmitter} map
     * @param {string} id The key id.
     * @param {ChannelCallback} callback The callback to unregister.
     */
    __removeCallback(map, id, callback) {
        if (!map.has(id)) {
            return;
        }
        const emitter = map.get(id);
        emitter.removeListener('message', callback);
    }

    /**
     * Inner method to invoke callbacks.
     * @param {Map<string, EventEmitter} map
     * @param {string} id
     * @param {Message} msg
     */
    __invokeCallbacks(map, id, msg) {
        const emitter = map.get(id);
        if (!emitter)
            return;
        
        try {
            emitter.emit('message', msg);
        }
        catch (err) {
            logger.error(err);
        }
    }

    /**
     * Register a callback to listen to a specific channel.
     * @param {string} channelId The id of the channel to listen to.
     * @param {ChannelCallback} callback The callback to register.
     */
    registerSingleChannelCallback(channelId, callback) {
        this.__registerCallback(this.singleChannelCallbacks, channelId, callback);
    }

    /**
     * Register a callback to listen to all channels from the given guild.
     * @param {string} guildId The id of the guild to listen to.
     * @param {ChannelCallback} callback The callback to register.
     */
    registerMultipleChannelCallback(guildId, callback) {
        this.__registerCallback(this.multipleChannelCallbacks, guildId, callback);
    }

    /**
     * Unregister a callback for a specific channel listening.
     * @param {string} channelId The id of the channel to unlisten.
     * @param {ChannelCallback} callback The callback to remove.
     */
    removeSingleChannelCallback(channelId, callback) {
        this.__removeCallback(this.singleChannelCallbacks, channelId, callback);
    }

    /**
     * Unregister a callback for a guild.
     * @param {string} guildId The id of the guild to unlisten.
     * @param {ChannelCallback} callback The callback to remove.
     */
    removeMultipleChannelCallback(guildId, callback) {
        this.__removeCallback(this.multipleChannelCallbacks, guildId, callback);
    }

    /**
     * Invoke the callbacks for the given channel.
     * @param {string} channelId The id of the channel.
     * @param {Message} msg The new message.
     */
    invokeSingleChannelCallbacks(channelId, msg) {
        this.__invokeCallbacks(this.singleChannelCallbacks, channelId, msg);
    }

    /**
     * Invoke the callbacks for the given guild.
     * @param {string} guildId The id of the guild.
     * @param {Message} msg The new message.
     */
    invokeMultipleChannelCallbacks(guildId, msg) {
        this.__invokeCallbacks(this.multipleChannelCallbacks, guildId, msg);
    }
}

module.exports = {
    ChannelListener
}