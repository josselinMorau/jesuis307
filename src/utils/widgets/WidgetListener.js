const { ButtonInteraction, Interaction, SelectMenuInteraction } = require("discord.js");
const EventEmitter = require("events");
const { BaseService } = require("../../BaseService.js");
const { logger } = require('../../logging/logging.js');

const EVENT_NAMES = {
    BUTTON: 'button',
    SELECT_MENU: 'selectMenu'
}

/**
 * Service to handle the callbacks invoked when a user interacts with a widget (buttons, menus, etc).
 */
class WidgetListener extends BaseService
{
    /**
     * 
     * @param {*} ctx The ClientContext bound to this service. 
     */
    constructor(ctx) {
        super(ctx);
        
        this.__emitter = new EventEmitter({captureRejections: true}); // Necessary to catch the errors of the asynchronous callbacks.
        this.__emitter.on('error', err => logger.error(err)); // In case an error occurs during a callback invocation.
    }

    /**
     * Internal method to register the callbacks.
     * @param {string} eventName The name of the event.
     * @param {(interaction : Interaction) => void} callback The callback to register.
     */
    __registerCallback(eventName, callback) {
        this.__emitter.on(eventName, callback);
    }

    /**
     * Internal method to remove the callbacks.
     * @param {string} eventName The name of the event.
     * @param {(interaction : Interaction) => void} callback The callback to remove.
     */
    __removeCallback(eventName, callback) {
        this.__emitter.removeListener(eventName, callback);
    }

    /**
     * Internal method to invoke the callbacks.
     * @param {string} eventName The name of the event.
     * @param {Interaction} interaction The interaction to pass as an argument.
     */
    __invokeCallbacks(eventName, interaction) {
        this.__emitter.emit(eventName, interaction);
    }
    
    /**
     * Register a callback function called whenever a button interaction is invoked.
     * Use this method when you want a persistent callback between multiple sessions.
     * For a direct interaction prefer to use `Message.awaitMessageComponent` or `TextChannel.awaitMessageComponent`.
     * @param {(buttonInteraction : ButtonInteraction) => void} callback The callback to register.
     */
    registerButtonCallback(callback) {
        this.__registerCallback(EVENT_NAMES.BUTTON, callback);
    }

    /**
     * Register a callback function called whenever a menu interaction is invoked.
     * Use this method when you want a persistent callback between multiple sessions.
     * For a direct interaction prefer to use `Message.awaitMessageComponent` or `TextChannel.awaitMessageComponent`.
     * @param {(selectMenuInteraction : SelectMenuInteraction) => void} callback The callback to register.
     */
    registerSelectMenuCallback(callback) {
        this.__registerCallback(EVENT_NAMES.SELECT_MENU, callback);
    }

    /**
     * Remove a callback from the button interaction listening.
     * @param {(buttonInteraction : ButtonInteraction) => void} callback The callback to remove.
     */
    removeButtonCallback(callback) {
        this.__removeCallback(EVENT_NAMES.BUTTON, callback);
    }

    /**
     * Remove a callback from the menu interaction listening.
     * @param {(selectMenuInteraction : SelectMenuInteraction) => void} callback The callback to remove.
     */
    removeSelectMenuCallback(callback) {
        this.__removeCallback(EVENT_NAMES.SELECT_MENU, callback);
    }

    /**
     * Invoke the callbacks when a button interaction occurs.
     * @param {ButtonInteraction} buttonInteraction 
     */
    invokeButtonCallbacks(buttonInteraction) {
        this.__invokeCallbacks(EVENT_NAMES.BUTTON, buttonInteraction);
    }

    /**
     * Invoke the callbacks when a menu interaction occurs.
     * @param {SelectMenuInteraction} menuInteraction 
     */
    invokeSelectMenuCallbacks(menuInteraction) {
        this.__invokeCallbacks(EVENT_NAMES.SELECT_MENU, menuInteraction);
    }
}

module.exports = {
    WidgetListener
}