
/**
 * @enum {string} Enum of all the special role names.
 */
const ROLE_NAMES = {
    NONE: "NONE",
    EISTIEN: "EISTIEN",
    ING1: "ING1",
    ING2: "ING2",
    ING3: "ING3",
    ANCIEN: "ANCIEN",
    PRISONER: "PRISONER"
}

/**
 * Returns all role names.
 */
function getRoleNames() {
    return Object.values(ROLE_NAMES);
}

/**
 * Retourne `true` si le nom de rôle donné est valide.
 * @param {string} roleName  
 */
function isValidRoleName(roleName) {
    return getRoleNames().includes(roleName);
}

module.exports = {
    ROLE_NAMES,
    isValidRoleName,
    getRoleNames
}