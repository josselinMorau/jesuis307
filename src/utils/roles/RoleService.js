const { RoleInfo } = require('../../database/models/RoleInfo.js');
const { ROLE_NAMES, isValidRoleName, getRoleNames } = require('./roles.js');
const { logger } = require('../../logging/logging.js');
const { Role, Guild } = require('discord.js');
const { BaseService } = require('../../BaseService.js');

/**
 * @class Service handling data around specific roles for each guild.
 */
class RoleService extends BaseService {

    /**
     * 
     * @param ctx 
     */
    constructor(ctx) {
        super(ctx);

        // Register Discord's callbacks.
        this.clientContext.client.on('roleDelete', role => {
            this.deleteOneRoleInfo(role);
        });
    }

    /**
     * Returns the role with the given name from the given guild.
     * @param {Guild} guild The guild to search the role in.
     * @param {ROLE_NAMES} roleName The name of the role.
     * @returns {Promise<Role?>} Returns `null` if not found.
     */
    async getRoleWithName(guild, roleName) {
        const roleInfo = await this.findRoleInfoWithName(guild.id, roleName);
        if (!roleInfo)
            return null;
        return (await guild.roles.fetch(roleInfo.roleId).catch(() => {})) || null;
    }

    /**
     * Returns the RoleInfo with the given id.
     * @param {string} roleId The id of the role. 
     * @returns {RoleInfo?}
     */
    async findOneRoleInfo(roleId) {
        return await RoleInfo.findByPk(roleId)
            .catch(_err => logger.error(_err));
    }

    /**
     * Returns the RoleInfo with the given role name.
     * @param {string} guildId The id of the guild.
     * @param {ROLE_NAMES} roleName The name of the role.
     * @returns {Promise<RoleInfo?>}
     */
    async findRoleInfoWithName(guildId, roleName) {
        return await RoleInfo.findOne({where: { guildId: guildId, roleName: roleName }})
            .catch(_err => { logger.error(_err); });
    }

    /**
     * Returns all RoleInfos from the given guild.
     * @param {string} guildId The id of the guild.
     * @returns {Promise<RoleInfo[]>}
     */
    async findAllRoleInfos(guildId) {
        return await RoleInfo.findAll({ where: { guildId: guildId } })
            .catch(_err => { logger.error(_err); return []; });
    }

    /**
     * Update infos on the given role. If no RoleInfo exists for this role, it will be created.
     * @param {Role} role The role to update.
     * @param {(info : RoleInfo) => void} updateFn The update callback function
     * @returns {Promise<boolean>} Returns `true` if the operation is successfull.
     */
    async upsertRoleInfo(role, updateFn) {
        const [roleInfo, _created] = await RoleInfo.findOrCreate({ where: { roleId: role.id, guildId: role.guild.id } })
            .catch(_err => { logger.error(_err); return [null, false]; });
        
        if (!roleInfo)
            return false;

        updateFn(roleInfo);
        const success = await roleInfo.save().catch(_err => logger.error(_err));
        return !!success;
    }

    /**
     * Define the name of the given role. If no corresponding RoleInfo exists, it will be created.
     * @param {Role} role The role to update.
     * @param {ROLE_NAMES} roleName The name of the role. If it's invalid, the operation will be canceled.
     * @returns {Promise<boolean>} Returns `true` if the operation is successfull.
     */
    async setRoleName(role, roleName) {
        if (!isValidRoleName(roleName)) {
            return false;
        }

        return await this.upsertRoleInfo(role, info => {
            info.roleName = roleName;
        });
    }

    /**
     * Remove the corresponding RoleInfo from the database.
     * @param {Role} role The role to delete.
     */
    async deleteOneRoleInfo(role) {
        await RoleInfo.destroy({ where: { roleId: role.id } })
            .catch(_err => logger.error(_err));
    }
}

module.exports = {
    RoleService,
    ROLE_NAMES,
    isValidRoleName,
    getRoleNames
}