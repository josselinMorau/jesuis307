const { Message, MessageEmbed, TextChannel, BaseGuildTextChannel, User, MessageButton, MessageActionRow, Guild, GuildMember } = require("discord.js");
const { v4: uuidv4 } = require('uuid');
const { InvalidParams } = require("./errors.js");

/**
 * Collection of utility constants.
 */
const CONSTANTS = {
    /**
     * La limite de la longueur d'un message dans discord.
     */
    DISCORD_MESSAGE_LENGHT_LIMIT: 2000,
    /**
     * Embed titles are limited to 256 characters
     */
    DISCORD_EMBED_TITLE_MAX_LENGTH: 256,
    /**
     * Embed descriptions are limited to 4096 characters
     */
    DISCORD_EMBED_DESCRIPTION_MAX_LENGTH: 4096,
    /**
     * A field's name is limited to 256 characters and its value to 1024 characters
     */
    DISCORD_EMBED_FIELD_NAME_MAX_LENGTH: 256,
    /**
     * A field's name is limited to 256 characters and its value to 1024 characters
     */
    DISCORD_EMBED_FIELD_VALUE_MAX_LENGTH: 1024,
    /**
     * There can be up to 25 fields
     */
    DISCORD_EMBED_FIELD_MAX_COUNT: 25,
    /**
     * The footer text is limited to 2048 characters
     */
    DISCORD_EMBED_FOOTER_MAX_LENGTH: 2048,
    /**
     * The author name is limited to 256 characters
     */
    DISCORD_EMBED_AUTHOR_NAME_MAX_LENGTH: 256,
    /**
     * The sum of all characters from all embed structures in a message must not exceed 6000 characters
     */
    DISCORD_EMBED_ALL_TEXT_MAX_LENGTH: 6000,
    /**
     * Ten embeds can be sent per message
     */
    DISCORD_EMBED_PER_MESSAGE_MAX_COUNT: 10,
    /**
     * You can have up to 5 Action Rows per message
     */
    DISCORD_ACTION_ROW_PER_MESSAGE_MAX_COUNT: 5,
    /**
     * An Action Row can contain up to 5 buttons
     */
    DISCORD_BUTTON_PER_ACTION_ROW_MAX_COUNT: 5,
    /**
     * An Action Row can contain only one select menu
     */
    DISCORD_MENU_PER_ACTION_ROW_MAX_COUNT: 1,
    /**
     * The embed color for settings/roles/channels etc...
     */
    COLOR_SETTING: "#ea9c30",
    /**
     * The embed color for a copy of a message.
     */
    COLOR_MESSAGE: "#0f8ad1",
    /**
     * The embed color for infos.
     */
    COLOR_INFO: "#2ab300",
    /**
     * The embed color for infos on a destroy operation.
     */
    COLOR_DESTROY: "#de0700",
    /**
     * The embed color for music related embeds.
     */
    COLOR_MUSIC: "#e905f5"
}

/**
 * Set of color modifiers for the terminal.
 */
const TERMINAL_COLORS = {
    Reset: "\x1b[0m",
    Bright: "\x1b[1m",
    Dim: "\x1b[2m",
    Underscore: "\x1b[4m",
    Blink: "\x1b[5m",
    Reverse: "\x1b[7m",
    Hidden: "\x1b[8m",

    FgBlack: "\x1b[30m",
    FgRed: "\x1b[31m",
    FgGreen: "\x1b[32m",
    FgYellow: "\x1b[33m",
    FgBlue: "\x1b[34m",
    FgMagenta: "\x1b[35m",
    FgCyan: "\x1b[36m",
    FgWhite: "\x1b[37m",

    BgBlack: "\x1b[40m",
    BgRed: "\x1b[41m",
    BgGreen: "\x1b[42m",
    BgYellow: "\x1b[43m",
    BgBlue: "\x1b[44m",
    BgMagenta: "\x1b[45m",
    BgCyan: "\x1b[46m",
    BgWhite: "\x1b[47m"
}

/**
 * Retourne true si `derivedType` est une sous-classe de `baseType`.
 * @param {*} derivedType Le sous-type à tester.
 * @param {*} baseType Le type parent à tester.
 */
function isSubclassOf(derivedType, baseType) {
    return derivedType.prototype instanceof baseType || derivedType === baseType;
}

/**
 * Récupère le contenu d'un message sans les emojis et les mentions.
 * @param {Message} msg Un message.
 */
function getCleanerContent(msg) {
    const contents = msg.content.split(/\s+/gmi)
        .filter(content => {
            return !!content && !/<[#@].*>/gm.test(content) /*mention*/ && !/:.*:/gm.test(content) /* emoji serveur */
                && !/\p{Extended_Pictographic}/u.test(content); /* emoji unicode */
        });
    return contents.join(" ");
}

/**
 * Retourne l'id d'un channel à partir de sa représentation dans un message.
 * @param {string} channelStr
 * @returns {string?} L'id du channel ou null si aucun id n'a pu être parsé.
 */
function parseChannelId(channelStr) {
    if (!channelStr)
        return null;

    const results = /<#([0-9]*)>/gm.exec(channelStr);
    return results ? results[1] : null;
}

/**
 * Retourne l'id d'un user à partir de sa représentation dans un message.
 * @param {string} userStr
 * @returns {string?} L'id du user ou null si aucun id n'a pu être parsé.
 */
function parseUserId(userStr) {
    if (!userStr)
        return null;
    
    const results = /<@!?([0-9]*)>/gm.exec(userStr);
    return results ? results[1] : null;
}

/**
 * Retourne l'id d'un role à partir de sa représentation dans un message.
 * @param {string} roleStr
 * @returns {string?} L'id du role ou null si aucun id n'a pu être parsé.
 */
function parseRoleId(roleStr) {
    if (!roleStr)
        return null;

    const results = /<@&([0-9]*)>/gm.exec(roleStr);
    return results ? results[1] : null;
}

/**
 * Returns a boolean from it's string representation.
 * @param {string} boolStr
 * @returns {boolean | undefined} Returns the corresponding boolean if the parsing succeeds, `undefined` otherwise.
 */
function parseBoolean(boolStr) {
    if (typeof boolStr === "boolean")
        return boolStr;
    if (!boolStr)
        return undefined;
    const _boolStr = boolStr.trim().toLowerCase();
    return (_boolStr === "true" || _boolStr === "false") ? _boolStr === "true" : undefined;
}

/**
 * Returns a formated string. Format arguments have the form '{i}' where i is the index of the argument.
 * @param {string} str The string to format.
 * @param  {...any} args The substitute arguments.
 */
function format(str, ...args) {
    return str.replace(/{(\d+)}/g, (match, index) => {
        return args[index] !== undefined ? `${args[index]}` : match;
    });
}

/**
 * Returns the expected count of arguments for a format string.
 * @param {string} str A format string.
 */
function getFormatStringArgCount(str) {
    const results = str.matchAll(/{(\d+)}/g);
    var maxIndex = -1;
    for (let result of results) {
        let index = parseInt(result[1]);
        maxIndex = index > maxIndex ? index : maxIndex;
    }
    return maxIndex + 1;
}

/**
 * Retourne la représentation adéquate de la valeur donnée en string.
 * Pour un string vide retourne `''` pour éviter de passer un string vide au message ou aux embeds.
 * @param {any} val 
 */
function repr(val) {
    return val !== "" ? `${val}` : "''";
}

/**
 * Delete all of the null or undefined fields in an object.
 * @param {object} object The object to clean.
 */
function cleanObject(object) {
    for (let key in object) {
        if (object[key] === undefined || object[key] === null)
            delete object[key];
    }
}

/**
 * Creates an array of embeds to fill from the given data.
 * The method makes sure to not go over the Discord's embed limits.
 * @param {import("discord.js").MessageEmbedOptions} options The embed options to copy for each new embed of the array.
 * @param {T[]} datas An array of datas to iterate over.
 * @param {(embed : MessageEmbed, data : T) => void} fieldUpdate The method called for each data in the array.
 * Avoid to add several fields per data unless you're sure you won't go over the limit.
 * @template T
 * @returns {MessageEmbed[]}
 */
function createEmbedsFromData(options, datas, fieldUpdate) {
    const embeds = [];
    var embed = new MessageEmbed(options);
    
    datas.forEach(data => {
        if (embed.fields.length >= CONSTANTS.DISCORD_EMBED_FIELD_MAX_COUNT) {
            embeds.push(embed);
            embed = new MessageEmbed(options);
        }

        fieldUpdate(embed, data);
    });

    if (embed.fields.length > 0)
        embeds.push(embed);
    return embeds;
}

/**
 * Same as `createEmbedsFromData` but asynchronous. Each fieldUpdate call is awaited.
 * @param {import("discord.js").MessageEmbedOptions} options The embed options to copy for each new embed of the array.
 * @param {T[]} datas An array of datas to iterate over.
 * @param {(embed : MessageEmbed, data : T) => Promise<void>} fieldUpdate The method called for each data in the array.
 * Avoid to add several fields per data unless you're sure you won't go over the limit.
 * @template T
 * @returns {Promise<MessageEmbed[]>}
 */
async function createEmbedsFromDataAsync(options, datas, fieldUpdate) {
    const embeds = [];
    var embed = new MessageEmbed(options);
    
    for (let data of datas) {
        if (embed.fields.length >= CONSTANTS.DISCORD_EMBED_FIELD_MAX_COUNT) {
            embeds.push(embed);
            embed = new MessageEmbed(options);
        }

        await fieldUpdate(embed, data);
    }

    if (embed.fields.length > 0)
        embeds.push(embed);
    return embeds;
}

/**
 * Returns the creation date of the given Discord id.
 * @param {string} id The id of a Discord object (message, user, channel, etc).
 */
function snowflakeToDate(id) {
    // See https://discord.com/developers/docs/reference#snowflakes-snowflake-id-format-structure-left-to-right
    const timestamp = (BigInt(id) >> BigInt(22)) + BigInt(1420070400000); // Use BigInt because of the limit of regular numbers.
    return new Date(Number(timestamp));
}

/**
 * An async generator function to iterate over the messages of a specific text channel from the latest to the oldest.
 * @param {BaseGuildTextChannel} channel The channel to search the messages in.
 * @param {object} options Fetch options.
 * @param {number} options.count The amount of messages to fetch. Optional only if `options.from` is not specified.
 * @param {Date | string} options.from The date or the message id from which to fetch messages. Optional only if `options.count` is not specified.
 * @param {Date | string} options.to The date or the message id up to which to fetch messages. (Optional)
 */
async function* fetchMessages(channel, options) {
    const hasCount = isFinite(options.count);
    const fromDate = options.from instanceof Date ? options.from : 
        (typeof options.from === "string" ? snowflakeToDate(options.from) : null);
    const toDate = options.to instanceof Date ? options.to : 
        (typeof options.to === "string" ? snowflakeToDate(options.to) : null);

    if (!hasCount && !fromDate)
        throw new InvalidParams("fetchMessages should have a parameter 'count' or a parameter 'from'.");
    
    var before = typeof options.to === "string" ? options.to : undefined;
    var remaining = hasCount ? Math.min(options.count,100) : 100;

    // before option in fetch is exclusive so we need to fetch this specific message before.
    if (typeof options.to === "string") {
        let firstMsg = await channel.messages.fetch(options.to);
        if (firstMsg) {
            yield firstMsg;
            --remaining;
        }
    }

    while (remaining > 0) {
        let messages = await channel.messages.fetch({ limit: remaining, before: before });
        if (!messages || messages.size === 0)
            break;
        
        for (let msg of messages.values()) {
            // End iteration if date range has been exceed.
            if (fromDate && (msg.createdAt - fromDate < 0)) {
                remaining = 0;
                break;
            }
            if (toDate && (msg.createdAt - toDate > 0))
                continue; // Ignore message outside of the range.
            yield msg;
            before = msg.id;
            if (hasCount)
                --remaining;
        }
    }
}

/**
 * Parse and returns a message's id from its link.
 * @param {string} messageLink 
 */
function parseMessageIdFromLink(messageLink) {
    return messageLink.split('/').at(-1);
}

/**
 * 
 * @typedef {object} Hour
 * @property {number} hours
 * @property {number} minutes 
 */

/**
 * Parse and returns the corresponding hour.
 * @param {string} hour A string representing an hour. The format is HH:MM.
 * @returns {Hour?} The corresponding Hour object if the parsing is successfull, `null` otherwise.
 */
function parseHour(hour) {
    if (!hour)
        return null;
    
    const hourRes = /([0-9]+):([0-9]+)/gmi.exec(hour);
    if (!hourRes)
        return null;
    
    const hours = parseInt(hourRes[1]);
    const minutes = parseInt(hourRes[2]);

    if (isNaN(hours) || isNaN(minutes) || hours < 0 || hours >= 24 || minutes < 0 || minutes >= 60)
        return null;
    
    return {
        hours: hours,
        minutes: minutes
    }
}

/**
 * Parse and returns the corresponding date.
 * @param {string} date A string representing a date. The format is DD/MM/YYYY.
 * @returns {Date?} The corresponding Date object if the parsing is successfull, `null` otherwise.
 */
function parseDate(date) {
    if (!date)
        return null;
    
    const dateRes = /([0-9]+)\/([0-9]+)\/([0-9]+)/gmi.exec(date);
    if (!dateRes)
        return null;
    
    const day = parseInt(dateRes[1]);
    const month = parseInt(dateRes[2]);
    const year = parseInt(dateRes[3]);

    if (isNaN(day) || isNaN(month) || isNaN(year))
        return null;
    
    return new Date(year, month-1, day); // Month is 0-indexed.
}

/**
 * Parses and returns the corresponding date.
 * @param {string[]} dateAndHour The date and hour given in a string array. The format is DD/MM/YYYY and HH:MM.
 * @returns {Date?} The corresponding date object if the parsing is successfull, `null` otherwise.
 */
function parseDateAndHour(dateAndHour) {
    if (!dateAndHour || dateAndHour.length === 0)
        return null;
    
    if (dateAndHour.length === 1) {
        const asDate = parseDate(dateAndHour[0]);
        if (isValidDate(asDate))
            return asDate;

        const asHour = parseHour(dateAndHour[1]);
        if (!asHour)
            return null;
        const date = new Date();
        date.setHours(asHour.hours);
        date.setMinutes(asHour.minutes);
        return date;
    }

    const date = parseDate(dateAndHour[0]);
    const hour = parseHour(dateAndHour[1]);

    if (!isValidDate(date) || !hour)
        return null;
    
    date.setHours(hour.hours);
    date.setMinutes(hour.minutes);
    return date;
}

/**
 * Returns `true` if the given Date object is valid.
 * @param {Date} date 
 */
function isValidDate(date) {
    return !!date && date instanceof Date && !isNaN(date);
}

/**
 * Returns a Promise that resolves itself after a given amount of time.
 * @param {number} time The amount of time to wait in ms. 
 */
function sleep(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

/**
 * Parse the fetch options for the `fetchMessages` method from command arguments.
 * @param {string[]} args The command arguments.
 * @param {Object<string,string[]>} kwargs The command keyword arguments.
 * @param {object} options Options for this method.
 * @param {number} options.countIndex The index in the arguments to parse the count.
 */
 function parseFetchOptions(args, kwargs, options = {}) {
    const fetchOptions = {};
    
    const countIndex = options.countIndex || 0;
    if (args.length > 0) {
        const count = parseInt(args[countIndex]);
        if (!isNaN(count))
            fetchOptions.count = count;
    }
    
    const { from, to } = kwargs;
    if (from) {
        const fromDate = parseDateAndHour(from);
        if (isValidDate(fromDate))
            fetchOptions.from = fromDate;
        else if (from.length > 0)
            fetchOptions.from = parseMessageIdFromLink(from[0]);
    }

    if (to) {
        const toDate = parseDateAndHour(to);
        if (isValidDate(toDate))
            fetchOptions.to = toDate;
        else if (to.length > 0)
            fetchOptions.to = parseMessageIdFromLink(to[0]);
    }

    return fetchOptions;
}

/**
 * 
 * @typedef {object} ChoiceOption
 * @property {string} label
 * @property {string?} customId
 * @property {import("discord.js").MessageButtonStyleResolvable?} style
 */

/**
 * Create a message with a question with multiple buttons and returns the answer the given user has chosen.
 * @param {User | GuildMember} user The user to send the question to. He will be the only one to be able to answer the question.
 * @param {TextChannel} channel The channel to send the question in.
 * @param {number} timeout The amount of time in ms given to answer question.
 * @param {import("discord.js").MessageOptions} confirmMessageOptions The message options of the question (content, embeds, etc).
 * @param {string[] | ChoiceOption[]} choices An array of possible answers. An answer is either a string or a detaild `ChoiceOption` object.
 * @returns {Promise<[string?, Message]>} A [answer, message] pair. The first value is the answer chosen by the user aka the customId of the chosen button.
 * The value is `null` if there's a timeout. The seconde value is the message created inside the function.
 */
async function waitForUserAnswer(user, channel, timeout, confirmMessageOptions, choices) {
    const buttons = choices.map(choice => {
        const button = new MessageButton();
        if (typeof choice === "string") {
            button.setCustomId(choice)
                .setLabel(choice)
                .setStyle("PRIMARY");
        }
        else {
            button.setCustomId(choice.customId || choice.label)
                .setLabel(choice.label)
                .setStyle(choice.style || "PRIMARY");
        }
        return button;
    });

    const components = [];
    for (let i=0; i<buttons.length; i+=5) {
        const row = new MessageActionRow({ components: buttons.slice(i,i+5) }); // We have to create slices because each row can contain 5 buttons at best.
        components.push(row);
    }

    confirmMessageOptions.components = components;
    const confirmMessage = await channel.send(confirmMessageOptions);
    /**
     * 
     * @param {ButtonInteraction} interaction 
     */
    const filter = (interaction) => {
        if (interaction.member.id !== user.id) {
            // Prevent other users to interact with the buttons.
            interaction.reply({ ephemeral: true, content: "Ce message ne t'est pas destiné. :stuck_out_tongue:" })
                .catch(_err => {});
            return false;
        }
        return true;
    };
    const userInteraction = await confirmMessage.awaitMessageComponent({ filter: filter, time: timeout, componentType: "BUTTON" })
        .catch(_err => {});
    
    // Disable all the buttons once the answer was given.
    components.forEach(row => row.components.forEach(button => button.setDisabled(true)));
    if (userInteraction) {
        // Through interaction to resolve it.
        await userInteraction.update({ components: components }).catch(() => {});
    }
    else {
        // Through message edit.
        await confirmMessage.edit({ components: components }).catch(() => {});
    }

    return [userInteraction ? userInteraction.customId : null, confirmMessage];
}

/**
 * Fetch all the urls in the given string.
 * @param {string} content 
 * @returns {string[]} An array of all the found urls.
 */
function fetchURLs(content) {
    return content.match(
        /https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/\/=]*)/gm
    ) || [];
}

/**
 * Fetch the first url in the given string.
 * @param {string} content 
 * @returns {string?} The url found if any. `null` otherwise.
 */
function fetchURL(content) {
    return fetchURLs(content)[0] || null;
}

/**
 * Returns the unaccentued version of the given string.
 * @param {string} str 
 * @returns {string}
 */
function unaccent(str) {
    return str.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
}

/**
 * Returns the names of all the methods from a class.
 * @param cls The class.
 * @param {boolean} includeStatic Whether to include static methods as well. Default is `false`.
 * @returns {string[]} 
 */
function getClassMethodNames(cls, includeStatic = false) {
    var methods = [];
    if (includeStatic)
        methods = methods.concat(Object.getOwnPropertyNames(cls).filter(fn => typeof cls[fn] === "function"));
    methods = methods.concat(Object.getOwnPropertyNames(cls.prototype).filter(fn => typeof cls.prototype[fn] === "function"));
    return methods;
}

/**
 * Same as `Array.filter` but asynchronous.
 * @template T
 * @param {T[]} array The array to filter.
 * @param {(element : T, index : number, array : T[]) => Promise<boolean>} callback Predicate to test each element of the array.
 * @returns {Promise<T[]>} Like `Array.filter`, returns a new array with only elements that passes the test.
 */
async function asyncFilter(array, callback) {
    const newArray = [];
    var index = 0;
    for (let element of array) {
        const isValid = await callback(element, index++, array);
        if (isValid)
            newArray.push(element);
    }
    return newArray;
}

/**
 * Returns a random number between min and max (inclusive).
 * @param {number} min 
 * @param {number} max 
 */
function randomRange(min, max) {
    return min + Math.random() * (max - min);
}

/**
 * Returns a random integer between min and max (inclusive).
 */
function randomRangeInt(min, max) {
    return Math.floor(randomRange(min, max));
}

/**
 * Returns a random element of the given array.
 * @template T
 * @param {T[]} array 
 * @returns {T}
 */
function randomElement(array) {
    return array.at(randomRangeInt(0, array.length - 1));
}

/**
 * Returns a shuffled copy of the given array.
 * @template T
 * @param {T[]} array 
 * @returns {T[]}
 */
 function shuffle(array) {
    // Copy the array.
    const shuffled = array.map(x => x);
    for (let i=array.length-1; i>0; --i) {
        const index = randomRangeInt(0, i);
        const temp = shuffled[i];
        shuffled[i] = shuffled[index];
        shuffled[index] = temp;
    }
    return shuffled;
}

/**
 * Returns a random UUIDV4.
 * @returns {string}
 */
function uuid() {
    return uuidv4();
}

/**
 * Tries to find the message with the given id in a guild.
 * @param {Guild} guild The guild to search the message in.
 * @param {string} msgId The id of the message to search.
 * @returns {Promise<Message?>} Returns `null` if no message was found.
 */
async function findMessageInGuild(guild, msgId) {
    const channels = [...(await guild.channels.fetch()).values()]
        .filter(channel => channel.isText() || channel.isThread());
    for (let channel of channels) {
        const msg = await channel.messages.fetch(msgId, { cache: true }).catch(() => {});
        if (msg)
            return msg;
    }
    // If not found, search in threads.
    for (let channel of channels) {
        const threads = await channel.threads.fetch();
        for (let thread of threads.threads.values()) {
            const msg = await channel.messages.fetch(msgId, { cache: true }).catch(() => {});
            if (msg)
                return msg;
        }
    }
    return null;
}

/**
 * Enable/disable all components of the given message.
 * @param {Message} msg The message whose component to enable/disable.
 * @param {boolean} enabled Whether to enable the components.
 */
async function setMessageComponenentsEnabled(msg, enabled) {
    const components = msg.components;
    if (components.length === 0)
        return;
    
    components.forEach(row => {
        row.components.forEach(comp => {
            comp.setDisabled(!enabled);
        });
    });
    await msg.edit({ components: components });
}

module.exports = {
    CONSTANTS,
    TERMINAL_COLORS,
    isSubclassOf,
    getCleanerContent,
    parseChannelId,
    parseUserId,
    parseRoleId,
    parseBoolean,
    format,
    getFormatStringArgCount,
    repr,
    cleanObject,
    createEmbedsFromData,
    createEmbedsFromDataAsync,
    fetchMessages,
    snowflakeToDate,
    parseMessageIdFromLink,
    parseDateAndHour,
    parseHour,
    parseDate,
    isValidDate,
    sleep,
    parseFetchOptions,
    waitForUserAnswer,
    fetchURLs,
    fetchURL,
    getClassMethodNames,
    asyncFilter,
    unaccent,
    randomRange,
    randomRangeInt,
    randomElement,
    shuffle,
    uuid,
    findMessageInGuild,
    setMessageComponenentsEnabled
}
