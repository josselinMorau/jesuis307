
/**
 * @class Custom implementation of a queue container that avoids the O(n) cost of the shift method of classic JS arrays.
 * @template T
 */
class Queue
{
    /**
     * Constructs a new Queue object.
     */
    constructor()
    {
        /**
         * @type {T[]}
         */
        this.__pushedData = [];
        /**
         * @type {T[]}
         */
        this.__toPopData = [];
    }

    /**
     * Erase all elements from the queue.
     */
    clear() {
        this.__pushedData.length = 0;
        this.__toPopData.length = 0;
    }

    /**
     * Return the current size of the queue.
     * @returns {number}
     */
    size() { return this.__pushedData.length + this.__toPopData.length; }

    /**
     * Inserts all the given elements at the end of the queue.
     * @param  {...T} elems 
     */
    push(...elems) {
        elems.forEach(elem => { 
            this.__pushedData.push(elem);
        });
    }

    /**
     * Removes and returns the first element of the queue.
     * @returns {T}
     */
    pop() {
        if (this.__toPopData.length === 0 && this.__pushedData.length > 0) {
            while (this.__pushedData.length > 0) {
                this.__toPopData.push(this.__pushedData.pop());
            }
        }
        return this.__toPopData.pop();
    }

    /**
     * Returns an array of the inner elements from the first inserted to the last.
     * @param {number} start (Optional) The index of the first element to iterate over. Default is 0.
     * @param {number} end (Optional) The index of the last element to iterate over. Default is queue's size - 1.
     * @returns {T[]}
     */
    toArray(start = 0, end = undefined) {
        return [...this.iterate(start, end)];
    }

    /**
     * Same as `toArray` but from the last inserted to the first.
     * @param {number} start (Optional) The index of the first element to iterate over. Default is queue's size -1.
     * @param {number} end (Optional) The index of the last element to iterate over. Default is 0.
     * @returns {T[]}
     */
    toReverseArray(start = undefined, end = 0) {
        return [...this.reverseIterate(start, end)];
    }

    /**
     * Returns a Generator to iterate over the elements of the queue from the first inserted to the last.
     * @param {number} start (Optional) The index of the first element to iterate over. Default is 0.
     * @param {number} end (Optional) The index of the last element to iterate over. Default is queue's size - 1.
     */
    * iterate(start = 0, end = undefined) {
        start = Math.max(0, start);
        if (!isFinite(end))
            end = this.size() - 1;
        if (start > end || start >= this.size() || end < 0)
            return;
        var count = end - start + 1;
        var from = this.__toPopData.length-1-start;
        var to = Math.max(0, from - count + 1);
        for (let i=from; i>=to; --i) {
            yield this.__toPopData[i];
            --count;
        }
        from = Math.max(start - this.__toPopData.length,0);
        to = Math.min(this.__pushedData.length, from + count);
        for (let i=from; i<to; ++i)
            yield this.__pushedData[i];
    }

    /**
     * Same as `iterate` but from the last inserted to the first.
     * @param {number} start (Optional) The index of the first element to iterate over. Default is queue's size -1.
     * @param {number} end (Optional) The index of the last element to iterate over. Default is 0.
     */
    * reverseIterate(start = undefined, end = 0) {
        end = Math.max(0, end);
        if (!isFinite(start))
            start = this.size() - 1;
        else
            start = Math.min(this.size() - 1, start);
        if (end > start || end >= this.size() || start < 0)
            return;
        var count = start - end + 1;
        var from = this.__pushedData.length - this.size() + start;
        var to =  Math.max(0, from - count + 1);
        for (let i=from; i>=to; --i) {
            yield this.__pushedData[i];
            --count;
        }
        from = Math.max(this.size() - 1 - start - this.__pushedData.length,0);
        to = Math.min(this.__toPopData.length, from + count);
        for (let i=from; i<to; ++i)
            yield this.__toPopData[i];
    }
}

module.exports = Queue;
