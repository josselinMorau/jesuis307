const { BaseService } = require("../../BaseService.js");
const { Settings } = require("../../database/models/Settings.js");
const { logger } = require("../../logging/logging.js");
const { SETTINGS, SETTING_NAMES, SPAM_MODE_VALUES } = require('./settings.js');

/**
 * Classe qui gère les settings de chaque serveur.
 */
class SettingService extends BaseService
{

    /**
     * Retourne une map de tous les settings du serveur donné.
     * @param {string} guildId L'id du serveur.
     * @returns {Promise<Map<string, any>>}
     */
    async getSettingMap(guildId) {
        const guildSettings = await Settings.findByPk(guildId);
        const proxy = guildSettings ? guildSettings.createProxy() : {};
        const map = new Map(Object.entries(proxy));
        map.delete("guildId");

        // Remplis la map avec les valeurs par défaut pour les settings non définis.
        Object.entries(SETTINGS).forEach(([settingName, settingInfo]) => {
            const val = map.get(settingName);
            if (val === undefined || val === null) {
                map.set(settingName, settingInfo.defaultValue);
            }
        });

        return map;
    }

    /**
     * Retourne l'option avec le nom donné.
     * @param {string} guildId L'id du serveur correspondant.
     * @param {string} settingName Le nom de l'option.
     * @returns La valeur de l'option. La valeur par défaut définie dans `settings` est choisie si l'option n'est pas définie dans la bdd.
     * Retourne `undefined` si le nom de l'option n'existe pas. 
     */
    async getSetting(guildId, settingName) {
        if (!SETTINGS[settingName])
            return undefined;
        const guildSettings = await Settings.findByPk(guildId);
        return !!guildSettings && guildSettings[settingName] !== null && guildSettings[settingName] !== undefined ?
            guildSettings[settingName] : SETTINGS[settingName].defaultValue;
    }

    /**
     * Définis la valeur de l'option donnée pour un serveur.
     * @param {string} guildId L'id du serveur.
     * @param {string} settingName Le nom du setting.
     * @param {any} value La valeur du setting.
     * @returns {Promise<boolean>} Retourne `true` si l'opération est un succès.
     */
    async setSetting(guildId, settingName, value) {
        if (!SETTINGS[settingName] || value === undefined)
            return false;
        
        const settingInfo = {
            guildId: guildId,
            [settingName]: value
        };
        const success = Settings.upsert(settingInfo, { fields: [ "guildId", settingName ] })
            .catch(_err => logger.error(_err) );
        return !!success;
    }

    /**
     * Return the prefix define for the guild if one, the default one otherwise.
     * @param {string} guildId
     * @returns {Promise<string>}
     */
    async getPrefix(guildId) {
        return await this.getSetting(guildId, SETTING_NAMES.PREFIX);
    }
}

module.exports = {
    SettingService,
    SETTING_NAMES,
    SPAM_MODE_VALUES
}
