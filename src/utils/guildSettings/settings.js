const { DataTypes } = require('sequelize');

/**
 * @enum {string} Enum of all the setting names.
 */
const SETTING_NAMES = {
    PREFIX: "prefix",
    WELCOME_ENABLED: "welcomeEnabled",
    WELCOME_MESSAGE: "welcomeMessage",
    TASK_COOLDOWN: "taskCooldown",
    JAIL_DEFAULT_DURATION: "jailDefaultDuration",
    SPAM_MODE: "spamMode"
};

/**
 * @typedef {Object} SettingInfo
 * @property {string} description A user-friendly description of the setting and its purpose.
 * @property {DataTypes} type The setting type. Must be a member of `DataTypes`.
 * @property {any} defaultValue The default value of the setting when it's not set in the database.
 * @property {number} argsCount The number of arguments that the setting take when parsing.
 * @property {(args : string[]) => [boolean, any]} parser The parsing function. If not defined `parseByDefault` is called instead.
 */

/**
 * Returns only the first value.
 * @param {string[]} values 
 * @returns {[boolean, string]}
 */
function parseFirstString(values) {
    return [true, values.at(0)];
}

/**
 * Returns a valid integer only if it's strictly positive (> 0).
 * @param {string[]} values
 * @returns {[boolean, number]} 
 */
function parseStrictPositiveInteger(values) {
    const res = parseInt(values[0]);
    const isValid = !isNaN(res) && res > 0;
    return [isValid, res];
}

/**
 * @enum {string} Accepted values for the 'spamMode' setting.
 */
const SPAM_MODE_VALUES = {
    FREE: "FREE", // Allow spam commands everywhere.
    RESTRICTED: "RESTRICTED", // Only allow spam commands in spamm channels.
    BAN: "BAN" // Same as RESTRICTED but jail automatically the user if he doesn't respect the rule.
};

/**
 * Parser for the setting 'spamMode'. Accepted values are from the enum `SPAM_MODE_VALUES`.
 * @param {string[]} values 
 */
function parseSpamMode(values) {
    const value = values[0].toUpperCase();
    const acceptedValues = Object.values(SPAM_MODE_VALUES);
    const isValid = acceptedValues.includes(value);
    return [isValid, value];
}

/**
 * Object defining the name and properties of each setting.
 * Each field is a `SettingInfo`.
 * @type {Object<string, SettingInfo>} 
 */
const SETTINGS = {
    [SETTING_NAMES.PREFIX]: {
        type: DataTypes.STRING,
        defaultValue: "!",
        argsCount: 1,
        parser: parseFirstString,
        description: "Le préfixe de commandes utilisé pour ce serveur."
    },
    [SETTING_NAMES.WELCOME_ENABLED]: {
        type: DataTypes.BOOLEAN,
        defaultValue: false,
        argsCount: 1,
        description: "Si réglé à `true`, active l'accueil des nouveaux venus dans le serveur en leur demandant leur nom puis leur promo."
    },
    [SETTING_NAMES.WELCOME_MESSAGE]: {
        type: DataTypes.STRING,
        defaultValue: "",
        argsCount: 1,
        description: "Un texte d'introduction envoyé en MP aux nouveaux venus avant de leur envoyer la description de tous les channels."
    },
    [SETTING_NAMES.TASK_COOLDOWN]: {
        type: DataTypes.INTEGER,
        defaultValue: 30,
        argsCount: 1,
        description: "Le temps d'attente (en s) entre 2 \"grosse tâches\" de même type. Par exemple: copy et move."
    },
    [SETTING_NAMES.JAIL_DEFAULT_DURATION]: {
        type: DataTypes.INTEGER,
        defaultValue: 10,
        argsCount: 1,
        parser: parseStrictPositiveInteger,
        description: "La durée (en min) d'emprisonnement par défaut."
    },
    [SETTING_NAMES.SPAM_MODE]: {
        type: DataTypes.STRING,
        defaultValue: SPAM_MODE_VALUES.RESTRICTED,
        argsCount: 1,
        parser: parseSpamMode,
        description: "Définis comment doivent être traités les commandes de spam. Trois modes sont possibles : \n" + 
            "- FREE : Les commandes de spam sont autorisées partout.\n" + 
            "- RESTRICTED : Les commandes de spam ne sont autorisées que dans les channels avec la catégorie `SPAM`.\n" +
            "- BAN : Comme RESTRICTED mais si un utilisateur tente une commande de spam en dehors des channels dédiés, il est automatiquement envoyé en prison."
    }
};

/**
 * Default parser.
 * @param {DataTypes} type The setting type.
 * @param {string[]} values The array of values passed to the parser.
 * @returns {[boolean, any]} A [boolean,value] pair. The first member indicates if the parsing is valid, the second is the parsed value.
 */
function parseByDefault(type, values) {
    if (type === DataTypes.STRING) {
        return [true, values.join(" ")];
    }
    else if (type === DataTypes.DOUBLE || type === DataTypes.FLOAT) {
        const res = parseFloat(values[0]);
        return [!isNaN(res), res];
    }
    else if (type === DataTypes.INTEGER) {
        const res = parseInt(values[0]);
        return [!isNaN(res), res];
    }
    else if (type === DataTypes.BOOLEAN) {
        const value = values[0].toLowerCase();
        return [value === "true" || value === "false", value === "true"];
    }
    return [false, undefined];
}

/**
 * Parse the given values in the expected type.
 * @param {string} settingName The name of the setting.
 * @param {string[]} values An array of values to give to the parser.
 * @returns {[boolean, any]}  A [boolean,value] pair. The first member indicates if the parsing is valid, the second is the parsed value.
 * Returns `[false,undefined]` if the settingName doesn't exist.
 */
function parseSettings(settingName, values) {
    const setting = SETTINGS[settingName];
    if (!setting)
        return [false, undefined];
    
    return setting.parser ? setting.parser(values) : parseByDefault(setting.type, values);
}

/**
 * Returns `true` if the given setting name is valid.
 * @param {string} settingName
 */
function isValidSettingName(settingName) {
    return Object.values(SETTING_NAMES).includes(settingName);
}

/**
 * Returns the corresponding setting name with the correct case.
 * @param {string} settingName The name of the setting.
 * @returns {string | undefined} The corresponding setting name if it exists else `undefined`. 
 */
function getCaseInsensitiveSettingName(settingName) {
    const upper = settingName.toUpperCase();
    return Object.values(SETTING_NAMES).find(s => s.toUpperCase() === upper);
}

module.exports = {
    SETTING_NAMES,
    SETTINGS,
    SPAM_MODE_VALUES,
    parseSettings,
    isValidSettingName,
    getCaseInsensitiveSettingName
}