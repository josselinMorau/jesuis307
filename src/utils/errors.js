/**
 * File to describe custom error types.
 */

/**
 * Error thrown when wrong parameters are passed to a function.
 */
class InvalidParams extends Error
{
    constructor(...params) {
        super(...params);
    }
}

/**
 * Error thrown when a function is not implemented.
 */
class NotImplemented extends Error
{
    constructor(...params) {
        super(...params);
    }
}

/**
 * Error thrown when a provided index is out of the range of a collection.
 */
class OutOfRange extends Error
{
    constructor(...params) {
        super(...params);
    }
}

module.exports = {
    InvalidParams,
    NotImplemented,
    OutOfRange
}