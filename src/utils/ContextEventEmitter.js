const EventEmitter = require("events");

/**
 * @typedef {import('../ClientContext').ClientContext} ClientContext
 */

/**
 * Similar to `EventEmitter` but context dependent.
 */
class ContextEventEmitter {

    constructor() {
        /**
         * @type {Map<ClientContext, EventEmitter}
         */
        this.__emitters = new Map();
    }

    /**
     * Returns the inner `EventEmitter` bound to the given context.
     * @param {ClientContext} ctx The ClientContext bound to the searched emitter.
     * @param {boolean} createIfNotFound If `true` creates the emitter if it doesn't exist yet. Default is `false`.
     * @returns {EventEmitter}
     */
    getCtxEmitter(ctx, createIfNotFound = false) {
        var emitter = this.__emitters.get(ctx);
        // Lazy instanciation.
        if (!emitter && createIfNotFound) {
            emitter = new EventEmitter();
            this.__emitters.set(ctx, emitter);
        }
        return emitter;
    }

    /**
     * Clear all events bound to the given context.
     * @param {ClientContext} ctx 
     */
    clear(ctx) {
        const emitter = this.getCtxEmitter(ctx);
        if (emitter)
            emitter.removeAllListeners();
        this.__emitters.delete(ctx);
    }

    /**
     * Register a listener for the given event.
     * @param {ClientContext} ctx The context the event is bound to.
     * @param {string | Symbol} eventName The name of the event.
     * @param {(...args : any[]) => void} listener The callback function.
     * @returns {ContextEventEmitter} Returns a reference to the `ContextEventEmitter` so that calls can be chained.
     */
    on(ctx, eventName, listener) {
        const emitter = this.getCtxEmitter(ctx, true);
        emitter.on(eventName, listener);
        return this;
    }

    /**
     * Adds a one-time listener function for the event named `eventName`.
     * The next time `eventName` is triggered, this listener is removed and then invoked.
     * @param {ClientContext} ctx The context the event is bound to.
     * @param {string | Symbol} eventName The name of the event.
     * @param {(...args : any[]) => void} listener The callback function.
     * @returns {ContextEventEmitter} Returns a reference to the `ContextEventEmitter` so that calls can be chained.
     */
    once(ctx, eventName, listener) {
        const emitter = this.getCtxEmitter(ctx, true);
        emitter.once(eventName, listener);
        return this;
    }

    /**
     * Unregister a listener for the given event.
     * @param {ClientContext} ctx The context the event is bound to.
     * @param {string | Symbol} eventName The name of the event.
     * @param {(...args : any[]) => void} listener The callback function.
     * @returns {ContextEventEmitter} Returns a reference to the `ContextEventEmitter` so that calls can be chained.
     */
    off(ctx, eventName, listener) {
        const emitter = this.getCtxEmitter(ctx);
        if (emitter) {
            emitter.off(eventName, listener);
        }
        return this;
    }

    /**
     * Synchronously calls each of the listeners registered for the event named `eventName` and the bound to the given context,
     * in the order they were registered, passing the supplied arguments to each.
     * @param {ClientContext} ctx The context the event is bound to.
     * @param {string | Symbol} eventName The name of the event.
     * @param  {...any} args The arguments for the listeners.
     * @returns {boolean} Returns true if the event had listeners, false otherwise.
     */
    emit(ctx, eventName, ...args) {
        const emitter = this.getCtxEmitter(ctx);
        if (!emitter)
            return false;
        return emitter.emit(eventName, ...args);
    }

    /**
     * Emit all listeners registered for the given eventName whatever the context.
     * @param {string | Symbol} eventName The name of the event.
     * @param  {...any} args The arguments for the listeners.
     */
    emitForAll(eventName, ...args) {
        this.__emitters.forEach(emitter => {
            emitter.emit(eventName, ...args);
        });
    }
}

module.exports = {
    ContextEventEmitter
}