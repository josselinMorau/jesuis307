const { DataModel } = require('../DataModel.js');
const { ModelAttributes, DataTypes } = require('sequelize');

/**
 * Data model for routines.
 */
class RoutineInfo extends DataModel {

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        guildId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        builtin: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        enabled: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        rule: {
            type: DataTypes.STRING,
            allowNull: false
        },
        message: {
            type: DataTypes.STRING,
            allowNull: true
        },
        messageChannelId: {
            type: DataTypes.STRING,
            allowNull: true
        }
    }

    /**
     * @type {string[]}
     */
    static hooks = ["afterUpdate", "afterCreate", "afterDestroy"];

    /**
     * Create an id for a RoutineInfo.
     * @param {string} guildId The id of the guild.
     * @param {string} routineName The name of the routine.
     */
    static createUniqueId(guildId, routineName) {
        return `${guildId}:${routineName}`;
    }

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} Primary key of the routine. It's a concatenation of the guildId and the name. The separator is ':'.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the guild this routine is attached to.
         */
        this.guildId;
        /**
         * @type {string} The routine's name.
         */
        this.name;
        /**
         * @type {boolean} Whether the routine is a built-in one.
         */
        this.builtin;
        /**
         * @type {boolean} Whether the routine is enabled.
         */
        this.enabled;
        /**
         * @type {string} The scheduling rule of the routine.
         */
        this.rule;
        /**
         * @type {string?} The message that should be posted once the routine event occurs.
         */
        this.message;
        /**
         * @type {string?} The id of the channel where to post the message.
         */
        this.messageChannelId;
    }

}

module.exports = {
    RoutineInfo
}