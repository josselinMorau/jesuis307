const { DataModel, ModelAssociation, ASSOCIATION_TYPE } = require('../DataModel.js');
const { MemberInfo } = require('./MemberInfo.js');
const { RoleInfo } = require('./RoleInfo.js');
const { ModelAttributes, DataTypes } = require('sequelize');

/**
 * Data model for custom command permissions given to specific user or roles.
 */
class CommandPermission extends DataModel
{

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        guildId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        commandName: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }

    /**
     * @type {ModelAssociation[]}
     */
    static modelAssociations = [
    {
        type: ASSOCIATION_TYPE.BELONGS_TO_MANY,
        model: MemberInfo,
        options: {
            through: "MemberCommandPermission"
        }
    },
    {
        type: ASSOCIATION_TYPE.BELONGS_TO_MANY,
        model: RoleInfo,
        options: {
            through: "RoleCommandPermission"
        }
    }];

    /**
     * Creates an id for `CommandPermission`.
     * @param {string} guildId The id of the guild.
     * @param {string} commandName The name of the command.
     */
    static createUniqueId(guildId, commandName) {
        return `${guildId}:${commandName}`;
    }

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The unique id to identify the permission.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the guild this permission exists in.
         */
        this.guildId;
        /**
         * @type {string} The name of the command.
         */
        this.commandName;
        /**
         * @type {MemberInfo[]} The list of authorized members by the permission.
         */
        this.MemberInfos;
        /**
         * @type {RoleInfo[]} The list of authorized roles by the permission.
         */
        this.RoleInfos;
    }

}

module.exports = {
    CommandPermission
}