const { DataModel } = require('../DataModel.js');
const { ModelAttributes, DataTypes } = require('sequelize');

/**
 * Model for custom commands.
 */
class CustomCommandInfo extends DataModel
{

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        guildId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        content: {
            type: DataTypes.STRING,
            allowNull: false
        },
        deleteMsg: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        }
    }

    /**
     * Create an unique id.
     * @param {string} guildId The id of the guild. 
     * @param {string} customCmdName The name of the custom command.
     */
    static createUniqueId(guildId, customCmdName) {
        return `${guildId}:${customCmdName}`;
    }

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The unique id for this command.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the guild the command is set for.
         */
        this.guildId;
        /**
         * @type {string} The name of the custom command.
         */
        this.name;
        /**
         * @type {string} The content that should be posted when invoking the command.
         */
        this.content;
        /**
         * @type {boolean} Whether the command should delete the message that invoked it.
         */
        this.deleteMsg;
    }

}

module.exports = {
    CustomCommandInfo
}