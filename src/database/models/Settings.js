const { DataTypes, ModelAttributes } = require("sequelize");
const { DataModel } = require("../DataModel.js");
const { SETTINGS } = require('../../utils/guildSettings/settings.js');

/**
 * Data model for the settings per guild. 
 */
class Settings extends DataModel {

    /**
     * The fields are overriden according to the values in `SETTINGS`.
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        guildId: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        }
    }
    
    static async initAttributes(sequelize = null) {
        // Override modelAttributes fields before initialization.
        for (let key in SETTINGS) {
            let value = SETTINGS[key];
            this.modelAttributes[key] = {
                type: value.type,
                allowNull: true
            };
        }

        await super.initAttributes(sequelize);
    }
}

module.exports = {
    Settings
}
