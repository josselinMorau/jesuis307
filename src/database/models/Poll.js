const { ModelAttributes, DataTypes } = require('sequelize');
const { DataModel, ModelAssociation, ASSOCIATION_TYPE } = require('../DataModel.js');
const { PollChoice } = require('./PollChoice.js');
const { PollAnswer } = require('./PollAnswer.js');

/**
 * @class Data model for a poll.
 */
class Poll extends DataModel
{
    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        msgId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        guildId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        anonymous: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        allowMultiple: {
            type: DataTypes.BOOLEAN,
            allowNull: false
        },
        question: {
            type: DataTypes.STRING,
            allowNull: false
        },
        author: {
            type: DataTypes.STRING,
            allowNull: true
        }
    };

    /**
     * @type {ModelAssociation[]}
     */
    static modelAssociations = [
        {
            model: PollChoice,
            type: ASSOCIATION_TYPE.HAS_MANY,
            options: {
                foreignKey: "pollMsgId",
                onDelete: "CASCADE" // Make sure to destroy all associated associations on delete.
            }
        },
        {
            model: PollAnswer,
            type: ASSOCIATION_TYPE.HAS_MANY,
            options: {
                foreignKey: "pollMsgId",
                onDelete: "CASCADE" // Make sure to destroy all associated associations on delete.
            }
        }
    ];

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The id of the message linked to this poll.
         */
        this.msgId;
        /**
         * @type {string} The id of the guild where the poll has been created.
         */
        this.guildId;
        /**
         * @type {boolean} Whether this poll is anonymous.
         */
        this.anonymous;
        /**
         * @type {boolean} Whether multiple answers are allowed for this poll.
         */
        this.allowMultiple;
        /**
         * @type {string} The question of the poll.
         */
        this.question;
        /**
         * @type {string} The id of the author of the poll.
         */
        this.author;
        /**
         * @type {PollChoice[]} The choices for this poll.
         */
        this.PollChoices;
        /**
         * @type {PollAnswer[]} The answers given for this choice.
         */
        this.PollAnswers;
    }
}

module.exports = {
    Poll
}