const { DataModel } = require('../DataModel.js');
const { ModelAttributes, DataTypes } = require('sequelize');

/**
 * Data model for user birthdays.
 */
class Birthday extends DataModel {

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        guildId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        userId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        date: {
            type: DataTypes.DATE,
            allowNull: false
        }
    }

    /**
     * Creates a unique id.
     * @param {string} guildId The id of the guild. 
     * @param {string} userId The id of the user.
     * @returns {string}
     */
    static createUniqueId(guildId, userId) {
        return `${guildId}:${userId}`;
    }

    /**
     * @type {string[]}
     */
    static hooks = [];
    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The unique id for this model.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the guild where the birthday has been registered.
         */
        this.guildId;
        /**
         * @type {string} The id of the user whose birthday it is.
         */
        this.userId;
        /**
         * @type {Date} The date of the birthday.
         */
        this.date;
    }

}

module.exports = {
    Birthday
}