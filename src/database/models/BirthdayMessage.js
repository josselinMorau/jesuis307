const { DataModel } = require('../DataModel.js');
const { ModelAttributes, DataTypes } = require('sequelize');

/**
 * Data model for custom messages sent when a birthday happens.
 */
class BirthdayMessage extends DataModel {

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        guildId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        authorId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        content: {
            type: DataTypes.STRING,
            allowNull: false,

        }
    }

    /**
     * @type {string[]}
     */
    static hooks = [];
    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The unique id for this model.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the guild where the message is active.
         */
        this.guildId;
        /**
         * @type {string} The id of the author the message.
         */
        this.authorId;
        /**
         * @type {string} The content of the message. %n in the string will be replaced by the name of whose birthday it is.
         */
        this.content;
    }

}

module.exports = {
    BirthdayMessage
}