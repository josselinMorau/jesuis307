const { DataModel } = require('../DataModel.js');
const { ModelAttributes, DataTypes } = require('sequelize');

/**
 * Model for custom command aliases.
 */
class CustomAliasInfo extends DataModel
{

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        guildId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        aliasName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        commandName: {
            type: DataTypes.STRING,
            allowNull: false
        },
        aliasArgs: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }

    /**
     * Create an unique id.
     * @param {string} guildId The id of the guild. 
     * @param {string} aliasName The name of the alias.
     */
    static createUniqueId(guildId, aliasName) {
        return `${guildId}:${aliasName}`;
    }

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The unique id for this alias.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the guild the alias is set for.
         */
        this.guildId;
        /**
         * @type {string} The name of the alias.
         */
        this.aliasName;
        /**
         * @type {string} The name of the original command.
         */
        this.commandName;
        /**
         * @type {string}
         */
        this.aliasArgs;
    }

}

module.exports = {
    CustomAliasInfo
}