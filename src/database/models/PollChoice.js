const { ModelAttributes, DataTypes } = require('sequelize');
const { DataModel, ModelAssociation, ASSOCIATION_TYPE } = require('../DataModel.js');

/**
 * @class Data model for available choices in a poll.
 */
class PollChoice extends DataModel
{
    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        content: {
            type: DataTypes.STRING,
            allowNull: false
        },
        emoji: {
            type: DataTypes.STRING,
            allowNull: false
        }
    };

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} Unique id for this data.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the poll this choice belongs to.
         */
        this.pollMsgId;
        /**
         * @type {string} The content of the choice.
         */
        this.content;
        /**
         * @type {string} The emoji linked to the choice.
         */
        this.emoji;
    }
}

module.exports = {
    PollChoice
}