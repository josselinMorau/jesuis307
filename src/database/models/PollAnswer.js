const { ModelAttributes, DataTypes } = require('sequelize');
const { DataModel } = require('../DataModel.js');

/**
 * @class Data model for answer given for a choice.
 * This is only used if the corresponding poll is anonymous as non-anonymous ones have reactions to count the answers.
 */
class PollAnswer extends DataModel
{
    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            type: DataTypes.STRING,
            primaryKey: true
        },
        userId: {
            type: DataTypes.STRING,
            allowNull: false
        },
        answer: {
            type: DataTypes.STRING,
            allowNull: false
        }
    };

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The unique id for this model.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the corresponding poll.
         */
        this.pollMsgId;
        /**
         * @type {string} The id of the user who gave the answer.
         */
        this.userId;
        /**
         * @type {string} The value of the answer. (aka the corresponding emoji)
         */
        this.answer;
    }
}

module.exports = {
    PollAnswer
}