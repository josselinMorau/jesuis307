const { DataModel } = require('../DataModel.js');
const { isValidCategory, CHANNEL_CATEGORIES } = require('../../utils/channels/channelUtils.js');
const { ModelAttributes, DataTypes } = require('sequelize');

const ERROR_MESSAGES = {
    SUCCESS: "Success",
    INVALID_CATEGORY: "Invalid category name.",
    ALREADY_PRESENT_CATEGORY: "The given category is already present."
}

/**
 * Data model for infos on a channel.
 */
class ChannelInfo extends DataModel
{

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        channelId: {
            primaryKey: true,
            type: DataTypes.STRING
        },
        guildId: {
            allowNull: false,
            type: DataTypes.STRING
        },
        categories: {
            allowNull: true,
            type: DataTypes.STRING
        },
        description: {
            allowNull: true,
            type: DataTypes.STRING
        }
    }

    static hooks = [ "afterSave", "beforeSave", "afterDestroy" ];

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The id of the channel.
         */
        this.channelId;
        /**
         * @type {string} The id of the channel's guild.
         */
        this.guildId;
        /**
         * @type {string} A concatenation of all the category names. The names are separated by '+'.
         */
        this.categories;
        /**
         * @type {string} The description of the channel.
         */
        this.description;
    }

    /**
     * Add a category to the current instance. Do nothing if the category is invalid or already present.
     * @param {String} category The category name. It should be a value contained in `CHANNEL_CATEGORIES`.
     * @returns {[boolean, String]} A [inserted, errorMessage] pair.
     */
    addCategory(category) {
        // Vérifie la validité de la catégorie.
        if (!isValidCategory(category)) {
            return [false, ERROR_MESSAGES.INVALID_CATEGORY];
        }

        // Initialise le champ si null.
        if (!this.categories) {
           this.categories = ""; 
        }

        const categoryList = this.getCategories();
        if (categoryList.includes(category)) {
            return [false, ERROR_MESSAGES.ALREADY_PRESENT_CATEGORY];
        }

        categoryList.push(category);
        this.categories = categoryList.join('+');
        return [true, ERROR_MESSAGES.SUCCESS];
    }

    /**
     * Remove a category to the current instance.
     * @param {String} category The category name. It should be a value contained in `CHANNEL_CATEGORIES`.
     * @return {boolean} Returns true if the category was present.
     */
    removeCategory(category) {
        if (!this.categories) {
            return false;
        }

        const categoryList = this.getCategories();
        const index = categoryList.indexOf(category);
        if (index != -1) {
            categoryList.splice(index, 1);
            this.categories = categoryList.join('+');
            return true;
        }
        return false;
    }

    /**
     * Return an array of the categories present inside the ChannelInfo.
     * @returns {CHANNEL_CATEGORIES[]}
     */
    getCategories() {
        return this.categories ? this.categories.split('+').filter(str => !!str) : [];
    }

    /**
     * Returns whether the ChannelInfo has the given category.
     * @param {CHANNEL_CATEGORIES} category 
     * @returns {boolean}
     */
    hasCategory(category) {
        return this.getCategories().includes(category);
    }
}

module.exports = {
    ChannelInfo
}