const { DataModel } = require('../DataModel.js');
const { ModelAttributes, DataTypes } = require('sequelize');

/**
 * Data model to register info on a jailed member.
 */
class JailInfo extends DataModel
{

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            primaryKey: true,
            type: DataTypes.STRING
        },
        guildId : {
            allowNull: false,
            type: DataTypes.STRING
        },
        userId: {
            allowNull: false,
            type: DataTypes.STRING
        },
        jailedAt: {
            type: DataTypes.DATE
        },
        duration: {
            type: DataTypes.FLOAT
        },
        originalRoles: {
            type: DataTypes.STRING
        },
        channelId: {
            type: DataTypes.STRING
        }
    }

    /**
     * Creates an unique id from the given guildId and userId.
     * @param {string} guildId 
     * @param {string} userId 
     */
    static createUniqueId(guildId, userId) {
        return `${guildId}:${userId}`;
    }

    /**
     * Returns an array of the original role ids.
     * @returns {string[]}
     */
    getOriginalRoles() {
        return this.originalRoles ? this.originalRoles.split('+') : [];
    }

    /**
     * Set the original role ids.
     * @param {string[]} roleIds 
     */
    setOriginalRoles(roleIds) {
        this.originalRoles = roleIds.join('+');
    }

    /**
     * Returns the date at which the user should be released.
     * @returns {Date}
     */
    getReleaseDate() {
        return this.jailedAt && this.duration ? new Date(this.jailedAt.getTime() + this.duration) : new Date();
    }

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string} The unique id of the info.
         */
        this.uniqueId;
        /**
         * @type {string} The id of the guild where the user has been jailed.
         */
        this.guildId;
        /**
         * @type {string} The id of the user that has been jailed.
         */
        this.userId;
        /**
         * @type {Date} The date at which the user has been jailed.
         */
        this.jailedAt;
        /**
         * @type {number} The amount of time (in ms) the user has to stay in jail.
         */
        this.duration;
        /**
         * @type {string} Concatenation of the user original roles.
         */
        this.originalRoles;
        /**
         * @type {string} The id of the channel from which the user has been sent to jail.
         */
        this.channelId;
    }

}

module.exports = {
    JailInfo
}