const { DataModel } = require('../DataModel.js');
const { ModelAttributes, DataTypes } = require('sequelize');

/**
 * Modèle pour les données autour d'un rôle.
 */
class RoleInfo extends DataModel
{

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        roleId: { // L'id du rôle.
            type: DataTypes.STRING,
            primaryKey: true
        },
        guildId: { // L'id du serveur.
            type: DataTypes.STRING,
            allowNull: false
        },
        roleName: { // Le nom du rôle.
            type: DataTypes.STRING,
            allowNull: true
        }
    }

    /**
     * @inheritdoc
     */
     defines() {
        /**
         * @type {string}
         */
        this.roleId;
        /**
         * @type {string}
         */
        this.guildId;
        /**
         * @type {string?}
         */
        this.roleName;
    }

}

module.exports = {
    RoleInfo
}