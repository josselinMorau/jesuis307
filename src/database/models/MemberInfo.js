const { DataModel } = require('../DataModel.js');
const { ModelAttributes, DataTypes } = require('sequelize');
const { GuildMember } = require('discord.js');

/**
 * Data model for the infos concerning guild members.
 */
class MemberInfo extends DataModel
{

    /**
     * @type {ModelAttributes}
     */
    static modelAttributes = {
        uniqueId: {
            primaryKey: true,
            type: DataTypes.STRING
        },
        guildId: {
            allowNull: false,
            type: DataTypes.STRING
        },
        userId: {
            allowNull: false,
            type: DataTypes.STRING
        },
        named: {
            type: DataTypes.BOOLEAN
        },
        fullyIntroduced: {
            type: DataTypes.BOOLEAN
        },
        properName: {
            type: DataTypes.STRING
        }
    }

    /**
     * Creates an unique id from the guild id and the member id.
     * @param {string} guildId
     * @param {string} memberId
     */
    static createUniqueId(guildId, memberId) {
        return `${guildId}:${memberId}`;
    }

    /**
     * Fetch the MemberInfo of a member based on its ids.
     * @param {string} guildId
     * @param {string} memberId
     */
    static async findByIds(guildId, memberId) {
        return await this.findByPk(this.createUniqueId(guildId, memberId));
    }

    /**
     * Create a dummy MemberInfo object from the given member.
     * @param {GuildMember} member
     */
    static constructFromMember(member) {
        return {
            uniqueId: this.createUniqueId(member.guild.id, member.id),
            guildId: member.guild.id,
            userId: member.id,
            named: true,
            fullyIntroduced: true,
            properName: member.displayName
        };
    }

    /**
     * @inheritdoc
     */
    defines() {
        /**
         * @type {string}
         */
        this.uniqueId;
        /**
         * @type {string} The id of the guild.
         */
        this.guildId;
        /**
         * @type {string} The id of the member.
         */
        this.userId;
        /**
         * @type {boolean?} Whether the member has given their name.
         */
        this.named;
        /**
         * @type {boolean?} Whether the member has given their name and their year.
         */
        this.fullyIntroduced;
        /**
         * @type {string?} The registered name for the member.
         */
        this.properName;
    }

}

module.exports = {
    MemberInfo
}