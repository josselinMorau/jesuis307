const fs = require('fs');
const { Sequelize } = require("sequelize");
const { DataModel } = require("./DataModel.js");
const { isSubclassOf } = require('../utils/helper.js');
const { logger } = require('../logging/logging.js');

const DATABASE_URI = process.env.ATILLA_BOT_DATABASE_URI;

/**
 * Initialize the database and all of its models.
 */
async function initDatabase() {
    try {
        // Important : Set charset to "utf8mb4" to support emoji characters.
        DataModel.commonSequelize = new Sequelize(DATABASE_URI, { logging: () => {}, dialectOptions: { charset: 'utf8mb4' } });
    
        const modelFiles = fs.readdirSync('./src/database/models')
            .filter(file => file.endsWith('.js'));
        
        /**
         * @type {(typeof DataModel)[]}
         */
        const models = [];

        // Fetch all models.
        for (let file of modelFiles) {
            const modelModule = require(`./models/${file}`);
            if (isSubclassOf(modelModule, DataModel)) {
                models.push(modelModule);
            }
            else {
                for (let key in modelModule) {
                    const subModule = modelModule[key];
                    if (isSubclassOf(subModule, DataModel)) {
                        models.push(subModule);
                    }
                }
            }
        }

        // First define attributes.
        for (let model of models) {
            await model.initAttributes();
        }

        // Then associations.
        for (let model of models) {
            model.createAssociations();
        }

        // Finally sync all the models of the database.
        await DataModel.commonSequelize.sync({ alter: true });
    }
    catch (err)
    {
        logger.error("An error occured while trying to get access to the database : ", err);
        throw err; // If something is broken with the database, don't cover it and throws.
    }    
}

module.exports = {
    initDatabase
}