const EventEmitter = require('events');
const { Model, ModelAttributes, Sequelize } = require('sequelize');
const { logger } = require('../logging/logging.js');
const { ContextEventEmitter } = require('../utils/ContextEventEmitter.js');

/**
 * @typedef {import('../ClientContext').ClientContext} ClientContext
 */

/**
 * @enum {string} Enumeration of each type of association.
 */
const ASSOCIATION_TYPE = {
    HAS_ONE: "hasOne",
    HAS_MANY: "hasMany",
    BELONGS_TO: "habelongsTo",
    BELONGS_TO_MANY: "belongsToMany",
}

/**
 * @class The prototype of a model association with another.
 */
class ModelAssociation {

    constructor(type, model, options = undefined) {
        /**
         * @type {ASSOCIATION_TYPE} The type of association.
         */
        this.type = type;
        /**
         * @type {typeof DataModel} The type of DataModel to associate to.
         */
        this.model = model;
        /**
         * @type {import('sequelize').BelongsToOptions | import('sequelize').BelongsToManyOptions | import('sequelize').HasOneOptions | import('sequelize').HasManyOptions}
         * The options of the association.
         * @see https://sequelize.org/master/class/lib/model.js~Model.html#static-method-belongsTo
         * @see https://sequelize.org/master/class/lib/model.js~Model.html#static-method-belongsToMany
         * @see https://sequelize.org/master/class/lib/model.js~Model.html#static-method-hasOne
         * @see https://sequelize.org/master/class/lib/model.js~Model.html#static-method-hasMany
         */
        this.options = options;
    }

}

/**
 * Base class for all models in the database.
 */
class DataModel extends Model
{
    /**
     * Override this object to define the fields of your model.
     * @type {ModelAttributes}
     */
    static modelAttributes = {};

    /**
     * Override this object to define the different assocations of your model.
     * @type {ModelAssociation[]}
     */
    static modelAssociations = [];

    /**
     * Common instance of sequelize for all models. Don't override this object in sub-classes.
     * @type {Sequelize} 
     */
    static commonSequelize = null;

    /**
     * A list of the event names that the model should listen to.
     * Override this list to define the events of the model.
     * For more infos on the available events : https://sequelize.org/master/manual/hooks.html#available-hooks
     * @type {string[]}
     */
    static hooks = [];

    /**
     * A map of `ContextEventEmitter` for each model. Each emitter sends events defined in the `hooks` properties.
     * Don't override this field.
     * @type {Map<string, ContextEventEmitter>}
     */
    static emitters = new Map();

    /**
     * Returns the current class name.
     * You only have to override this function if you override the static `name` field of the sub-class.
     * @returns {string}
     */
    static getClassName() {
        return this.name;
    }

    /**
     * Initialize the model attributes and hooks.
     * @param {Sequelize} sequelize The instance of sequelize.
     */
    static async initAttributes(sequelize = null) {
        const hooks = {};
        const emitter = this.getCtxEventEmitter();
        this.hooks.forEach(hook => {
            hooks[hook] = (...params) => { emitter.emitForAll(hook, ...params); };
        });
        // Important : Set charset to "utf8mb4" to support emoji characters.
        super.init(this.modelAttributes, { sequelize: (sequelize || DataModel.commonSequelize), hooks: hooks, charset: "utf8mb4" });
    }

    /**
     * Create all associations of the model.
     * This should be called once `initAttributes` has been called.
     */
    static createAssociations() {
        for (let association of this.modelAssociations) {
            switch (association.type) {
                case ASSOCIATION_TYPE.BELONGS_TO:
                    this.belongsTo(association.model, association.options);
                    break;
                case ASSOCIATION_TYPE.BELONGS_TO_MANY:
                    this.belongsToMany(association.model, association.options);
                    break;
                case ASSOCIATION_TYPE.HAS_ONE:
                    this.hasOne(association.model, association.options);
                    break;
                case ASSOCIATION_TYPE.HAS_MANY:
                    this.hasMany(association.model, association.options);
                    break;
            }
        }
    }

    /**
     * Find one random element in the database.
     * @param {import('sequelize').NonNullFindOptions} options Search options.
     */
    static async findOneRandom(options = {}) {
        options.order = this.commonSequelize.random();
        return await this.findOne(options);
    }

    /**
     * Returns the emitter for the current model class.
     * @returns {ContextEventEmitter}
     */
    static getCtxEventEmitter() {
        // Lazy instanciation.
        var emitter = this.emitters.get(this.getClassName());
        if (!emitter) {
            emitter = new ContextEventEmitter();
            this.emitters.set(this.getClassName(), emitter);
        }
        return emitter;
    }

    /**
     * Clear all events of the current model for the given context.
     * @param {ClientContext} ctx 
     */
    static clearEvents(ctx) {
        const emitter = this.getCtxEventEmitter();
        emitter.clear(ctx);
    }

    /**
     * Clear all events for every model for the given context.
     * @param {ClientContext} ctx 
     */
    static clearAllModelEvents(ctx) {
        this.emitters.forEach(emitter => {
            emitter.clear(ctx);
        });
    }

    /**
     * Register a callback for the given event name.
     * @param {ClientContext} ctx The context the event is bound to.
     * @param {string} eventName The name of the event to listen to.
     * @param {(...args : any[]) => void} listener The callback to register.
     */
    static on(ctx, eventName, listener) {
        const emitter = this.getCtxEventEmitter();
        emitter.on(ctx, eventName, listener);
    }

    /**
     * Unregister a callback for the given event name.
     * @param {ClientContext} ctx The context the event is bound to.
     * @param {string} eventName The name of the event.
     * @param {(...args : any[]) => void} listener The callback to unregister.
     */
    static off(ctx, eventName, listener) {
        const emitter = this.getCtxEventEmitter();
        emitter.off(ctx, eventName, listener);
    }

    /**
     * Returns a proxy object of the current instance.
     * @returns {object}
     */
    createProxy() {
        const proxy = {};
        for (let key in this.constructor.modelAttributes) {
            proxy[key] = this[key];
        }
        return proxy;
    }

    /**
     * Dummy function only to document member variables.
     */
    defines(){}
}

module.exports = {
    DataModel,
    ModelAssociation,
    ASSOCIATION_TYPE
}