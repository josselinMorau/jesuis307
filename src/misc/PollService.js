const { MessageReaction, User, Message, TextChannel, MessageEmbed, Guild, MessageActionRow, MessageSelectMenu, SelectMenuInteraction, GuildMember } = require("discord.js");
const { BaseService } = require("../BaseService");
const { Poll } = require("../database/models/Poll");
const { PollAnswer } = require("../database/models/PollAnswer");
const { PollChoice } = require("../database/models/PollChoice");
const { logger } = require("../logging/logging");
const { findMessageInGuild, CONSTANTS, uuid, createEmbedsFromData } = require("../utils/helper");
const { WidgetListener } = require("../utils/widgets/WidgetListener");

const EMOJI_ORDER = ['👍', '👎', '🤷', '🇦','🇧','🇨','🇩','🇪','🇫','🇬','🇭','🇮','🇯','🇰','🇱','🇲','🇳','🇴','🇵','🇶','🇷','🇸','🇹','🇺','🇻','🇼','🇽','🇾','🇿'];
const POLL_MENU_CUSTOM_ID = "poll";
/**
 * @enum {string}
 */
const POLL_ERRORS = {
    NO_SUCH_POLL: "NO_SUCH_POLL"
}

/**
 * @class A proxy to the original model.
 */
class PollProxy {

    /**
     * 
     * @param {Poll} poll 
     */
    constructor(poll) {
        this.__pollId = poll.msgId;
        this.anonymous = poll.anonymous;
        this.allowMultiple = poll.allowMultiple;
        this.choiceEmojis = poll.PollChoices ? poll.PollChoices.map(choice => choice.emoji) : [];
        /**
         * @type {Message}
         */
        this.__cachedMsg = null;
    }

    /**
     * @returns {Promise<Poll>}
     */
    async getPoll() {
        return (await Poll.findByPk(this.__pollId, { include: [{ model: PollAnswer, model: PollChoice }] }).catch(() => {})) || null;
    }

    /**
     * @param {Guild} guild
     * @returns {Promise<Message>}
     */
    async getMessage(guild) {
        this.__cachedMsg = await (!!this.__cachedMsg ? this.__cachedMsg.fetch() : findMessageInGuild(guild, this.__pollId));
        return this.__cachedMsg;
    }
}

/**
 * @class Service to handle polls across the different guilds.
 */
class PollService extends BaseService
{
    constructor(ctx) {
        super(ctx);

        /**
         * @type {Map<string, PollProxy>}
         */
        this.__polls = new Map();

        // Register to events.
        this.clientContext.client.on('messageReactionAdd', this.onMessageReactionAdd.bind(this));
        this.clientContext.client.on('messageDelete', this.onMessageDelete.bind(this));
    }

    /**
     * @inheritdoc
     */
    async init() {
        // Register to select menus interactions.
        const widgetListener = this.clientContext.getService(WidgetListener);
        widgetListener.registerSelectMenuCallback((interaction) => {
            this.onSelectMenuInteraction(interaction).catch(err => logger.error(err)); 
        });

        // Init cache for each guild.
        for (let guild of this.clientContext.client.guilds.cache.values()) {
            const polls = await this.findAllPolls(guild.id);
            for (let poll of polls) {
                // Check if the related message still exists.
                let msg = await findMessageInGuild(guild, poll.msgId);
                if (!msg) {
                    // If not delete the corresponding poll.
                    poll.destroy().catch(err => logger.error(err));
                    continue;
                }
                // Cache the corresponding poll.
                const proxy = new PollProxy(poll);
                this.__polls.set(poll.msgId, proxy);

                // Get rid of unwanted reactions
                const reactions = [...msg.reactions.cache.values()];
                const removePromises = [];
                for (let reaction of reactions) {
                    if (!proxy.choiceEmojis.includes(reaction.emoji.name))
                        removePromises.push(
                            reaction.remove()
                        );
                }
                await Promise.all(removePromises);

                // Refetch the current message object.
                msg = await msg.fetch();

                if (!poll.allowMultiple && !poll.anonymous) {
                    // Ensure there's no duplicates
                    const userIdSet = new Set();
                    const duplicateIdSet = new Set();
                    for (let reaction of msg.reactions.cache.values()) {
                        const users = await reaction.users.fetch();
                        for (let user of users.values()) {
                            if (user.bot)
                                continue; // Ignore bot's reaction.
                            if (userIdSet.has(user.id))
                                duplicateIdSet.add(user.id);
                            else
                                userIdSet.add(user.id);
                        }
                    }

                    // Since we don't know which answer is the last one, we just remove all duplicates.
                    if (duplicateIdSet.size > 0) {
                        const promises = [];
                        for (let reaction of msg.reactions.cache.values()) {
                            for (let userId of duplicateIdSet.values()) {
                                promises.push(
                                    reaction.users.remove(userId)
                                );
                            }
                        }
                        await Promise.all(promises);
                    }
                }
            }
        }
    }

    /**
     * Callback for menu interactions.
     * @param {SelectMenuInteraction} interaction 
     */
    async onSelectMenuInteraction(interaction) {
        if (interaction.customId !== POLL_MENU_CUSTOM_ID)
            return;

        const pollProxy = this.__polls.get(interaction.message.id);
        if (!pollProxy)
            return;
        
        if (!pollProxy.allowMultiple) {
            // Just update the current answer's value.
            const [pollAnswer, created] = await PollAnswer.findOrCreate({ 
                where: { pollMsgId: pollProxy.__pollId, userId: interaction.user.id },
                defaults: { 
                    uniqueId: uuid(), pollMsgId: pollProxy.__pollId,
                    userId: interaction.user.id, answer: interaction.values.at(0) 
                }
            });
            if (!created) { // Update the answer value if it was not created.
                pollAnswer.answer = interaction.values.at(0);
                await pollAnswer.save();
            }
        }
        else {
            const valueSet = new Set(interaction.values);
            const currentPollAnswers = await PollAnswer.findAll({ where: { pollMsgId: pollProxy.__pollId, userId: interaction.user.id } });
            const promises = [];
            for (let pollAnswer of currentPollAnswers) {
                // If the answer is still there, ignore it.
                if (valueSet.has(pollAnswer.answer)) {
                    valueSet.delete(pollAnswer.answer);
                    continue;
                }

                // Else update the answer with one in the set.
                if (valueSet.size > 0) {
                    pollAnswer.answer = valueSet.values().next().value;
                    valueSet.delete(pollAnswer.answer);
                    promises.push(
                        pollAnswer.save()
                    );
                    continue;
                }

                // If there's no values in the set destroy the answer
                promises.push(
                    pollAnswer.destroy()
                );
            }

            // If the set is not empty, that means there's still values to upsert so we create them.
            for (let value of valueSet.values()) {
                promises.push(
                    PollAnswer.create({
                        uniqueId: uuid(),
                        pollMsgId: pollProxy.__pollId,
                        userId: interaction.user.id,
                        answer: value
                    })
                );
            }

            await Promise.all(promises);
        }

        // Finally responds to the interaction
        await interaction.reply({ 
            content: "Ton vote a bien été pris en compte.\nSi tu avais déjà voté, ton ancien vote a été remplacé par le nouveau.", 
            ephemeral: true 
        });
    }

    /**
     * Callback for 'messageDelete' event.
     * @param {Message} message 
     */
    onMessageDelete(message) {
        const pollProxy = this.__polls.get(message.id);
        if (pollProxy) {
            pollProxy.getPoll().then(poll => {
                poll.destroy().catch(err => logger.error(err));
            });
            this.__polls.delete(message.id);
        }
    }

    /**
     * Callback for 'messageReactionAdd' event.
     * @param {MessageReaction} reaction 
     * @param {User} user
     */
    onMessageReactionAdd(reaction, user) {
        const pollProxy = this.__polls.get(reaction.message.id);

        // Ignore reaction if it's not on a poll or if it's an anonymous poll (no reactions).
        if (!pollProxy || pollProxy.anonymous || user.bot)
            return;
        
        // If it's not among the expected emojis, remove it.
        if (!pollProxy.choiceEmojis.includes(reaction.emoji.name)) {
            reaction.remove().catch(err => logger.error(err));
            return;
        }

        // If we allow multiple answers, it's useless to check duplicates.
        if (pollProxy.allowMultiple)
            return;
        
        reaction.message.reactions.cache.forEach(rct => {
            // Ignore the added one.
            if (rct.emoji.name === reaction.emoji.name)
                return;
            
            // Remove the previous answer of the user.
            if (rct.users.cache.has(user.id)) {
                rct.users.remove(user).catch(err => logger.error(err));
            }
        });
    }

    /**
     * Creates a poll.
     * @param {TextChannel} channel The channel where to create the poll.
     * @param {object} createOptions Options to create the poll.
     * @param {string} createOptions.question The question of the poll.
     * @param {boolean} createOptions.anonymous Whether the poll is an anonymous one.
     * @param {boolean} createOptions.allowMultiple Whether the poll allows multiple answers per user.
     * @param {GuildMember} createOptions.author The id of the author of the poll.
     * @param {{ emoji: string, content: string }[]} createOptions.choices The choices of the poll.
     */
    async createPoll(channel, createOptions) {
        const choiceContent = createOptions.choices
            .map(choice => `${choice.emoji} - ${choice.content}`)
            .join('\n');
        const embed = new MessageEmbed()
            .setTitle(createOptions.question)
            .setDescription(choiceContent)
            .setColor(CONSTANTS.COLOR_INFO)
            .setFooter(`Créé par : ${createOptions.author.displayName}`);
        const components = [];
        
        if (createOptions.anonymous) {
            // Add select menu.
            const choiceOptions = createOptions.choices.map(choice => { 
                return { 
                    label: choice.content,
                    emoji: choice.emoji,
                    value: choice.emoji };
                });
            
            const selectMenu = new MessageSelectMenu()
                .setCustomId(POLL_MENU_CUSTOM_ID)
                .setMinValues(1)
                .setMaxValues(createOptions.allowMultiple ? createOptions.choices.length : 1)
                .addOptions(choiceOptions);
                        
            const row = new MessageActionRow({ components: [selectMenu] });
            components.push(row);
        }

        const pollMsg = await channel.send({ embeds: [embed], components: components });
        const poll = await Poll.create({ 
            msgId: pollMsg.id,
            guildId: channel.guildId,
            anonymous: !!createOptions.anonymous,
            allowMultiple: !!createOptions.allowMultiple,
            question: createOptions.question,
            author: createOptions.author.id
        }, { include: [
            { model: PollAnswer },
            { model: PollChoice }
        ]});

        const pollChoices = [];
        for (let choice of createOptions.choices) {
            const pollChoice = await PollChoice.create({
                uniqueId: uuid(),
                content: choice.content,
                emoji: choice.emoji
            });
            pollChoices.push(pollChoice);
        }
        await poll.addPollChoices(pollChoices);
        poll.PollChoices = pollChoices; // Make sure to load the array of choices into the data.
        this.__polls.set(pollMsg.id, new PollProxy(poll));

        // Add reactions
        if (!createOptions.anonymous) {
            const promises = [];
            for (let choice of createOptions.choices) {
                promises.push(
                    pollMsg.react(choice.emoji).catch(err => logger.error(err))
                );
            }
            await Promise.all(promises);
        }
    }

    /**
     * Returns the poll with the given id.
     * @param {string} pollId The id of the poll's message.
     * @returns {Promise<Poll?>} Returns `null` if not found.
     */
    async fetchPoll(pollId) {
        const pollProxy = this.__polls.get(pollId);
        if (!pollProxy)
            return null;
        return await pollProxy.getPoll();
    }

    /**
     * 
     * @param {TextChannel} channel The channel where to close the poll.
     * @param {string} pollId The id of the poll's message.
     */
    async closePoll(channel, pollId) {
        const pollProxy = this.__polls.get(pollId);
        if (!pollProxy) {
            throw POLL_ERRORS.NO_SUCH_POLL;
        }

        const pollMsg = await pollProxy.getMessage(channel.guild);
        
        // Disable select menu
        if (pollProxy.anonymous) {
            const rows = pollMsg.components;
            rows.forEach(row => {
                row.components.forEach(comp => {
                    comp.setDisabled(true);
                });
            })
            await pollMsg.edit({ components: rows });
        }
        
        const poll = await pollProxy.getPoll();
        const pollResults = await this.fetchResults(channel.guild, poll, pollProxy);

        const BAR_SIZE = 10;
        const WINNER_SQUARE = '🟦';
        const LOSER_SQUARE = '⬜';
        const maxCount = Math.max(...Object.values(pollResults.results).filter(r => isFinite(r)));
        const sortedChoices = poll.PollChoices.sort((a,b) => EMOJI_ORDER.indexOf(a.emoji) - EMOJI_ORDER.indexOf(b.emoji));
        const embeds = createEmbedsFromData(
            { title: poll.question, color: CONSTANTS.COLOR_INFO, url: pollMsg.url, footer: { text: `Nombres de votants: ${pollResults.userCount}` } }, sortedChoices,
            (embed, pollChoice) => {
                const count = pollResults.results[pollChoice.emoji] || 0;
                const percentage = pollResults.count ? count / pollResults.count : 0;
                const squareCount = Math.floor((percentage) * BAR_SIZE);
                const square = count >= maxCount ? WINNER_SQUARE : LOSER_SQUARE;
                const bar = square.repeat(squareCount);
                embed.addField(`${pollChoice.emoji} - ${pollChoice.content}`, `${bar} ${Math.floor(percentage * 10000) / 100} %`);
            }
        );

        await channel.send({ embeds: embeds });
        await poll.destroy();
        this.__polls.delete(pollId);
    }

    /**
     * @typedef {object} PollResults
     * @property {number} count The number of answers given.
     * @property {number} userCount The number of users who gave an answer.
     * @property {Object<string, number>} results The amount of answers for each choice. Index is the emoji of the choice.
     */

    /**
     * Returns the results of the given poll.
     * @param {Guild} guild The guild where the poll has been created.
     * @param {Poll} poll The poll to fetch the results of.
     * @param {PollProxy?} proxy The proxy corresponding to the given poll.
     * @returns {Promise<PollResults>}
     */
    async fetchResults(guild, poll, proxy = null) {
        if (!proxy) {
            proxy = this.__polls.get(poll.msgId);
        }

        var count = 0;
        const results = {};
        const userIdSet = new Set();
        // Initialize results.
        const choiceEmojis = poll.PollChoices.map(choice => choice.emoji);
        for (let emoji of choiceEmojis) {
            results[emoji] = 0;
        }

        if (poll.anonymous) {
            const pollAnswers = await PollAnswer.findAll({ where: { pollMsgId: poll.msgId } });
            count = pollAnswers.length;
            for (let pollAnswer of pollAnswers) {
                results[pollAnswer.answer] += 1;
                userIdSet.add(pollAnswer.userId);
            }
        }
        else {
            const pollMsg = await proxy.getMessage(guild);
            for (let emoji of choiceEmojis) {
                const reaction = pollMsg.reactions.cache.get(emoji);
                if (!reaction)
                    continue;
                var reactionCount = 0;
                const users = await reaction.users.fetch();
                for (let user of users.values()) {
                    if (!user.bot) { // Don't count the bot's reactions.
                        ++reactionCount;
                        userIdSet.add(user.id);
                    }
                }
                count += reactionCount;
                results[emoji] = reactionCount;
            }
        }

        return {
            count: count,
            userCount: userIdSet.size,
            results: results
        };
    }

    /**
     * Find all polls for a given guild.
     * @param {string} guildId The id of the guild.
     * @returns {Promise<Poll[]>}
     */
    async findAllPolls(guildId) {
        return await Poll.findAll({ 
            where: { guildId: guildId },
            include: [
                { model: PollAnswer },
                { model: PollChoice }
            ]
        });
    }
}

module.exports = {
    PollService,
    POLL_ERRORS
}
