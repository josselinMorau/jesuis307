const { Guild, VoiceChannel, TextChannel, ButtonInteraction, Message, MessageEmbed, GuildMember, MessageActionRow, MessageButton } = require('discord.js');
const { AudioPlayer, AudioResource, VoiceConnection, getVoiceConnection, VoiceConnectionStatus, joinVoiceChannel, createAudioResource, AudioPlayerStatus, createAudioPlayer } = require('@discordjs/voice');
const playdl = require('play-dl');

const { BaseService } = require("../BaseService");
const { logger } = require('../logging/logging');
const Queue = require('../utils/Queue.js');
const { WidgetListener } = require('../utils/widgets/WidgetListener');
const { CONSTANTS, setMessageComponenentsEnabled } = require('../utils/helper');

/**
 * The amount of time (in seconds) remains inactive before disconnecting from a voice channel.
 */
const DISCONNECT_TIMEOUT = 120;

/**
 * The number of songs displayed on each page of a queue embed.
 */
const SONGS_PER_PAGE = 10;

/**
 * @enum {string} Errors thrown by the MusicService.
 */
const MUSIC_ERRORS = {
    NO_ERROR: "NO_ERROR",
    UNJOINABLE_VOICE_CHANNEL: "UNJOINABLE_VOICE_CHANNEL",
    WRONG_VOICE_CHANNEL: "WRONG_VOICE_CHANNEL",
    INVALID_URL: "INVALID_URL",
    NO_RESULT: "NO_RESULT",
    NO_CONNECTION: "NO_CONNECTION",
    WRONG_STATUS: "WRONG_STATUS",
    OPERATION_FAILED: "OPERATION_FAILED"
};

/**
 * Returns whether the given connection is valid.
 * @param {VoiceConnection} connection  
 */
function isConnected(connection) {
    return !!connection && connection.state.status === VoiceConnectionStatus.Ready;
}

/**
 * Parse the duration in seconds from a string.
 * @param {string} durationStr 
 * @returns {number?}
 */
function parseDuration(durationStr) {
    if (!durationStr)
        return null;
    const split = durationStr.split(':').map(str => parseInt(str)).reverse();
    var toSeconds = 1;
    var duration = 0;
    for (let segment of split) {
        if (isNaN(segment))
            return null;
        duration += segment * toSeconds;
        toSeconds *= 60;
    }
    return duration;
}

/**
 * Returns the duration in seconds into a formated string.
 * @param {number} duration 
 * @returns {string}
 */
function durationToString(duration) {
    var split = [];
    while (duration >= 1) {
        const segment = Math.abs(duration % 60);
        split.push(`${segment < 10 ? '0' : ''}${segment}`);
        duration -= segment;
        duration /= 60;
    }
    while (split.length < 2) {
        split.push("00");
    }
    return split.reverse().join(':');
}

/**
 * 
 * @param {playdl.YouTubeVideo} ytbVideo 
 * @returns {SongInfo} 
 */
function getSongInfo(ytbVideo) {
    const bestThumbnail = ytbVideo.thumbnails ? ytbVideo.thumbnails.reduce((best, current) => {
        if (!best)
            return current;
        if (current.height * current.width > best.height * best.height)
            return current;
        return best;
    }, null) : null;
    return {
        url: ytbVideo.url,
        title: ytbVideo.title,
        author: ytbVideo.channel ? ytbVideo.channel.name : null,
        thumbnail: bestThumbnail ? bestThumbnail.url : null,
        length: ytbVideo.durationInSec
    };
}

/**
 * Returns the infos of the song with the given url.
 * @param {string} url The url of the youtube video.
 * @returns {Promise<SongInfo>} 
 */
 async function getSongInfoFromUrl(url) {
    const info = await playdl.video_info(url, { language: 'fr' }).catch(err => logger.error(err));
    const details = info ? info.video_details : null;
    if (!details)
        return null;
    return getSongInfo(details);
}

/**
 * Returns the infos of the 1st result of a youtube query.
 * @param {string} query The search query.
 * @returns {Promise<SongInfo>} 
 */
async function getSongInfoFromQuery(query) {
    const results = await playdl.search(query, { source: { youtube: "video" }, language: "fr", limit: 1 })
        .catch(err => logger.error(err));
    const item = results ? results.at(0) : null;
    if (!item)
        return null;
    return getSongInfo(item);
}

/**
 * Returns true if the given url is a valid youtube url.
 * @param {string} url 
 * @returns {boolean}
 */
function validateYtUrl(url) {
    return url.startsWith('http') && playdl.yt_validate(url) === "video";
}

/**
 * @typedef {object} SongInfo Struct containing infos on a song pushed into a queue.
 * @property {string} url The url of the youtube video.
 * @property {string} title The title of the song.
 * @property {string} author The name of the author.
 * @property {string} thumbnail The url of the youtube video thumbnail.
 * @property {number} length The length of the song in seconds.
 * @property {string} requester The id of the user who requested the song.
 */

/**
 * @typedef {object} MusicSettings Struct containing settings for a MusicState object.
 * @property {boolean} loop Whether the queue is in loop mode.
 * @property {boolean} loopCurrent Whether the current song should loop.
 * @property {number} page The index of the current page displayed on the queue.
 */

/**
 * @class Wrapper for connection state and infos on queued musics for each guild.
 */
class MusicState
{
    /**
     * Constructs a new MusicState object.
     * @param {MusicService} service The MusicService that spawned this object.
     * @param {string} guildId The id of the guild this state corresponds to.
     */
    constructor(service, guildId) {
        /**
         * @type {MusicService} The MusicService that spawned this object.
         */
        this.service = service;
        /**
         * @type {string}
         */
        this.guildId = guildId;
        /**
         * @type {Queue<SongInfo>}
         */
        this.queue = new Queue();
        /**
         * @type {AudioPlayer?}
         */
        this.__currentPlayer = null;
        /**
         * @type {AudioResource?}
         */
        this.__currentResource = null;
        /**
         * @type {SongInfo?}
         */
        this.currentSongInfo = null;
        /**
         * @type {Message?} The current message displaying the queue.
         */
        this.queueMsg = null;
        /**
         * @type {Message?} The current message displaying the dashboard.
         */
        this.dashboardMsg = null;
        /**
         * @type {MusicSettings} The current settings for the queue.
         */
        this.settings;
        this.resetSettings();
    }

    /**
     * Internal clear method.
     */
    __clear() {
        if (this.queueMsg) {
            setMessageComponenentsEnabled(this.queueMsg, false).catch(err => {});
        }
        if (this.dashboardMsg) {
            setMessageComponenentsEnabled(this.dashboardMsg, false).catch(err => {});
        }
        this.currentSongInfo = null;
        this.__currentResource = null;
        this.__currentPlayer = null;
        this.queue.clear();
        this.queueMsg = null;
        this.dashboardMsg = null;
        this.resetSettings();
    }

    /**
     * Reset the current settings.
     */
    resetSettings() {
        this.settings = {
            loop: false,
            loopCurrent: false,
            page: 0
        };
    }

    /**
     * Returns the voice connection corresponding to this state.
     * @returns {VoiceConnection}
     */
    getVoiceConnection() {
        return getVoiceConnection(this.guildId);
    }
    
    /**
     * Create a voice connection for the given channel.
     * @param {Guild} guild The guild where the connection is created.
     * @param {VoiceChannel} voiceChannel The channel to connect to.
     */
    connect(guild, voiceChannel) {
        this.resetSettings();
        const connection = joinVoiceChannel({
            channelId: voiceChannel.id,
            guildId: guild.id,
            adapterCreator: guild.voiceAdapterCreator
        });
        connection.on('stateChange', (oldState, newState) => {
            if (newState.status === VoiceConnectionStatus.Disconnected || 
                newState.status === VoiceConnectionStatus.Destroyed) {
                    this.__clear();
            }
        });
    }

    /**
     * Returns whether the bot is currently connected in a voice channel.
     */
    isConnected() {
        const connection = this.getVoiceConnection();
        return isConnected(connection);
    }

    /**
     * Terminates the current connection and all the associated resources.
     */
    destroy() {
        this.__clear();
        if (this.__currentPlayer) {
            this.__currentPlayer.stop(true);
        }
        const connection = this.getVoiceConnection();
        if (connection) {
            connection.disconnect();
            connection.destroy();
        }
    }

    /**
     * Returns whether the bot is currently playing a song (even if it's paused).
     */
    isPlaying() {
        return !!this.__currentPlayer && this.__currentPlayer.state.status !== AudioPlayerStatus.Idle;
    }

    /**
     * Returns the current status of the audio player or null if no audio player has been created.
     * @returns {AudioPlayerStatus?}
     */
    getPlayerStatus() {
        return !!this.__currentPlayer ? this.__currentPlayer.state.status : null;
    }

    __onCurrentSongFinished() {
        if (this.settings.loopCurrent) {
            this.playSong(this.currentSongInfo).catch(err => {
                logger.error(err);
            });
            return;
        }
        if (this.settings.loop) {
            this.queue.push(this.currentSongInfo);
        }
        if (this.queue.size() === 0) {
            this.currentSongInfo = null;
            // If there's no song in the queue disconnect after a given time.
            setTimeout(() => {
                if (!this.isPlaying() && this.queue.size() === 0)
                    this.destroy();
            }, DISCONNECT_TIMEOUT * 1000);
            return;
        }

        const songInfo = this.queue.pop();
        this.playSong(songInfo).catch(err => {
            logger.error(err);
        });
    }
    
    /**
     * Plays the given song.
     * @param {SongInfo} songInfo 
     */
    async playSong(songInfo) {
        if (!this.__currentPlayer) {
            // Create player if necessary.
            this.__currentPlayer = createAudioPlayer();
            this.__currentPlayer.on('error', err => {
                logger.error(err);
                if (this.dashboardMsg) {
                    this.dashboardMsg.channel.send("Une erreur est survenue. :confused:\nOn passe à la musique suivante. :point_right:")
                        .catch(() => {});
                }
                this.__onCurrentSongFinished();
            });
            // Set behaviour when a song is finished.
            this.__currentPlayer.on(AudioPlayerStatus.Idle, this.__onCurrentSongFinished.bind(this));
            const connection = this.getVoiceConnection();
            connection.subscribe(this.__currentPlayer);
        }
        try {
            const source = await playdl.stream(songInfo.url, { language: "fr", quality: 2 })
            this.__currentResource = createAudioResource(source.stream, { inputType: source.type });
            this.__currentPlayer.play(this.__currentResource);
            this.currentSongInfo = songInfo;
            if (this.dashboardMsg) {
                this.refreshDashboardMsg(this.dashboardMsg.channel).catch(err => {
                    logger.error(err);
                });
            }
        }
        catch (err) {
            // If an error occurs, consider the current song finished and play the next one.
            logger.error(err);
            this.__onCurrentSongFinished();
        }
    }

    /**
     * Add a song to be played later in the queue.
     * @param {SongInfo} songInfo 
     */
    async enqueueSong(songInfo) {
        // If a song is currently playing push the song to the queue.
        if (this.isPlaying()) {
            this.queue.push(songInfo);
        }
        else {
            // Else create the correct player and associated events.
            await this.playSong(songInfo).catch(err => {
                logger.error(err);
            });
        }
    }

    /**
     * Pause the currently played song.
     * @returns {boolean} Whether the player was successfully paused.
     */
    pause() {
        return !!this.__currentPlayer ? this.__currentPlayer.pause() : false;
    }

    /**
     * Resume the currently played song.
     * @returns {boolean} Whether the player was successfully unpaused.
     */
    resume() {
        return !!this.__currentPlayer ? this.__currentPlayer.unpause() : false;
    }

    /**
     * Resend the queue message with the updated data and deletes the old one.
     * @param {TextChannel} channel The channel where to send the new queue message.
     */
    async refreshQueueMsg(channel) {
        if (this.queueMsg) {
            await this.queueMsg.delete().catch(() => {});
        }
        const pageCount = Math.ceil(this.queue.size() / SONGS_PER_PAGE);
        // Make sure the page index is not out of bounds.
        this.settings.page = Math.max(0, Math.min(pageCount - 1, this.settings.page));

        const incomingSongs = [];
        {
            const start = this.settings.page * SONGS_PER_PAGE;
            let index = start + 1;
            for (let info of this.queue.iterate(start, start + 9)) {
                incomingSongs.push(`${index++} - \`${info.title}\` ajouté par <@${info.requester}>`);
            }
        }

        const embed = new MessageEmbed()
            .setTitle("Playlist")
            .setFooter(`Page ${this.settings.page + 1}/${pageCount}`)
            .setColor(CONSTANTS.COLOR_MUSIC);
        if (this.settings.loop)
            embed.addField("En boucle:", "Oui")
        if (this.settings.loopCurrent)
            embed.addField("Musique actuelle en boucle:", "Oui")
        if (this.currentSongInfo)
            embed.addField("En cours:", this.currentSongInfo.title);
        if (incomingSongs.length > 0)
            embed.addField("À venir:", incomingSongs.join('\n'))
        if (!this.currentSongInfo && incomingSongs.length === 0)
            embed.setDescription("Aucune musique dans la playlist.");
        
        const rows = [];
        var currentRow = new MessageActionRow();
        this.service.__queueInteractionHandlers.forEach(handler => {
            const button = new MessageButton()
                .setCustomId(`queue:${handler.name}`)
                .setEmoji(handler.icon)
                .setStyle("SECONDARY");
            if (currentRow.components.length >= CONSTANTS.DISCORD_BUTTON_PER_ACTION_ROW_MAX_COUNT) {
                rows.push(currentRow);
                currentRow = new MessageActionRow();
            }
            currentRow.components.push(button);
        });
        rows.push(currentRow);
        this.queueMsg = await channel.send({ embeds: [embed], components: rows });
    }

    /**
     * Resend the dashboard message with the updated data and deletes the old one.
     * @param {TextChannel} channel The channel where to send the new dashboard message.
     */
    async refreshDashboardMsg(channel) {
        if (this.dashboardMsg) {
            await this.dashboardMsg.delete().catch(() => {});
            this.dashboardMsg = null;
        }

        if (!this.currentSongInfo) {
            channel.send("Aucune musique en cours.");
            return;
        }

        const barSize = 20;
        const progressElement = "▬";
        const progressElementDone = `[▬](${this.currentSongInfo.url})`;
        const currentDuration = durationToString(Math.floor(this.__currentResource.playbackDuration * 0.001));
        const maxDuration = durationToString(this.currentSongInfo.length);
        const percentage = this.__currentResource.playbackDuration / (this.currentSongInfo.length * 1000);
        const done = Math.floor(barSize * percentage);
        const toFinish = barSize - done;
        const progressBar = `${progressElementDone.repeat(done)}${progressElement.repeat(toFinish)} ${currentDuration}/${maxDuration}`;

        const embed = new MessageEmbed()
            .setTitle(this.currentSongInfo.title)
            .setURL(this.currentSongInfo.url)
            .setColor(CONSTANTS.COLOR_MUSIC)
            .setDescription(progressBar)
            .addField("Ajouté par:", `<@${this.currentSongInfo.requester}>`, true);
        if (this.settings.loopCurrent)
            embed.addField("En boucle:", "Oui", true)
        if (this.currentSongInfo.thumbnail)
            embed.setThumbnail(this.currentSongInfo.thumbnail);
        
        const rows = [];
        var currentRow = new MessageActionRow();
        this.service.__dashboardInteractionHandlers.forEach(handler => {
            const button = new MessageButton()
                .setCustomId(`dashboard:${handler.name}`)
                .setEmoji(handler.icon)
                .setStyle("SECONDARY");
            if (currentRow.components.length >= CONSTANTS.DISCORD_BUTTON_PER_ACTION_ROW_MAX_COUNT) {
                rows.push(currentRow);
                currentRow = new MessageActionRow();
            }
            currentRow.components.push(button);
        });
        rows.push(currentRow);

        this.dashboardMsg = await channel.send({ embeds: [embed], components: rows });
    }
}

/**
 * @returns {import('./musicButtonInteractions').InteractionHandler[]}
 */
function getInteractionHandlers() {
    const handlers = [];
    const handlerModule = require('./musicButtonInteractions.js');
    for (let key in handlerModule) {
        handlers.push(handlerModule[key]);
    }
    return handlers;
}

/**
 * @class Service to play and manage musics in vocal channels.
 */
class MusicService extends BaseService
{
    constructor(ctx) {
        super(ctx);

        /**
         * @type {Map<string, MusicState>}
         */
        this.__musicStates = new Map();
        /**
         * @type {Map<string, import('./musicButtonInteractions').InteractionHandler}
         */
        this.__queueInteractionHandlers = new Map();
        /**
         * @type {Map<string, import('./musicButtonInteractions').InteractionHandler}
         */
         this.__dashboardInteractionHandlers = new Map();
    }

    async init() {
        // Import handlers.
        const handlers = getInteractionHandlers();
        for (let handler of handlers) {
            if (handler.type === "Queue") {
                this.__queueInteractionHandlers.set(handler.name, handler);
            }
            else if (handler.type === "Dashboard") {
                this.__dashboardInteractionHandlers.set(handler.name, handler);
            }
        }

        const listener = this.clientContext.getService(WidgetListener);
        listener.registerButtonCallback(this.onButtonClicked.bind(this));
    }

    /**
     * @inheritdoc
     */
    async terminate() {
        for (let [_, state] of this.__musicStates) {
            state.destroy();
        }
        this.__musicStates.clear();
    }

    /**
     * Callback invoked when a button is clicked.
     * @param {ButtonInteraction} interaction 
     */
    onButtonClicked(interaction) {
        const musicState = this.getMusicState(interaction.guildId);
        if (!musicState.isConnected())
            return;
        
        var handlerMap = null;
        if (musicState.queueMsg && interaction.message.id === musicState.queueMsg.id)
        {
            // Handle queue buttons
            handlerMap = this.__queueInteractionHandlers;
        }
        else if (musicState.dashboardMsg && interaction.message.id === musicState.dashboardMsg.id)
        {
            // Handle dashboard buttons
            handlerMap = this.__dashboardInteractionHandlers;
        }
        
        if (!handlerMap)
            return;
        
        const voiceChannel = interaction.member.voice.channel;
        if (!voiceChannel || voiceChannel.id !== musicState.getVoiceConnection().joinConfig.channelId) {
            interaction.reply({ ephemeral: true, content: "Tu dois être connecté dans le même vocal que le bot pour utiliser ces boutons." })
                .catch(err => { logger.error(err); });
            return;
        }
        
        const handlerName = interaction.customId.split(':').at(-1);
        const handler = handlerMap.get(handlerName);
        if (!handler)
            return;
        
        handler.execute(musicState, interaction).catch(err => {
            logger.error(err);
        });
    }

    /**
     * Returns the current MusicState for the given guild.
     * @param {string} guildId 
     * @returns {MusicState}
     */
    getMusicState(guildId) {
        var musicState = this.__musicStates.get(guildId);
        if (!musicState) {
            // Lazy instantiation.
            musicState = new MusicState(this, guildId);
            this.__musicStates.set(guildId, musicState);
        }
        return musicState;
    }

    /**
     * Search and adds the given song to the play queue. If no voice connection has been established, connects the bot in the given channel.
     * @param {Guild} guild The guild in which to play the song.
     * @param {VoiceChannel} voiceChannel The channel in which to play the song.
     * @param {GuildMember} requester The user who requested the song.
     * @param {string} songQuery A query string or the url of the youtube video to play for the next song.
     * @returns {Promise<SongInfo>} The infos about the newly added song.
     */
    async play(guild, voiceChannel, requester, songQuery) {
        const musicState = this.getMusicState(guild.id);
        var songInfo = null;
        if (validateYtUrl(songQuery)) {
            songInfo = await getSongInfoFromUrl(songQuery).catch(err => { logger.error(err); });
            if (!songInfo) {
                throw MUSIC_ERRORS.INVALID_URL;
            }
        }
        else {
            // Search the video.
            songInfo = await getSongInfoFromQuery(songQuery).catch(err => { logger.error(err); });
            if (!songInfo)
                throw MUSIC_ERRORS.NO_RESULT;
        }
        songInfo.requester = requester.id;
        const connection = musicState.getVoiceConnection();
        if (!isConnected(connection)) {
            musicState.connect(guild, voiceChannel);
        }
        else if (connection.joinConfig.channelId !== voiceChannel.id) {
            throw MUSIC_ERRORS.WRONG_VOICE_CHANNEL;
        }
        else if (!voiceChannel.joinable) {
            throw MUSIC_ERRORS.UNJOINABLE_VOICE_CHANNEL;
        }
        await musicState.enqueueSong(songInfo);
        return songInfo;
    }
}

module.exports = {
    MusicService,
    MusicState,
    MUSIC_ERRORS,
    parseDuration,
    durationToString,
    validateYtUrl
}
