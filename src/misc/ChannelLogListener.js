const util = require('util');
const { BaseService } = require("../BaseService");
const { ChannelService, CHANNEL_CATEGORIES } = require('../utils/channels/ChannelService.js');
const { ChannelInfo } = require("../database/models/ChannelInfo");
const { logger } = require("../logging/logging");
const { CONSTANTS } = require('../utils/helper');

/**
 * Service allowing to print all the logs of the application into dedicated channels for every guild.
 * This is not implemented as a specific Logger class because those are not context sensitive.
 * This class use instead events callbacks to the static logger class.
 */
class ChannelLogListener extends BaseService
{
    constructor(ctx) {
        super(ctx);
        /**
         * @type {Set<string>} Set of all the current log channel ids.
         */
        this.__logChannels = new Set();
        // Register callbacks in variables so that they can be unregistered later.
        this.__onLog = this.onLog.bind(this);
        this.__onDebug = this.onDebug.bind(this);
        this.__onError = this.onError.bind(this);
    }

    /**
     * @inheritdoc
     */
    async init() {
        const channelService = this.clientContext.getService(ChannelService);
        const channelInfos = await channelService.findAllChannelInfosWithCategoryWithoutGuild(CHANNEL_CATEGORIES.LOG, false);
        for (let channelInfo of channelInfos) {
            this.__logChannels.add(channelInfo.channelId);
        }

        channelService.onChannelInfoUpdate(this.onChannelInfoUpdate.bind(this));
        channelService.onChannelInfoDestroy(this.onChannelInfoDestroy.bind(this));
        logger.on('LOG', this.__onLog);
        logger.on('DEBUG', this.__onDebug);
        logger.on('ERROR', this.__onError);
    }

    /**
     * @inheritdoc
     */
    async terminate() {
        // Make sure to unregister to events to avoid memory leaks.
        logger.off('LOG', this.__onLog);
        logger.off('DEBUG', this.__onDebug);
        logger.off('ERROR', this.__onError);
    }

    /**
     * Internal method to send the logs into all the log channels. 
     * @param {"LOG" | "DEBUG" | "ERROR"} logType The type of log
     * @param {*} message 
     * @param  {...any} params 
     */
    async __flushLogsToChannels(logType, message, ...params) {
        const header = `######${logType}######\n`;
        const limit = CONSTANTS.DISCORD_MESSAGE_LENGHT_LIMIT - header.length - 6;
        for (let channelId of this.__logChannels) {
            const channel = await this.clientContext.client.channels.fetch(channelId).catch(_ => {});
            if (channel && channel.isText()) {
                const formated = util.format(message, ...params);
                const text = `\`\`\`${header}${formated.length > limit ? formated.slice(0,limit-1) : formated}\`\`\``;
                channel.send(text).catch(_ => {});
            }
        }
    }

    /**
     * Callback invoked when a standard log occurs.
     * @param {*} message 
     * @param  {...any} params 
     */
    onLog(message, ...params) {
        this.__flushLogsToChannels("LOG", message, ...params).catch(_ => {}); // Don't log in case of errors to avoid recursive nonsense.
    }

    /**
     * Callback invoked when a debug log occurs.
     * @param {*} message 
     * @param  {...any} params 
     */
    onDebug(message, ...params) {
        this.__flushLogsToChannels("DEBUG", message, ...params).catch(_ => {}); // Don't log in case of errors to avoid recursive nonsense.
    }

    /**
     * Callback invoked when an error log occurs.
     * @param {*} message 
     * @param  {...any} params 
     */
    onError(message, ...params) {
        this.__flushLogsToChannels("ERROR", message, ...params).catch(_ => {}); // Don't log in case of errors to avoid recursive nonsense.
    }

    /**
     * Callback called when a channel info is updated.
     * @param {ChannelInfo} oldInfo 
     * @param {ChannelInfo} newInfo 
     */
    onChannelInfoUpdate(oldInfo, newInfo) {
        if (newInfo.getCategories().includes(CHANNEL_CATEGORIES.LOG)) {
            this.__logChannels.add(newInfo.channelId);
        }
        else {
            this.__logChannels.delete(newInfo.channelId);
        }
    }

    /**
     * Callback called when a channel info is destroyed.
     * @param {ChannelInfo} info 
     */
    onChannelInfoDestroy(info) {
        this.__logChannels.delete(info.channelId);
    }
}

module.exports = {
    ChannelLogListener
}
