const { BaseService } = require("../BaseService");

class GuildCache
{
    constructor(guildId) {
        this.guildId = guildId;
        /**
         * @type {Map<string,any>}
         */
        this.cache = new Map();
    }
}

/**
 * A service to cache any kind of data for each guild.
 */
class CacheService extends BaseService
{
    constructor(ctx) {
        super(ctx);

        /**
         * @type {Map<string, GuildCache>}
         */
        this.__caches = new Map();
    }

    /**
     * Cache the given data.
     * @param {string} guildId The id of the guild.
     * @param {string} name The name of the data.
     * @param {*} data The data to cache.
     */
    set(guildId, name, data) {
        var cache = this.__caches.get(guildId);
        if (!cache) {
            cache = new GuildCache(guildId);
            this.__caches.set(guildId, cache);
        }
        cache.cache.set(name, data);
    }

    /**
     * Get cached data.
     * @param {string} guildId The id of the guild.
     * @param {string} name The name of the data.
     * @returns {any | null}
     */
    get(guildId, name) {
        const cache = this.__caches.get(guildId);
        return cache ? cache.cache.get(name) : null;
    }
}

module.exports = {
    CacheService
}
