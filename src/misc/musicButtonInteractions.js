const { ButtonInteraction } = require('discord.js');
const { AudioPlayerStatus } = require('@discordjs/voice');
const { shuffle } = require('../utils/helper');

/**
 * @typedef {import('./MusicService').MusicState} MusicState
 */

/**
 * @typedef {"Queue" | "Dashboard"} InteractionType
 */

/**
 * @typedef {Object} InteractionHandler
 * @property {string} name The name of the interaction.
 * @property {string} icon The emoji used for this button.
 * @property {InteractionType} type The type of interaction.
 * @property {(musicState : MusicState, interaction : ButtonInteraction) => Promise} execute The function called when the interaction occurs.
 */

/**
 * @type {InteractionHandler}
 */
 const queuePreviousPage = {
    name: "previousPage",
    icon: "⬅️",
    type: "Queue",
    async execute(musicState, interaction) {
        musicState.settings.page -= 1;
        await musicState.refreshQueueMsg(interaction.channel);
    }
};

/**
 * @type {InteractionHandler}
 */
const queueNextPage = {
    name: "nextPage",
    icon: "➡️",
    type: "Queue",
    async execute(musicState, interaction) {
        musicState.settings.page += 1;
        await musicState.refreshQueueMsg(interaction.channel);
    }
};

/**
 * @type {InteractionHandler}
 */
const queueShuffle = {
    name: "shuffle",
    icon: "🔀",
    type: "Queue",
    async execute(musicState, interaction) {
        const shuffledContent = shuffle(musicState.queue.toArray());
        musicState.queue.clear();
        musicState.queue.push(...shuffledContent);
        musicState.settings.page = 0;
        await musicState.refreshQueueMsg(interaction.channel);
    }
};

/**
 * @type {InteractionHandler}
 */
const queueLoop = {
    name: "loop",
    icon: "🔁",
    type: "Queue",
    async execute(musicState, interaction) {
        musicState.settings.loop = !musicState.settings.loop;
        await musicState.refreshQueueMsg(interaction.channel);
        await interaction.channel.send(`Le mode loop est ${musicState.settings.loop ? 'activé' : 'désactivé'}.`);
    }
};

/**
 * @type {InteractionHandler}
 */
const queueClear = {
    name: "clear",
    icon: "🚮",
    type: "Queue",
    async execute(musicState, interaction) {
        musicState.queue.clear();
        await musicState.refreshQueueMsg(interaction.channel);
    }
};

/**
 * @type {InteractionHandler}
 */
 const dashboardPlayPause = {
    name: "playPause",
    icon: "⏯️",
    type: "Dashboard",
    async execute(musicState, interaction) {
        const status = musicState.getPlayerStatus();
        const paused = status !== AudioPlayerStatus.AutoPaused && status !== AudioPlayerStatus.Paused;
        if (paused)
            musicState.pause()
        else
            musicState.resume();
        await musicState.refreshDashboardMsg(interaction.channel);
        await interaction.channel.send(paused ? "La musique est en pause." : "La musique reprend.");
    }
};

/**
 * @type {InteractionHandler}
 */
const dashboardSkip = {
    name: "skip",
    icon: "⏭️",
    type: "Dashboard",
    async execute(musicState, interaction) {
        await interaction.update({}); // Empty update to catch the interaction.
        musicState.__currentPlayer.stop(true);
    }
};

/**
 * @type {InteractionHandler}
 */
 const dashboardStop = {
    name: "stop",
    icon: "⏹️",
    type: "Dashboard",
    async execute(musicState, interaction) {
        musicState.destroy();
        await interaction.update({}); // Empty update to catch the interaction.
        await interaction.channel.send("Déconnecté.");
    }
};

/**
 * @type {InteractionHandler}
 */
 const dashboardLoopCurrent = {
    name: "loopCurrent",
    icon: "🔂",
    type: "Dashboard",
    async execute(musicState, interaction) {
        musicState.settings.loopCurrent = !musicState.settings.loopCurrent;
        await musicState.refreshDashboardMsg(interaction.channel);
        await interaction.channel.send(`La musique en cours va${musicState.settings.loopCurrent ? '' : ' arrêter de'} se jouer en boucle.`);
    }
};

// The order in which the handler are exported corresponds to the order in which the buttons will be displayed.
module.exports = {
    // Queue
    queuePreviousPage,
    queueNextPage,
    queueShuffle,
    queueLoop,
    queueClear,
    // Dashboard
    dashboardPlayPause,
    dashboardSkip,
    dashboardStop,
    dashboardLoopCurrent
}