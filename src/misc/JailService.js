const { GuildMember, Permissions, TextChannel, Guild } = require("discord.js");
const { scheduleJob, Job } = require('node-schedule');
const { logger } = require("../logging/logging");
const { BaseService } = require("../BaseService");
const { JailInfo } = require("../database/models/JailInfo");
const { RoleService, ROLE_NAMES } = require("../utils/roles/RoleService");
const { SettingService, SETTING_NAMES } = require("../utils/guildSettings/SettingsService");

/**
 * @enum {string} Enumeration of possible errors.
 */
const JAIL_SERVICE_ERRORS = {
    NO_ERROR: "NO_ERROR",
    NO_PRISONER_ROLE: "NO_PRISONER_ROLE",
    ALREADY_IN_JAIL: "ALREADY_IN_JAIL",
    NOT_IN_JAIL: "NOT_IN_JAIL",
    CANT_BE_MANAGED: "CANT_BE_MANAGED",
    OPERATION_FAILED: "OPERATION_FAILED"
}

/**
 * @class Service to handle jail/free operations.
 */
class JailService extends BaseService
{
    constructor(ctx) {
        super(ctx);

        /**
         * @type {Map<string, Job>} Inner map containing jobs to release members.
         */
        this.__jobs = new Map();
    }

    /**
     * @inheritdoc
     */
    async init() {
        const roleService = this.clientContext.getService(RoleService);
        for (let guild of this.clientContext.client.guilds.cache.values()) {
            // Check all jailed members and whether to free them.
            const prisonerRole = await roleService.getRoleWithName(guild, ROLE_NAMES.PRISONER);
            if (!prisonerRole)
                continue;

            const jailInfos = await JailInfo.findAll({ where: { guildId: guild.id } });
            for (let jailInfo of jailInfos) {
                // If the jailInfo is not valid ignore it and destroy it.
                if (!jailInfo.jailedAt) {
                    jailInfo.destroy().catch(() => {});
                    continue;
                }

                const now = new Date();
                if (now > jailInfo.jailedAt) {
                    // Free immediately if the date is over.
                    const member = await guild.members.fetch(jailInfo.userId);
                    // Sanity check.
                    if (!member || !member.roles.cache.has(prisonerRole.id)) {
                        jailInfo.destroy().catch(() => {});
                        continue;
                    }
                    this.free(member, {}).catch((err) => { logger.error(err); });
                }
                else {
                    // Else schedule the release.
                    const duration = jailInfo.duration || 10_000; // Don't know why 10 seconds...
                    const releaseDate = new Date(jailInfo.jailedAt.getTime() + duration);
                    const job = scheduleJob(releaseDate, () => {
                        this.free(member, {}).catch(() => {});
                    });
                    this.__jobs.set(jailInfo.uniqueId, job);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    async terminate() {
        // Cancel all pending jobs.
        this.__jobs.forEach(job => {
            job.cancel();
        });
        this.__jobs.clear();
    }

    /**
     * Returns whether the given guild member is in jail.
     * @param {GuildMember} member 
     * @returns {Promise<boolean>}
     */
    async isInJail(member) {
        const roleService = this.clientContext.getService(RoleService);
        const prisonerRole = await roleService.getRoleWithName(member.guild, ROLE_NAMES.PRISONER);
        return !!prisonerRole && member.roles.cache.has(prisonerRole.id);
    }

    /**
     * Send to jail the given user.
     * @param {GuildMember} member The user to send to jail.
     * @param {object} options Jail options.
     * @param {number} options.duration How many time to jail the user (in ms).
     * @param {TextChannel} options.channel The channel from which the member has been sent to jail.
     * @throws {JAIL_SERVICE_ERRORS} An error message if the jail operation is impossible.
     */
    async jail(member, options) {
        var duration = options.duration;

        // Check default duration on the current guild. This can be handy for auto jail like the spam ban mode.
        if (!duration) {
            const settingService = this.clientContext.getService(SettingService);
            duration = await settingService.getSetting(member.guild.id, SETTING_NAMES.JAIL_DEFAULT_DURATION);
            // Converts min to ms
            duration *= 60_000;
        }

        const { guild } = member;

        const roleService = this.clientContext.getService(RoleService);
        const prisonerRole = await roleService.getRoleWithName(guild, ROLE_NAMES.PRISONER);
        if (!prisonerRole) {
            throw JAIL_SERVICE_ERRORS.NO_PRISONER_ROLE;
        }

        if (!guild.me.permissions.has(Permissions.FLAGS.MANAGE_ROLES) || !member.manageable) {
            throw JAIL_SERVICE_ERRORS.CANT_BE_MANAGED;
        }

        const uniqueId = JailInfo.createUniqueId(guild.id, member.id);
        const now = new Date();
        const originalRoleIds = member.roles.cache.map(role => role.id);

        var isError = false;

        // Check if the user is already in jail.
        if (member.roles.cache.has(prisonerRole.id)) {
            throw JAIL_SERVICE_ERRORS.ALREADY_IN_JAIL;
        } 

        // Remove all current roles and add prisoner role.
        await member.roles.set([prisonerRole]).catch(_err => {
            logger.error(_err);
            isError = true;
        });

        if (!isError) {
            const [jailInfo, _created] = await JailInfo.findOrCreate({
                where: { uniqueId: uniqueId },
                defaults: { uniqueId: uniqueId, guildId: guild.id, userId: member.id } 
            })
            .catch(_err => {
                logger.error(_err);
                isError = true;
                return [null, false];
            });

            if (jailInfo && !isError) {
                jailInfo.jailedAt = now;
                jailInfo.duration = duration;
                jailInfo.channelId = options.channel.id;
                jailInfo.setOriginalRoles(originalRoleIds);
                await jailInfo.save().catch(_err => {
                    logger.error(_err);
                    isError = true;
                });
            }
        }

        if (isError) {
            await member.roles.set(originalRoleIds).catch(() => {}); // Restore roles if the operation has failed.
            throw JAIL_SERVICE_ERRORS.OPERATION_FAILED;
        }

        if (options.channel)
            options.channel.send("La racaille a été envoyée en prison.");

        // Shedule release.
        const releaseDate = new Date(now.getTime() + duration);
        const job = scheduleJob(releaseDate, () => {
            this.free(member, { channel: options.channel })
                .catch(() => {});
        });
        this.__jobs.set(uniqueId, job);
    }

    /**
     * Free all jailed users from the given guild.
     * @param {Guild} guild The guild to free the users from.
     */
    async freeAll(guild) {
        const jailInfos = await JailInfo.findAll({ where: { guildId: guild.id } });
        if (!jailInfos || jailInfos.length === 0) {
            throw JAIL_SERVICE_ERRORS.NOT_IN_JAIL;
        }

        const promises = [];

        for (let jailInfo of jailInfos) {
            const member = await guild.members.fetch(jailInfo.userId);
            if (!member) {
                jailInfo.destroy().catch(() => {});
                continue;
            }
            promises.push(
                this.free(member, {}).catch(() => {})
            );
        }

        await Promise.all(promises);
    }

    /**
     * Free from jail the given member.
     * @param {GuildMember} member The user to free.
     * @param {object} options Free options.
     * @param {TextChannel} options.channel The channel in which the user has been freed.
     */
    async free(member, options) {
        const { guild } = member;

        const uniqueId = JailInfo.createUniqueId(guild.id, member.id);

        // Remove corresponding job.
        const job = this.__jobs.get(uniqueId);
        if (job)
            job.cancel();
        this.__jobs.delete(uniqueId);

        const roleService = this.clientContext.getService(RoleService);
        const prisonerRole = await roleService.getRoleWithName(guild, ROLE_NAMES.PRISONER);
        if (!prisonerRole) {
            throw JAIL_SERVICE_ERRORS.NO_PRISONER_ROLE;
        }

        if (!member.roles.cache.has(prisonerRole.id)) {
            throw JAIL_SERVICE_ERRORS.NOT_IN_JAIL;
        }

        if (!guild.me.permissions.has(Permissions.FLAGS.MANAGE_ROLES) || !member.manageable) {
            throw JAIL_SERVICE_ERRORS.CANT_BE_MANAGED;
        }

        const jailInfo = await JailInfo.findByPk(uniqueId);
        if (!jailInfo) {
            throw JAIL_SERVICE_ERRORS.OPERATION_FAILED;
        }
        const originalRoles = jailInfo.getOriginalRoles();

        var isError = false;

        await member.roles.set(originalRoles).catch(_err => {
            logger.error(_err);
            isError = true;
        });

        if (jailInfo)
            jailInfo.destroy().catch(() => {});
        
        if (isError)
            throw JAIL_SERVICE_ERRORS.OPERATION_FAILED;

        const channel = options.channel || guild.channels.cache.get(jailInfo.channelId);
        
        if (channel)
            channel.send(`Tu es libre maintenant ${member}. Mais qu'on ne t'y reprenne plus.`);
    }
}

module.exports = {
    JailService,
    JAIL_SERVICE_ERRORS
}