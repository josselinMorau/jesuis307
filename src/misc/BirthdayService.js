const { GuildMember } = require("discord.js");
const { Op, Sequelize } = require("sequelize");
const { BaseService } = require("../BaseService");
const { Birthday } = require('../database/models/Birthday.js');
const { BirthdayMessage } = require('../database/models/BirthdayMessage.js');
const { logger } = require("../logging/logging");
const { uuid } = require("../utils/helper");

/**
 * @class Service handling the datas relative to birthdays.
 */
class BirthdayService extends BaseService
{
    constructor(ctx) {
        super(ctx);

        // Remove Birthday data if a member leaves.
        this.clientContext.client.on('guildMemberRemove', member => {
            this.deleteBirthday(member).catch(_err => {
                logger.error(_err);
            });
        });
    }

    /**
     * Update or create the birthday model for the given member.
     * @param {GuildMember} member The member to update.
     * @param {Date} date The birthday's date.
     */
    async updateBirthday(member, date) {
        const data = {
            uniqueId: Birthday.createUniqueId(member.guild.id, member.id),
            guildId: member.guild.id,
            userId: member.id,
            date: date
        };
        const fields = Object.keys(data);
        await Birthday.upsert(data, { fields: fields });
    }

    /**
     * Deletes the birthday data for the given member.
     * @param {GuildMember} member 
     */
    async deleteBirthday(member) {
        const uniqueId = Birthday.createUniqueId(member.guild.id, member.id);
        await Birthday.destroy({ where: { uniqueId: uniqueId } });
    }

    /**
     * Returns all birthday data with the same day and month than the provided date.
     * @param {string} guildId The id of the guild.
     * @param {Date} date The date to compare.
     * @returns {Promise<Birthday[]>}
     */
    async findAllBirthdaysWithDate(guildId, date) {
        return await Birthday.findAll({ where: {
            [Op.and]: [
                { guildId: guildId },
                Sequelize.where(Sequelize.fn('MONTH', Sequelize.col('date')), date.getMonth() + 1),
                Sequelize.where(Sequelize.fn('DAYOFMONTH', Sequelize.col('date')), date.getDate())
            ]
        }});
    }

    /**
     * Returns all `BirthdayMessage` for the given guild.
     * @param {string} guildId The id of the guild.
     * @returns {Promise<BirthdayMessage[]>}
     */
    async findAllBirthdayMessages(guildId) {
        return BirthdayMessage.findAll({ where: { guildId: guildId } });
    }

    /**
     * Returns all `BirthdayMessage` for a guild whose id starts with the given one.
     * @param {string} guildId The id of the guild.
     * @param {string} id The id of the message to find. (or the start of it)
     * @returns {Promise<BirthdayMessage[]>}
     */
    async findAllBirthdayMessagesWithId(guildId, id) {
        return await BirthdayMessage.findAll({ where: {
            guildId: guildId,
            uniqueId: {
                [Op.startsWith]: id
            }
        }});
    }

    /**
     * Returns a random `BirthdayMessage` for the given guild.
     * @param {string} guildId The id of the guild.
     * @returns {Promise<BirthdayMessage>}
     */
    async findOneRandomBirthdayMessage(guildId) {
        return await BirthdayMessage.findOneRandom({ where: { guildId: guildId } });
    }

    /**
     * Add a `BirthdayMessage` for a guild.
     * @param {GuildMember} author The author of the message.
     * @param {string} content The message to sent on birthdays.
     */
    async addBirthdayMessage(author, content) {
        const data = {
            uniqueId: uuid(),
            guildId: author.guild.id,
            authorId: author.id,
            content: content
        };
        const fields = Object.keys(data);
        await BirthdayMessage.create(data, { fields: fields });
    }
}

module.exports = {
    BirthdayService
}