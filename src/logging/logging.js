const fs = require('fs');
const { BaseLogger } = require('./BaseLogger.js');
const { ConsoleLogger } = require('./loggers/ConsoleLogger.js');
const { isSubclassOf } = require('../utils/helper.js');
const { options } = require('../../options.js');

/**
 * Create the application logger.
 * @returns {BaseLogger}
 */
function createLogger() {
    const loggerName = options.logger.toLowerCase();
    var LoggerType = null;
    const loggerFiles = fs.readdirSync('./src/logging/loggers')
            .filter(file => file.endsWith('.js'));
    for (let file of loggerFiles) {
        const loggerModule = require(`./loggers/${file}`);
        if (isSubclassOf(loggerModule, BaseLogger) && loggerModule.name.toLowerCase().startsWith(loggerName)) {
            LoggerType = loggerModule;
            break;
        }
        else {
            for (let key in loggerModule) {
                const subModule = loggerModule[key];
                if (isSubclassOf(subModule, BaseLogger) && subModule.name.toLowerCase().startsWith(loggerName) ) {
                    LoggerType = subModule;
                    break;
                }
            }
        }
    }
    return LoggerType ? new LoggerType() : new ConsoleLogger(); // ConsoleLogger is the default logger.
}

// Events are capitalized because 'error' is a default event for EventEmitter.
/**
 * @typedef {"LOG" | "DEBUG" | "ERROR"} LogEventType
 */

/**
 * @typedef {(msg, ...params) => void} LogEventCallback
 */

/**
 * @class Static wrapper for the current application logger.
 */
class logger { // We don't capitalize the 1st letter to be consistent with the old instance version.

    /**
     * @type {BaseLogger} Logger singleton.
     */
    static __logger = null;

    /**
     * Create the application's logger.
     */
    static init() {
        this.__logger = createLogger();
    }

    /**
     * Log an information.
     * @param {*} message The main message.
     * @param  {...any} params Objects with which to replace substitution strings within the main message.
     * If no subsitution string is present, the arguments will be concatenated to the main message, separated by spaces.
     */
    static log(message, ...params) {
        if (this.__logger) {
            this.__logger.log(message, ...params);
            this.__logger.__emitter.emit('LOG', message, ...params);
        }
    }

    /**
     * Same as `log` but only active in test mode.
     * @param {*} message The main message.
     * @param  {...any} params Objects with which to replace substitution strings within the main message.
     * If no subsitution string is present, the arguments will be concatenated to the main message, separated by spaces.
     */
    static debug(message, ...params) {
        if (this.__logger) {
            this.__logger.debug(message, ...params);
            this.__logger.__emitter.emit('DEBUG', message, ...params);
        }
    }

    /**
     * Log an error.
     * @param {*} message The main message.
     * @param  {...any} params Objects with which to replace substitution strings within the main message.
     * If no subsitution string is present, the arguments will be concatenated to the main message, separated by spaces.
     */
    static error(message, ...params) {
        if (this.__logger) {
            this.__logger.error(message, ...params);
            this.__logger.__emitter.emit('ERROR', message, ...params);
        }
    }

    /**
     * Fetch the logs.
     * @param {number} page The index of the log page to fetch. 0 by default.
     * @returns {Promise<import('discord.js').FileOptions>} The content of the fetched logs as a file buffer.
     */
    static async fetchLogs(page = 0) {
        if (this.__logger)
            return await this.__logger.fetchLogs(page);
        return { 
            name: 'no_log.txt',
            attachment: Buffer.from("NO LOGS...", 'utf-8')
        }
    }
    
    /**
     * Register the callback for the given type of event.
     * @param {LogEventType} event The type of event to suscribe to.
     * @param {LogEventCallback} callback The callback to register.
     */
    static on(event, callback) {
        if (this.__logger)
            this.__logger.__emitter.on(event, callback);
    }

    /**
     * Unregister the callback for the given type of event.
     * @param {LogEventType} event The type of event to unsuscribe to.
     * @param {LogEventCallback} callback The callback to unregister.
     */
    static off(event, callback) {
        if (this.__logger)
            this.__logger.__emitter.off(event, callback);
    }
}

module.exports = {
    logger
}