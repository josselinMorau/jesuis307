const { EventEmitter } = require('events');
const { options } = require('../../options.js');
const { NotImplemented } = require('../utils/errors.js');

/**
 * Classe de base de tous les loggers.
 */
class BaseLogger {

    constructor() {
        /**
         * @type {EventEmitter} Event emitter for 'LOG', 'DEBUG' and 'ERROR' events.
         * Events are fired by the static logger class.
         */
        this.__emitter = new EventEmitter();
    }

    /**
     * Log an information.
     * @param {*} message The main message.
     * @param  {...any} params Objects with which to replace substitution strings within the main message.
     * If no subsitution string is present, the arguments will be concatenated to the main message, separated by spaces.
     */
    log(message, ...params) {}
    
    /**
     * Same as `log` but only active in test mode.
     * @param {*} message The main message.
     * @param  {...any} params Objects with which to replace substitution strings within the main message.
     * If no subsitution string is present, the arguments will be concatenated to the main message, separated by spaces.
     */
    debug(message, ...params) {
        if (options.test)
            this.log(message, ...params);
    }

    /**
     * Log an error.
     * @param {*} message The main message.
     * @param  {...any} params Objects with which to replace substitution strings within the main message.
     * If no subsitution string is present, the arguments will be concatenated to the main message, separated by spaces.
     */
    error(message, ...params) {}

    /**
     * Fetch the logs.
     * @param {number} page The index of the log page to fetch. 0 by default.
     * @returns {Promise<import('discord.js').FileOptions>} The content of the fetched logs.
     */
    async fetchLogs(page = 0) {
        throw new NotImplemented();
    }
}

module.exports = {
    BaseLogger
}