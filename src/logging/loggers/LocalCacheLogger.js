const util = require('util');
const { ConsoleLogger } = require('./ConsoleLogger.js');

const UPLOAD_MAX_SIZE = 8_000_000; // Discord default max upload size is 8MB.

/**
 * Logger that stores all logs into a local string in addition to printing it into the console.
 */
class LocalCacheLogger extends ConsoleLogger {

    constructor() {
        super();
        this.__localCache = [];
        this.__currentCache = "";
    }

    getCacheSize(cache) {
        return (new TextEncoder().encode(cache).byteLength);
    }

    log(message, ...params) {
        super.log(message, ...params);
        this.flush(message, ...params);
    }

    error(message, ...params) {
        super.error(message, ...params);
        this.flush(message, ...params);
    }

    flush(message, ...params) {
        const fmt = util.format(message, ...params);
        const incomingSize = this.getCacheSize(fmt);
        const currentSize = this.getCacheSize(this.__currentCache);
        if (currentSize + incomingSize > UPLOAD_MAX_SIZE) {
            this.__localCache.push(this.__currentCache);
            this.__currentCache = "";
        }
        this.__currentCache += fmt;
    }

    /**
     * @inheritdoc
     */
    async fetchLogs(page = 0) {
        const cache = page === 0 ? this.__currentCache : this.__localCache.at(-page);
        return {
            name: `logs_${page}.txt`,
            attachment: Buffer.from(cache, 'utf-8')
        };
    }
}

module.exports = {
    LocalCacheLogger
}