const { BaseLogger } = require('../BaseLogger.js');

/**
 * Logger that sends all its logs into the standard output.
 * Equivalent to call `console.log` or `console.error`.
 */
class ConsoleLogger extends BaseLogger {

    log(message, ...params) {
        console.log(message, ...params);
    }

    error(message, ...params) {
        console.error(message, ...params);
    }
}

module.exports = {
    ConsoleLogger
}