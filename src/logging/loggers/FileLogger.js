const fs = require('fs');
const util = require('util');
const { Buffer } = require('buffer');
const { ConsoleLogger } = require('./ConsoleLogger.js');
const { options } = require('../../../options.js');
const { OutOfRange } = require('../../utils/errors.js');

const UPLOAD_MAX_SIZE = 8_000_000; // Discord default max upload size is 8MB.

/**
 * Logger writing its logs in a file stream in addition to printing it into the console.
 */
class FileLogger extends ConsoleLogger {

    constructor() {
        super();
        this.createLogFolder();
        this.createNewStream();
    }

    log(message, ...params) {
        super.log(message, ...params);
        this.writeIntoStream(`Info [${new Date().toISOString()}] : ` + util.format(message, ...params));
    }

    error(message, ...params) {
        super.error(message, ...params);
        this.writeIntoStream(`Error [${new Date().toISOString()}] : `+ util.format(message, ...params));
    }

    /**
     * @inheritdoc
     * @param {number} page The index of the log page to fetch. 0 by default.
     * @returns {Promise<import('discord.js').FileOptions>} The content of the fetched logs.
     */
    async fetchLogs(page = 0) {
        const filepath = this.fetchFilePath(page);
        const filename = filepath.split('/').at(-1);
        const buffer = fs.readFileSync(filepath);
        return {
            name: filename,
            attachment: buffer
        }
    }

    /**
     * Returns the path of the log file corresponding to the given page.
     * @param {number} page
     * @returns {string} 
     */
    fetchFilePath(page) {
        if (page === 0) {
            return this.logFolderPath + `/${this.logFileName}`
        }

        const files = fs.readdirSync(this.logFolderPath , { withFileTypes: true })
            .filter(dirEnt => dirEnt.isFile() && dirEnt.name.endsWith('.log'))
            .map(dirEnt => dirEnt.name);
        if (page >= files.length)
            throw new OutOfRange();
        return this.logFolderPath + '/' + files.at(-1 - page);
    }

    /**
     * Create a logs folder if it doesn't exist.
     */
    createLogFolder() {
        this.logFolderPath = options.rootPath + "/logs";
        if (!fs.existsSync(this.logFolderPath)) {
            fs.mkdirSync(this.logFolderPath);
        }
    }

    /**
     * Write into the file stream. If the size of the stream is more than `UPLOAD_MAX_SIZE` the
     * content of the stream will be cached in the logs folder and a new file stream is generated.
     * @param {string} data 
     */
    writeIntoStream(data) {
        data = data + "\n";
        const dataSize = Buffer.from(data).length;
        if (this.logFileCurrentSize + dataSize > UPLOAD_MAX_SIZE) {
            this.stream.close();
            this.createNewStream();
        }
        this.stream.write(data);
        this.logFileCurrentSize += this.stream.writableLength;
    }

    /**
     * Create a new file stream in which writing logs.
     */
    createNewStream() {
        this.streamCreationDate = new Date();
        // Choose ISO string because it's sorted alphabetically.
        this.logFileName = `${this.streamCreationDate.toISOString().replaceAll(":", "-")}-${this.streamCreationDate.toLocaleTimeString().replaceAll(":","-")}.log`;
        this.stream = fs.createWriteStream(this.logFolderPath + `/${this.logFileName}`);
        /** Current file size in bytes.*/
        this.logFileCurrentSize = 0;
    }
}

module.exports = {
    FileLogger
}