
/**
 * @class Base class for all the other services.
 * A service is a 'singleton' object (each service is bound to a single ClientContext) that provides
 * functionalities for the rest of the application.
 */
class BaseService
{
    /**
     * Base constructor.
     * @param {import('./ClientContext').ClientContext} ctx The client context bound to the service.
     */
    constructor(ctx) {
        this.clientContext = ctx;
    }
    
    /**
     * Initialize the service once the Discord's client has been connected.
     */
    async init() {}

    /**
     * Called when the bot joins a new guild when it's connected.
     * Initialize the necessary data in the service for the new guild.
     * @param {Guild} guild The newly joined guild.
     */
    async initNewGuild(guild) {}

    /**
     * Called when the client has been destroyed and the ClientContext is terminating.
     */
    async terminate() {}
}

module.exports = {
    BaseService
}