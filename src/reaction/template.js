const { Message } = require('discord.js');

/**
 * 
 * @typedef {import('../ClientContext').ClientContext} ClientContext 
 */

/**
 * @typedef {object} Reaction The model for the object to export in a reaction script.
 * @property {number} order (optional) The order of priority of this reaction. The smaller the number, the higher the priority.
 * @property {boolean} break (optional) If `true` this reaction will break the chain of reactions if `execute` returns `true`.
 * @property {(ctx : ClientContext, msg : Message) => Promise<void | boolean>} execute The function to execute for the reaction. The function must be asynchronous.
 */

/**
 * The reaction code.
 * @param {ClientContext} ctx The current client context.
 * @param {Message} msg The message to react to. 
 */
async function execute(ctx, msg) {
    // Code goes there.
}

/**
 * @type {Reaction}
 */
module.exports = {
    break: false,
    order: 0,
    execute: execute
}