const fs = require('fs');
const { Message } = require('discord.js');
const { isAsyncFunction } = require('util/types');
const { logger } = require('../logging/logging');
const { BaseService } = require('../BaseService');

/**
 * @typedef {import("./template").Reaction} Reaction
 */

/**
 * Import all Reaction from the 'reactions' folder.
 * @returns {Reaction[]}
 */
function importReactions() {
    const reactionFiles = fs.readdirSync('./src/reaction/reactions')
        .filter(file => file.endsWith(".js"));
    const compare = (a,b) => {
        if (!Number.isFinite(a.order))
            return 1;
        if (!Number.isFinite(b.order))
            return -1;
        return a.order - b.order;
    };
    const reactions = reactionFiles.map(file => require(`./reactions/${file}`))
        .filter(reaction => isAsyncFunction(reaction.execute))
        .sort(compare);
    return reactions;
}

/**
 * Class that manages Reactions aka specific scripts to execute for each message that's not a command.
 * Not to be confound with Discord's ReactionEmojis.
 */
class ReactionService extends BaseService
{
    constructor(ctx) {
        super(ctx);
        
        /**
         * @type {Reaction[]}
         */
        this.__reactions = importReactions();
    }

    /**
     * Starts the chain of reactions to the given message.
     * @param {Message} msg The message to react to.
     */
    async react(msg) {
        for (let reaction of this.__reactions) {
            let success = await reaction.execute(this.clientContext, msg).catch(_err => logger.error(_err));
            if (reaction.break && success)
                break;
        }
    }
}
module.exports = {
    ReactionService
}