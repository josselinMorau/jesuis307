const { Message } = require('discord.js');

/**
 * React when someone says 'koa'.
 * @param {ClientContext} ctx The current client context.
 * @param {Message} msg The message to react to. 
 */
async function execute(ctx, msg) {
    if (/(^|[^a-z|^A-Z])a+t+i+l+a+([^a-z|^A-Z]|$)/gi.test(msg.content) && 
        !(/ATILLA|atilla|Atilla/g.test(msg.content))) {
            msg.reply(`On écrit **__ATILLA__** !!`);
    }
}

module.exports = {
    execute: execute
}