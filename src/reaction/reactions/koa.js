const { Message } = require('discord.js');

/**
 * React when someone says 'koa'.
 * @param {ClientContext} ctx The current client context.
 * @param {Message} msg The message to react to. 
 */
async function execute(ctx, msg) {
    if (msg.content.match(/(\s|^|[,;.?!])[kqc]+\s*[ow0]+\s*a+(\s|$|[,;.?!])/gi)) {
        msg.react('🐸').catch(_err => {});
    }
}

module.exports = {
    execute: execute
}