require("dotenv").config(); // Load environment variables from .env file.

const { Client, Intents } = require('discord.js');
const { ClientContext } = require('./src/ClientContext');
const { initDatabase } = require('./src/database/database.js');
const { sleep } = require("./src/utils/helper");
const { options } = require('./options.js');
const { logger } = require("./src/logging/logging");

/**
 * Keep in mind to update the bitfield depending on the features we want for the bot.
 * cf https://discord.com/developers/docs/topics/gateway#list-of-intents
 * */ 
const INTENTS = Intents.FLAGS.GUILDS | Intents.FLAGS.GUILD_MESSAGES | 
    Intents.FLAGS.GUILD_MEMBERS | Intents.FLAGS.DIRECT_MESSAGES |
    Intents.FLAGS.GUILD_MESSAGE_REACTIONS | Intents.FLAGS.GUILD_VOICE_STATES;

/**
 * Initialize all required modules before any client login. (eg Database)
 */
async function setupBeforeLogin() {
    logger.init();
    await initDatabase();
}

/**
 * Check if the given terminated ctx ask for a restart.
 * @param {ClientContext} _ctx 
 * @param {boolean} shouldRestart
 */
async function checkIfShouldRestart(_ctx, shouldRestart) {
    if (shouldRestart) {
        logger.debug("Restart time:", options.restartTime);
        await sleep(options.restartTime);
        logger.log("RESTART");
        login();
    }
}

/**
 * Creates a new ClientContext and login the corresponding Discord's client.
 */
function login() {
    const ctx = new ClientContext(new Client({ intents: INTENTS }));
    ctx.onceTerminated(checkIfShouldRestart);
    ctx.client.login(process.env.ATILLA_BOT_TOKEN);
}

/**
 * The main function.
 */
async function main() {
    await setupBeforeLogin();
    login();
}

main();
