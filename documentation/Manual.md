# Manuel

Ce manuel a pour but de décrire le fonctionnement global de chaque partie de l'application.
Cette section ne couvre pas les concepts de l'API Discord elle-même. Pour en savoir plus sur cette dernière, consultez
[la documentation officielle](https://discord.js.org/#/docs/main/stable/general/welcome).

## ClientContext

La classe `ClientContext` est un wrapper pour un client Discord. C'est cet objet qui permet d'accéder aux différents services de l'application. Le but de cet objet est d'éviter de passer par des variables statiques ou globales pour accéder à des dépendances (cf Inversion de dépendances). Cet aussi dans cette classe que l'on va définir les callbacks principales vers les events Discord.

En encapsulant toutes les dépendances dans un objet contexte passé aux scripts des commandes et aux différents services, on peut donc gérer plusieurs clients potentiels indépendamment sans se soucier qu'ils interferent entre eux.
Cela permet en autres de gérer des sessions successives du bot. Via une commande on peut terminer la session du client en cours, le contexte associé sera donc détruit ainsi que tous ses services. Une nouvelle session peut être démarrée juste après et le nouveau contexte se chargera de construire ses propres services sans qu'il n'y ait de conflits avec les anciens.
On peut aussi imaginer que l'application gère plusieurs clients simultanément (on aurait donc plusieurs instances du bot qui tournent sur le même programme), ce qui serait possible mais qui à l'heure actuelle n'a pas d'intérêt.

### Enregistrement des services

L'objet de la classe `ClientContext` est aussi celui qui s'occupe de créer les différents services pour sa session et de les initialiser. Pour ajouter un nouveau type de service dans le contexte, importez et ajouter sa classe dans la liste constante `SERVICES` définie dans [`ClientContext.js`](../src/ClientContext.js).

À sa construction, le contexte itére sur cette liste et crée une instance de chacun des services correspondants. L'ordre de construction et d'initialisation des services dépend donc de leur ordre dans cette liste. Contrairement à d'autres mécanismes dans l'application, les services ne sont donc pas auto-importés.

### Events Discord

Le `ClientContext` permet aussi d'implémenter les callbacks pour [les events Discord](https://discordjs.guide/creating-your-bot/event-handling.html#individual-event-files).

Les callbacks sont implémentées automatiquement à la construction du contexte. Pour créer une nouvelle callback dans le contexte, ajoutez une méthode de classe `onLeNomDeLevent` avec comme paramètres les arguments attendus par l'event correspondant. Le contexte associera automatiquement la méthode à l'event en fonction de son nom. Notez que la première lettre après le `on` du nom sera passé en minuscule. Ainsi `onReady` et `onready` seront tous deux enregistrées auprès de l'event `ready` du client Discord.

Pour connaitre le nom des events Discord, consultez [la documentation de la classe Client](https://discord.js.org/#/docs/main/stable/class/Client).

## Services

Les services sont des classes destinées à être unique par contexte et qui servent à gérer des fonctionnalités de l'application et à fournir une API pour d'autres services, des commandes ou d'autres parties du code. L'exemple le plus fréquent étant d'offrir une interface pour gérer certains modèles de la base de données.

Chaque service est généré à la construction du contexte puis initialisé par ce dernier une fois le client Discord connecté.
On peut accéder à chaque service en appelant la méthode `getService` de `ClientContext`. La méthode prend en argument le type de service que l'on veut récupérer. Par exemple pour récupérer l'instance du `SettingService` on écrira : 

```js
const settingService = ctx.getService(SettingService);
```

Tous les services héritent de la classe `BaseService` et doivent prendre en unique argument dans leur constructeur le contexte actuel. Comme le `ClientContext` est passé en argument, tous les services peuvent avoir accès aux autres en passant par leur variable membre `this.clientContext`.

La classe `BaseService` propose 3 méthodes à surcharger : 

- `async ìnit()` :  Qui est appelée pour initialiser le service une fois que le client est connecté.
- `async initNewGuild(guild)` : Qui est appelée quand le bot rejoint un nouveau serveur pendant que le bot est toujours connecté. Cela permet d'initialiser des données dans le service qu'on aurait dû faire d'habitude dans `init`.
- `async terminate()` : Qui est appelée quand le client Discord a été détruit et que le contexte est sur le point d'être détruit. Cela permet de se débarasser de certaines données.

Toutes les méthodes sont asynchrones et sont attendues avant d'appeler la suivante. Ainsi si un service `B` a besoin de données d'un service `A`, il suffit de s'assurer que `B` vient après `A` dans la liste `SERVICES` et `A` sera correctement initialisée au moment d'initialiser `B`.

Pour créer votre propre service, créez donc votre classe comme suit : 

```js
const { BaseService } = require('./BaseService.js'); // Adaptez le chemin d'import en fonction d'où se situe votre fichier.

class MyService extends BaseService
{
    constructor(ctx) {
        super(ctx); // Important ! Passez le contexte au constructeur parent pour initialiser correctement la variable membre.

        // ...
    }

    async init() { // Implémentez init si vous avez besoin d'initialiser des données une fois le bot connecté.
        // ...
    }

    async initNewGuild(guild) { // Implémentez si nécessaire.
        // ... 
    }

    // ...
}

module.exports = { // Exportez bien votre classe pour qu'elle soit accessible ailleurs.
    MyService
}
```

Puis dans `ClientContext.js` ajoutez votre nouvelle classe : 

```js
// ...
const { MyService } = require('./MyService.js'); // Encore une fois adaptez le bon chemin.

/**
 * @type {(typeof BaseService)[]} 
 * List of all service types. The order in the array defines the order in which each service will be initialized.
 */
const SERVICES = [
    CommandService,
    TaskExecutor,
    ReactionService,
    SettingService,
    RoutineService,
    RoleService,
    ChannelService,
    ChannelListener,
    WidgetListener,
    NewcomerHandler,
    MyService // Ajoutez votre classe dans SERVICES. Dans ce cas elle sera initialisée en dernier.
];

// ...
```

## Liste des services

### CommandService

Ce service permet de gérer l'éxecution des commandes. Il permet de gérer les alias et le commandes customisées propres à chaque serveur ainsi que les permissions personnalisées pour chaque commande.
Pour plus d'infos sur les commandes, regardez la section [Gestion des commandes](#gestion-des-commandes).

### TaskExecutor

Le `TaskExecutor` est un service qui permet de déléguer l'éxecution de tâches asynchrones. Son but est de s'assurer que des tâches du même type ne s'éxecute pas en même temps. Par exemple, on veut éviter de pouvoir appeler les commandes qui gèrent les messages des channels simultanément sur le même serveur (clear, move, copy...).
En confiant l'éxecution de ces tâches au `TaskExecutor` on peut donc s'assurer qu'une tâche de même type est éxecutée à la fois. Les types de tâches sont décrits dans l'enum `TASK_TYPES`.

### ReactionService

Ce service permet de gérer les réactions aux messages qui ne sont pas des commandes. Pour plus d'infos, regardez la section [Gestion des réactions](#gestion-des-réactions).

### RoutineService

Ce service permet de gérer les événements routiniers. Pour plus d'infos, regardez la section [Gestion des routines](#gestion-des-routines).

### SettingService

Le `SettingService` est un service permettant d'accéder et de redéfinir les settings pour chaque serveur.
Les settings sont une collection de paramètres pour activer, désactiver ou définir certains comportements du bot sur le serveur. (eg activer l'accueil des nouveaux membres, etc)

Les différents settings sont définis via l'objet `SETTINGS` dans [`settings.js`](../src/utils/guildSettings/settings.js).
Pour ajouter un nouveau setting, renseignez son nom dans l'objet `SETTING_NAMES` et complétez l'objet `SETTINGS` avec le champ adéquat.

Un champ de `SETTINGS` est un objet qui doit respecter les attributs suivants : 

- `type` : Un `DataType` de sequelize déterminant sous quel type le setting est enregistré dans la BDD.
- `defaultValue` : La valeur par défaut du setting s'il n'est pas défini dans la BDD.
- `argsCount`: Le nombre d'arguments minimum attendu par le setter de la commande `settings`. Si non précisé, le setter peut prendre n'importe quel nombre d'arguments.
- `parser` : La fonction utilisée pour parser les arguments du setter. Si non précisé, la fonction `parseByDefault` sera utilisée.

Les attributs du modèle `Settings` seront automatiquement redéfinis au démarrage en fonction des champs présents dans `SETTINGS`.

Pour récuperer la valeur d'un setting dans le code, utilisez la fonction `getSetting` du SettingService. Utilisez l'objet `SETTING_NAMES` comme une énumération lorsque vous passez en argument le nom du setting souhaité plutôt que de passer un string en dur.

Exemple : 

```js

const prefix = await settingService.getSetting(guildId, SETTING_NAMES.PREFIX);
// Plutôt que const prefix = await settingService.getSetting(guildId, "prefix");

```

### ChannelService

Le `ChannelService` est un service permettant d'obtenir les métadonnées sur les channels d'un serveur. Il permet entre autres de récupérer les ids des channels ayant des catégories spécifiques (channel de bienvenue, channel de prison, etc).

Pour définir une nouvelle catégorie, renseignez un nouveau champ dans l'énumération `CHANNEL_CATEGORIES` dans [`channelUtils.js`](../src/utils/channels/channelUtils.js). Le champ et sa valeur doivent être en majuscules.

### RoleService

Le `RoleService` est un service permettant d'obtenir les métadonnées sur les rôles d'un serveur. Il permet entre autres de récupérer les ids des rôles ayant des noms spécifiques (rôle prisonnier, rôle eistien, etc).

Pour définir un nouveau 'nom' de rôle, renseignez un nouveau champ dans l'énumération `ROLE_NAMES` dans [`roles.js`](../src/utils/roles/roles.js). Le champ et sa valeur doivent être en majuscules.

### ChannelListener

Le `ChannelListener` est un service qui permet d'enregistrer des callbacks à invoquer lorsqu'un message est envoyé sur un channel (à l'exception des commandes).
Il y a deux types de callbacks enregistrables :
- `singleChannelCallback` : Est invoquée lorsqu'un nouveau message est entré dans un channel précis.
- `multipleChannelCallback` : Est invoquée lorsqu'un nouveau message est entré dans n'importe quel channel d'un serveur précis.

Cet service permet donc d'éviter d'avoir à rajouter des appels de fonctions directement dans le `ClientContext` et même de créer des écoutes à des channels temporairement puisqu'il est possible de désenregistrer une callback à tout moment.
Préférez utiliser ce service pour des opérations qui ont besoin d'écouter de manière persistente des messages. S'il s'agit juste d'attendre un nombre limité de réponse(s) dans un laps de temps donné, utilisez plutôt [`TextChannel.awaitMessages`](https://discord.js.org/#/docs/main/stable/class/TextChannel?scrollTo=awaitMessages) ou [`TextChannel.createMessageCollector`](https://discord.js.org/#/docs/main/stable/class/TextChannel?scrollTo=createMessageCollector).

### WidgetListener

Dans le même style que le `ChannelListener`, le `WidgetListener` est un service qui permet d'enregistrer des callbacks à invoquer quand une interaction avec un widget/component (Bouton, SelectMenu, etc) est effectuée.
Le service propose un type de callback enregistrable pour chaque type de components disponibles via l'API Discord.
Préférez utiliser ce service pour des opérations qui ont besoin d'écouter de manière persistente des interactions. S'il s'agit juste d'attendre une interaction précise avec un component, utilisez plutôt [`Message.awaitMessageComponent`](https://discord.js.org/#/docs/main/stable/class/Message?scrollTo=awaitMessageComponent) ou [`Message.createMessageComponentCollector`](https://discord.js.org/#/docs/main/stable/class/Message?scrollTo=createMessageComponentCollector).
Pour plus d'informations sur les interactions ou les components, référez vous à [la documentation officielle de Discord](https://discordjs.guide/interactions/buttons.html#building-and-sending-buttons).

### NewcomerHandler

Le `NewcomerHandler` est un service qui gère l'arrivée des nouveaux arrivants sur le serveur.
Ce service n'est actif que si le setting `welcomeEnabled` est activé sur le serveur.

### JailService

Le `JailService` est un service qui permet de gérer les emprisonnement et les libérations.
Ce service ne fonctionne correctement que si le rôle `PRISONER` a été défini sur le serveur.

### BirthdayService

Le `BirthdayService` est un service qui permet de gérer les dates d'anniversaires entrées en base de données.

### PollService

Le `PollService` est un service qui permet de créer, fermer et gérer les sondages de chaque serveur.

### MusicService

Le `MusicService` est un service qui gère les playlists de musiques qui peuvent être jouées dans le vocal de chaque serveur.
"L'état" de chaque playlist et de la diffusion de musique est englobé par une classe `MusicState` dont les instances sont générées
et gérées par le `MusicService`.
Ce service a de particulier que chaque `MusicState` génère des embeds avec des boutons interactifs. Ces boutons sont eux même générés à partir du fichier [`musicButtonInteraction.js`](../src/misc/musicButtonInteractions.js) dans lequel chaque bouton est géré par un objet implémentant le type `InteractionHandler`.

## Options

À ne pas confondre avec les settings. (Il faudrait sûrement trouvé un meilleur nom.)
Cet objet contient des paramètres de lancement de l'application. Mettez-y toutes les variables dépendant des variables d'environnement. Pour plus d'infos, lisez [l'implémentation de l'objet](../options.js).

## Logger

Pour abstraire l'utilisation des logs dans l'application, une classe statique `logger` est implémentée dans le fichier [`logging.js`](../src/logging/logging.js). Au démarrage de l'application, cette classe va créer une instance héritant de `BaseLogger`, la classe `logger` n'étant qu'un wrapper statique pour cet objet.
Utilisez cette objet à la place de `console.log` afin de s'assurer que tous les logs soient bien gérés par l'objet adéquat.
Le type concret de l'instance créée dans `logger` est déterminé au démarrage de l'application en lisant la variable d'environnement `ATILLA_BOT_LOGGER`. L'interface de `BaseLogger` donne accès aux fonctions suivantes : 
- `log` : Print les informations dans le logger. Fonctionne sur le même principe que `console.log`.
- `debug` : Comme `log` mais n'est invoqué que si l'application est en mode de test. Utilisez cette fonction pour débugger plutôt que `log` pour éviter de parasiter les logs en production si vous oubliez de retirer vos appels.
- `error` : Print une erreur dans le logger. Fonctionne sur le même principe que `console.error`.
- `fetchLogs(page)` : Une méthode asynchrone permettant de récupérer sous forme de fichier les logs d'une 'page' donnée. Cette fonction peut ne pas être implémentée mais la commande `logs` sera alors inutilisable avec le mode de logger correspondant. 

Pour créer un nouveau type de logger, implémentez une nouvelle sous-classe de `BaseLogger` dans un nouveau script dans le dossier `./src/logging/loggers`.

Exemple : 

```js

const { BaseLogger } = require('../BaseLogger.js');

// Le logger sera utilisé si la ATILLA_BOT_LOGGER vaut 'Example' ou 'ExampleLogger'.
class ExampleLogger extends BaseLogger
{
    // Respectez bien la forme des arguments.
    log(message, ...params) {
        // ...
    }

    error(message, ...params) {
        // ...
    }

    // Inutile d'implémenter debug() car son comportement est déjà défini par BaseLogger.

    // N'implémentez cette fonction que s'il est possible de récupérer les logs.
    // Ce n'est par exemple pas possible avec le ConsoleLogger.
    async fetchLogs(page = 0) {

    }
}

// Faites bien attention à l'exporter.
module.exports = {
    ExampleLogger
}
```

Le type du logger est déterminer en matchant la valeur de `ATILLA_BOT_LOGGER` avec le début du nom des sous-classes de `BaseLogger`. Évitez donc de donner à votre classe un nom qui commence pareil qu'une autre classe de Logger au risque de les confondre.

Les types de logger existants sont les suivants : 

- `ConsoleLogger` : Print dans `console`. Logger par défaut.
- `FileLogger` : Comme `ConsoleLogger` mais print également dans des fichiers de logs afin d'avoir une trace récupérable depuis Discord.

## Gestion des commandes

Toutes les commandes sont importées depuis le sous-dossier `./src/commands`.
Les commandes sont triées par catégorie (Divers, Modération, etc). Chaque sous-dossier dans `./src/commands` correspond à une catégorie de commandes. Si un script de commande ne se trouve pas dans un sous-dossier de catégorie, la commande ne sera pas importée.

Chaque script de commande contient 2 membres:

- Un objet `help` qui contient des informations générales sur la commande.
- Une fonction `run` qui est exécutée quand la commande est invoquée.

Vous pouvez copier le template d'une nouvelle commande dans [`./src/commands/template.js`](../src/commands/template.js).

L'objet `help` contient les valeurs suivantes : 

- `name` : Le nom de la commande.
- `aliases` : Une liste de noms alias pour la commande.
- `usage` : Le mode d'emploi complet de la commande. Le format préféré est `nomDeLaCommande <argument1> [<argumentOptionnel>]`.
- `description` : Une description de la commande.
- `examples`: Une liste d'exemples pour couvrir tous les cas d'utilisation de la commande. Chaque élément de la liste un objet qui contient les champs suivants : 
    - `usage` : Un exemple d'utilisation de la commande. (eg `test 1 2 3`)
    - `description` : La description de ce que fait l'exemple.
- `permissions` : Un objet permettant de détailler les permissions requises. Il contient les champs suivants : 
    - `flag` : Un bitfield des permissions requises pour éxecuter les commandes. Voir [ici](https://discord.js.org/#/docs/main/stable/class/Permissions?scrollTo=s-FLAGS) pour les valeurs possibles du flag.
    - `op` : Le type d'opérateur à appliquer sur le flag pour vérifier les permissions. Si égal à "any" le `CommandService` vérifiera si l'utilisateur a au moins un des flags du bitfield dans ses permissions. Si égal à "all", il vérifiera si l'utilisateur a tous les flags précisés. S'il vaut `null` ou `undefined`, aucune vérification des permissions ne sera faite.
- `delete` : Si égal à `true`, le message qui a invoqué la commande sera supprimé avant d'éxecuter le code de la commande.
- `onTest` : Si égal à `true`, la commande ne sera disponible qu'en environnement de test.

La fonction `run` prend en arguments dans l'ordre : 

- `ctx` : Le `ClientContext` dans lequel la commande a été invoquée. Utilisez cet objet pour récupérer les services nécessaires à la commande.
- `msg` : Le message duquel on a appelé la commande. Utile pour récupérer des informations comme le channel, l'auteur de la commande, etc.
- `args` : La liste des arguments passés à la commande. Les arguments sont parsés par le `CommandService` de sorte à ce que chaque mot séparé par des espaces suivants le nom de la commande est considéré comme un argument à l'exception des keyword arguments. Par exemple si on invoque la commande `!test 1 2 3 --option 10`, la liste des arguments contiendra `['1', '2', '3']`.
- `kwargs` : Un objet/dictionnaire des keyword arguments. Ces arguments sont parsés de sorte à ce que chaque liste de mot suivi d'un mot préfixé par `--` soit ajouté dans la liste des arguments de la clé correspondante. Par exemple si on invoque la commande `!test --option 1 2 3 --option2`, l'objet kwargs parsé sera `{ option: ['1', '2', '3'], option2: [] }`.

Il est préférable que la méthode `run` ne soit pas asynchrone car les versions récentes de Node.js ne supporte plus les erreurs non catchés dans une promesse.

S'il faut appeler attendre des appels asynchrones, utilisez `then` ou passez par une fonction tierce.
Par exemple : 

```js
//...

async function maFonction() {
    // ...
}

module.exports.run = (ctx, msg, args, kwargs) => {
    // Ici parsez les arguments de la commande.

    maFonction().then(res => {
        // Gérez le résultat de votre fonction.
    })
    .catch(err => {
        // Assurez vous de gérez vos erreurs.
    });
}

//...
```

## Gestion des réactions

Les réactions (à ne pas confondre avec les réactions emojis de Discord) sont des scripts éxecutés dans l'ordre pour chaque message qui n'est pas une commande. Ils sont gérés par le `ReactionService`.

Toutes les réactions sont automatiquement importées depuis le dossier `./src/reaction/reactions`. Chaque fichier de ce dossier doit exporter un objet avec les champs suivants : 

- `break` : Si égal à `true` la réaction stoppera la chaîne de réactions si sa fonction `execute` retourne `true`.
- `order` : Détermine l'ordre de priorité de la réaction. Plus la valeur est basse, plus la réaction sera prioritaire.
- `execute` : La fonction à appeler sur lors que la réaction s'opère. La fonction doit retourner un booléen si `break` vaut `true`. La fonction doit prendre en arguments :
    - `ctx` : Le `ClientContext` actuel.
    - `msg` : Le message sur lequel réagir.

Vous pouvez copier la structure de votre script de réaction sur le fichier [`template.js`](../src/routine/template.js).

## Gestion des routines

Les routines sont des événements réguliers qui se déclenchent à des moments précis dans la durée de vie de l'application.
Elles sont gérées par le `RoutineService`.

L'application utilise [node-schedule](https://www.npmjs.com/package/node-schedule) pour programmer ses événements. Renseignez vous notamment sur les [cron strings](https://fr.wikipedia.org/wiki/Cron#Syntaxe_de_la_table) et les Jobs.

Toutes les routines sont automatiquement importées depuis le dossier `./src/routine/routines`. Chaque fichier de ce dossier doit exporter un objet avec les champs suivants : 

- `name` : Le nom de la routine.
- `enabledByDefault` : Si la routine doit être activée par défaut.
- `defaultRule` : La règle d'occurrence par défaut de la routine. La règle est définie par une cron string.
- `schedule` : La fonction de programmation de la routine. La fonction doit retourner le `Job` créé par la programmation de l'événement. La fonction doit prendre en arguments : 
    - `ctx` : Le `ClientContext` actuel.
    - `guildId`: L'id du serveur dans lequel l'événement est programmé.
    - `rule` : La règle d'occurrence de l'événement.

## Gestion de la base de données

Le bot utilise [Sequelize](https://sequelize.org/) pour gérer les appels à la base de donnée.
Tous les modèles de données sont interfacés par des sous-classes de `DataModel` lui même une sous-classe de [`Model`](https://sequelize.org/master/class/lib/model.js~Model.html).
Pour créer un nouveau modèle de données, implémentez une nouvelle sous-classe de `DataModel` dans un nouveau script dans le dossier `./database/models`.

Exemple:
```js
const { DataTypes, ModelAttributes } = require('sequelize');
const { DataModel } = require('../DataModel.js');

class MyModel extends DataModel {
    // Définition du modèle ici.
    // ...
}
```

Vous pouvez ensuite déterminer les attributs du modèles en surchargeant la variable statique `modelAttributes` (plus d'info sur [la doc officielle](https://sequelize.org/master/manual/model-basics.html)):

```js
///...

    static modelAttributes = {
        id: {
            type: DataTypes.STRING,
            allowNull: false,
            primaryKey: true
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false
        },
        number: {
            type: DataTypes.INTEGER
        }
    }

//...
```

Si votre modèle a des associations avec d'autres modèles vous pouvez les définir en surchargeant la variable statique `modelAssociations` (plus d'info sur [la doc officielle](https://sequelize.org/master/manual/assocs.html))

```js
//...

    static modelAssociations = [
    {
        type: ASSOCIATION_TYPE.BELONGS_TO_MANY,
        model: SomeModel,
        options: {
            through: "SomeModelAssociation"
        }
    },
    {
        type: ASSOCIATION_TYPE.HAS_ONE,
        model: AnotherModel
    }
    ];

//...
```

Vous pouvez également définir une liste d'events ou ["hooks"](https://sequelize.org/master/manual/hooks.html) que le modèle enregistrera en surchargeant la variable statique `hooks` :

```js
//...

    static hooks = ["afterUpdate", "afterDestroy"];

//...
```

Vous pourrez ensuite enregistrer des callbacks pour ces events en appelant la méthode statique `on` du modèle correspondant.

Assurez vous enfin de bien exporter la classe à la fin de votre script : 
```js
module.exports = {
    MyModel
}
```

Tous les modèles de données sont importés et initialisés automatiquement au lancement de l'application. Si un nouveau modèle est ajouté ou un modèle existant a été modifié, Sequelize se chargera de synchroniser les modifications dans la base de donnée.

Pour effectuer des requêtes pour un modèle de données, [référez vous à la doc de Sequelize.](https://sequelize.org/master/manual/model-querying-basics.html)

Par exemple:

```js
const { MyModel } = require('./database/models/MyModel.js');

async function myFunc() {
    const patrick = await MyModel.find({where: { name: "Patrick" }});
    // ...
}

```
