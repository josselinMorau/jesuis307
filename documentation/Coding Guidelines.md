# Coding Guidelines

Cette section de la documentation a pour but de décrire les bonnes pratiques à suivre lorsqu'on développe le bot.
Il est important de la lire afin de se faire une idée de la ligne directrice prise lors du développement. Toutes les "règles" décrites ici ne sont pas gravées dans le marbre et peuvent changer en fonction des besoins de développement, du moins si le changement est pertinent.

L'ordre des sections qui suivent est arbitraire. N'hésitez pas à partager vos idées d'amélioration pour ce document.

## À propos des commentaires

Il va sans dire que commenter son code est important. Ne codez jamais sans laissez des commentaires.
À minima ce qui doit être documenté dans votre code sont : 
- Les fonctions. Les arguments et la valeur de retour doivent être aussi un minimum documentés (ne serait-ce que le type attendu.)
- Les classes et leurs membres "publics".
- Les "énumérations".
- Les sections de code peu claires sur leurs intentions.

Pour ce dernier, il est bon de préciser. On attend pas que chaque ligne d'un algorithme soit commentée. Le but est qu'on comprenne l'intention derrière chaque partie du code. Par exemple si à cause d'une limitation de l'API Discord, je suis contraint de faire un workaround, il faut le spécifier.

### JSDoc

Toutes les fonctions, les classes, les énumérations et les variables membres doivent documentées en utilisant [JSDoc](https://jsdoc.app/). Cela permet de connaitre plus facilement les types de données attendus en arguments ou en sortie et aussi cela simplifie l'auto-complétion sur les IDEs.

Cette section n'est pas un cours sur comment on se sert de JSDoc (lisez la doc). Néanmoins certains éléments non standards sont utilisés dans la documentation car supportés par la plupart des IDEs notamment la balise [@template](https://www.typescriptlang.org/docs/handbook/jsdoc-supported-types.html#template) qui permet de déclarer une fonction ou une classe comme templatable (aka dont les types d'entrées et/ou de retour sont génériques).

### Les commentaires en anglais

Je ne pense pas qu'il y ait besoin de s'étaler là dessus. Il faut se décider et c'est en règle général considéré comme une meilleure pratique de laisser ses commentaires en anglais. Vous aurez peut-être remarqué des commentaires en français qui trainent dans le code (ils datent d'une période où on avait pas vraiment tranché). Si vous les voyez penser à les remplacer.

Je précise également dans le doute mais les noms des variables, fonctions, classes, etc se doivent aussi d'être en anglais. Restons cohérents.

## Le principe du scout

Essayons d'appliquer le principe du scout en codant "un scout laisse un endroit toujours plus propre qu'à son arrivée".
Si vous tombez sur du code "trop dégueu", des commentaires qui manquent (ou qui sont en anglais), pensez à le corriger.
Si vous avez peur de casser du code dans le processus, n'hésitez pas à demander son avis à l'auteur original. Utilisez [`git blame`](https://git-scm.com/docs/git-blame) pour retrouver le nom de l'auteur. Ça vaut également pour la documentation. (N'hésitez pas à me dire si vous pensez que j'ai écrit de la merde.)

## DRY vs WET

Ici (et franchement en général quand vous codez) on applique le principe [DRY](https://fr.wikipedia.org/wiki/Ne_vous_r%C3%A9p%C3%A9tez_pas) 'Don't Repeat Yourself'. En gros si vous voyez que vous avez besoin de copier coller du code similaire (on appelle ça du WET 'Write Everything Twice'), c'est probablement que vous avez besoin de refactorer ce code via une fonction ou une structure spécifique. Le but d'appliquer ce principe est entres autres de maintenir plus facilement le code.

## Gardons notre code contextuel

Comme précisé dans le [manuel](<./Manual.md>) (si vous ne l'avez pas lu, je vous conseille fortement d'y jeter un oeil) la classe `ClientContext` est utilisée un peu partout pour assurer les dépendances. Ce qu'il faut retenir derrière son utilisation c'est qu'il faut impérativement garder le code contextuel. Tout (commandes, routines, service, modèles, etc) doit être codé avec cette idée de contexte. Plus généralement il faut garder à l'esprit que le bot doit pouvoir tourner sur plusieurs serveurs Discord en parallèle sans que le code éxecuté pour chacun d'entre eux ne vienne intérferer.

Plus concrètement, si on doit par exemple implémenter un modèle pour la base de données qui permet de paramétrer quelque chose sur un serveur Discord. Il faut s'assurer que les instances de ce modèle ne puissent pas être partagées entre plusieurs serveurs. Essayons donc d'avoir quelque part dans les champs du modèle, une référence à l'id du serveur, d'un de ses channels ou plus généralement un de ses objets qui lui est propre. Par exemple l'id d'un utilisateur/membre d'un serveur ne suffit pas car il est partagé entre tous les serveurs Discord.

Toujours dans cette idée de contexte, ne surtout pas utiliser des variables globales/statiques !!! (Cela comprend aussi les singletons.) D'une part c'est une mauvaise pratique, d'autre part ce n'est pas du tout safe quand fait de la programmation parallèle (les fonctions asynchrones de Javascript ne sont pas à proprement parler de la programmation parallèle mais l'idée reste la même). Si vous avez besoin d'accéder à l'instance d'une classe en particulier depuis plusieurs endroits faites en un [Service](<./Manual.md#services>).

Les seules situations où il est raisonnable d'utiliser des variables statiques sont :
- Pour des constantes. Comprenez ici une variable utile dont la valeur ne changera pas durant le cycle de vie l'application. (Avoir écrit `const` ne suffit pas toujours à assurer ce principe...)
- Pour des "classes statiques" dont l'utilisation est censé être partagé entre les différents contextes (eg les DataModel et le Logger).


## Gestion des erreurs/exceptions

### Ne pas ignorer les erreurs ! (Error hiding)

Quoiqu'il arrive évitons le plus possible de faire de l'error hiding (ou error swallowing). C'est à dire catcher des erreurs dans le code en les laissant silencieuses.

Ce qu'on doit faire d'une erreur dépend de la situation.

Si l'erreur implique qu'un élément important de l'application (Service, Database, etc) ne va pas fonctionner correctement alors on ne catche pas l'erreur. Il vaut mieux laisser l'application crasher et se rendre compte qu'il y a un problème plutôt que de tout ignorer et que le bot ne réponde plus correctement sans qu'on sache pourquoi. À la limite on peut catcher l'exception pour la relancer juste pour s'assurer qu'elle termine dans les logs : 

```js

try {
    // On fait quelque chose
}
catch (err) {
    logger.error(err); // On print dans les logs l'erreur.
    throw err; // On réinvoque l'erreur pour que l'application crash dignement.
}

```

Si l'erreur est localisée et ne témoigne pas d'une défaillance de l'application, on peut la catcher mais non sans signaler à l'utilisateur qu'un problème est survenue.

Par exemple pour une commande qui dépenderait d'un service tiers : 

```js

getAPIResponse()
.then(response => {
    // Si tout se passe bien on process la réponse.
})
.catch(err => {
    logger.error(err); // Si une erreur arrive c'est probablement que l'utilisateur a donné les mauvaises données ou que l'API ne répond pas. Inutile de faire crash l'application pour si peu.
    channel.send("Une erreur est survenue."); // On prévient tout de même l'utilisateur de l'échec.
});

```

### Catch des promesses

Important à noter. Les versions récentes de Node ne supportent plus les erreurs non catchées dans des promesses.
Si cela arrive, l'application entière sera terminée.

Pensez donc quand vous utilisez une fonction asynchrone susceptible de soulever une erreur de soit : 

- Appelez `catch` sur la fonction : 
```js

waitForSomething().catch(err => {
    // Process l'erreur.
});

```
- S'assurer que la fonction est "awaitée" et que l'appel parent catche correctement la fonction actuelle.
```js

async function waitProcess() {
    // On peut se contenter "d'awaiter" l'appel tant que waitProcess gère ses erreurs correctement.
    await waitForSomething();
}

// ...

waitProcess().catch(err => {
    // Process l'erreur.
});

```

## Naming convention

En Javascript la convention est en général la suivante : 

- Pour les fonctions et les variables : `camelCase`.
- Pour les classes : `PascalCase`.
- Pour les contantes et les enums : `SCREAMING_SNAKE_CASE`.

En bonus, pour préciser qu'une variable ou une méthode de classe est censée être "privée" on peut comme en Python la préfixer par deux underscores : 
```js
this.__dontTouchThisVariable = 10;
```
