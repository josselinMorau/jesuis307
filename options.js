const fs = require('fs');
const { parseBoolean } = require('./src/utils/helper.js');

/**
 * Object containing the differents startup options of the application.
 */
const options = {

    /**
     * Whether the application is in test mode.
     */
    test: !!parseBoolean(process.env.ATILLA_BOT_TESTING),

    /**
     * @type {string[]} A list of user ids that overwrites all permissions. This is only enabled on testing mode.
     */
    developerIds: [],

    /**
     * Whether to display the table of commands at the startup of the application.
     */
    displayCommandTable: !!parseBoolean(process.env.ATILLA_BOT_DISPLAY_COMMAND_TABLE),

    /**
     * The name of the type of logger used by the application.
     * @default "Console"
     */
    logger: (process.env.ATILLA_BOT_LOGGER || "Console").trim(),

    /**
     * The path to the root of the project.
     */
    rootPath: fs.realpathSync('./'),

    /**
     * The duration in ms before restarting the client if a restart is required.
     */
    restartTime: parseInt(process.env.ATILLA_BOT_RESTART_TIME) || 10_000
}

if (options.test && process.env.ATILLA_BOT_DEVELOPER_IDS) {
    options.developerIds = process.env.ATILLA_BOT_DEVELOPER_IDS.split(',').map(id => id.trim()).filter(id => !!id);
}

module.exports = { 
    options
};